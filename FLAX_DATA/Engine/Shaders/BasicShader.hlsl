////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2012-2015, Wojciech Figat - www.celelej.com
// All rights reserved.
////////////////////////////////////////////////////////////////////////////////////

// Includes
#include "Common"

float4 Col;

DECLARE_TEXTURE(Txt, 0, MIN_MAG_MIP_LINEAR, Wrap, Wrap, 0, 1);

struct VS_Input
{
	float3 Position : POSITION;
};

struct VS_InputVc
{
	float3 Position : POSITION;
    float4 Color    : TEXCOORD;
};

struct VS_InputTx
{
	float3 Position : POSITION;
    float2 TexCoord : TEXCOORD;
};

struct VS_InputTxVc
{
	float3 Position : POSITION;
	float4 Color    : TEXCOORD0;
    float2 TexCoord : TEXCOORD1;
};

struct VS_Output
{
	float4 Position : SV_POSITION;
    float4 Diffuse  : TEXCOORD0;
	float2 TexCoord : TEXCOORD1;
};

VS_Output VSBasic(VS_Input vin)
{
    VS_Output vout;
    
    vout.Position = mul(float4(vin.Position, 1), WVP);
    vout.Diffuse = Col;
	vout.TexCoord = float2(0, 0);
    
    return vout;
}

VS_Output VSBasicVc(VS_InputVc vin)
{
    VS_Output vout;
	
    vout.Position = mul(float4(vin.Position, 1), WVP);
    vout.Diffuse = Col * vin.Color;
	vout.TexCoord = float2(0,0);
	
    return vout;
}

VS_Output VSBasicTx(VS_InputTx vin)
{
    VS_Output vout;
    
    vout.Position = mul(float4(vin.Position, 1), WVP);
    vout.Diffuse = Col;
	vout.TexCoord = vin.TexCoord;
	
    return vout;
}

VS_Output VSBasicTxVc(VS_InputTxVc vin)
{
    VS_Output vout;
    
    vout.Position = mul(float4(vin.Position, 1), WVP);
    vout.Diffuse = Col * vin.Color;
	vout.TexCoord = vin.TexCoord;

    return vout;
}

float4 PSBasic(VS_Output vs) : STRUCT_TARGET0
{
	return vs.Diffuse;
}

float4 PSBasicVc(VS_Output vs) : STRUCT_TARGET0
{
	return vs.Diffuse;
}

float4 PSBasicTx(VS_Output vs) : STRUCT_TARGET0
{
	return SAMPLE_TEXTURE(Txt, vs.TexCoord) * vs.Diffuse;
}

float4 PSBasicTxVc(VS_Output vs) : STRUCT_TARGET0
{
	return SAMPLE_TEXTURE(Txt, vs.TexCoord) * vs.Diffuse;
}

DECLARE_TECHNIQUE(Basic, VSBasic(), PSBasic())
DECLARE_TECHNIQUE(VertexColor, VSBasicVc(), PSBasicVc())
DECLARE_TECHNIQUE(Texture, VSBasicTx(), PSBasicTx())
DECLARE_TECHNIQUE(TextureAndVertexColor, VSBasicTxVc(), PSBasicTxVc())
