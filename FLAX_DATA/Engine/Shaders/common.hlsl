////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2012-2016, Wojciech Figat - www.celelej.com
// All rights reserved.
////////////////////////////////////////////////////////////////////////////////////

//NOTE: Testing copy of common.api shader (Will modify it and use it myslef :), obviously copyright will stay :D )

#if SM4

// DirectX 10 and DirectX 10.1 (using Shader Model 4.0 not 4.1)
#define DECLARE_TEXTURE(Name, index, filter, addressU, addressV, mipLODBias, maxAnisotropy) Texture2D Name : register(t##index); sampler Name##T2D : register(s##index) { AddressU = addressU; AddressV = addressV; MaxAnisotropy = maxAnisotropy; MipLODBias = mipLODBias; Filter = filter; };
#define DECLARE_CUBEMAP(Name, index, filter, addressU, addressV, addressW, mipLODBias, maxAnisotropy) TextureCube Name : register(t##index); SamplerState Name##T3D : register(s##index) { AddressU = addressU; AddressV = addressV; AddressW = addressW; MaxAnisotropy = maxAnisotropy; MipLODBias = mipLODBias; Filter = filter; };
#define SAMPLE_RT(Name, texCoord) Name.SampleLevel(Name##T2D, texCoord, 0.0)
#define SAMPLE_TEXTURE(Name, texCoord) Name.Sample(Name##T2D, texCoord)
#define LOAD_TEXTURE(Name, pos, lod) Name.Load(int3(pos, lod))
#define SAMPLE_TEXTURE_LEVEL(Name, texCoord, lod) Name.SampleLevel(Name##T2D, texCoord, lod)
#define SAMPLE_TEXTURE_OFFSET(Name, texCoord, lod, offset) Name.SampleLevel(Name##T2D, texCoord, lod, offset)
#define SAMPLE_TEXTURE_GRAD(Name, texCoord, ddx, ddy) Name.SampleGrad(Name##T2D, texCoord, ddx, ddy)
#define SAMPLE_TEXTURE_ARRAY(Name, texCoord, arrayIndex) Name.SampleLevel(Name##T2D, float3(texCoord, arrayIndex), 0)
#define SAMPLE_CUBEMAP(Name, texCoord) Name.Sample(Name##T3D, texCoord)
#define SAMPLE_CUBEMAP_LEVEL(Name, texCoord, lod) Name.SampleLevel(Name##T3D, texCoord, lod)
#define DECLARE_TECHNIQUE(Name, VS, PS) technique10 Name{ Pass P{SetVertexShader(CompileShader(vs_4_0,VS));SetPixelShader(CompileShader(ps_4_0,PS));}}
#define DECLARE_TECHNIQUE_GS(Name, VS, GS, PS) technique10 Name{ Pass P{SetVertexShader(CompileShader(vs_4_0,VS));SetGeometryShader(CompileShader(gs_4_0, GS));SetPixelShader(CompileShader(ps_4_0,PS));}}

#elif SM5

// DirectX 11
#define DECLARE_TEXTURE(Name, index, filter, addressU, addressV, mipLODBias, maxAnisotropy) Texture2D Name : register(t##index); sampler Name##T2D : register(s##index) { AddressU = addressU; AddressV = addressV; MaxAnisotropy = maxAnisotropy; MipLODBias = mipLODBias; Filter = filter; };
#define DECLARE_CUBEMAP(Name, index, filter, addressU, addressV, addressW, mipLODBias, maxAnisotropy) TextureCube Name : register(t##index); SamplerState Name##T3D : register(s##index) { AddressU = addressU; AddressV = addressV; AddressW = addressW; MaxAnisotropy = maxAnisotropy; MipLODBias = mipLODBias; Filter = filter; };
#define SAMPLE_RT(Name, texCoord) Name.SampleLevel(Name##T2D, texCoord, 0.0)
#define SAMPLE_TEXTURE(Name, texCoord) Name.Sample(Name##T2D, texCoord)
#define LOAD_TEXTURE(Name, pos, lod) Name.Load(int3(pos, lod))
#define SAMPLE_TEXTURE_LEVEL(Name, texCoord, lod) Name.SampleLevel(Name##T2D, texCoord, lod)
#define SAMPLE_TEXTURE_OFFSET(Name, texCoord, lod, offset) Name.SampleLevel(Name##T2D, texCoord, lod, offset)
#define SAMPLE_TEXTURE_GRAD(Name, texCoord, ddx, ddy) Name.SampleGrad(Name##T2D, texCoord, ddx, ddy)
#define SAMPLE_TEXTURE_ARRAY(Name, texCoord, arrayIndex) Name.SampleLevel(Name##T2D, float3(texCoord, arrayIndex), 0)
#define SAMPLE_CUBEMAP(Name, texCoord) Name.Sample(Name##T3D, texCoord)
#define SAMPLE_CUBEMAP_LEVEL(Name, texCoord, lod) Name.SampleLevel(Name##T3D, texCoord, lod)
#define DECLARE_TECHNIQUE(Name, VS, PS) technique11 Name{ Pass P{SetVertexShader(CompileShader(vs_5_0,VS));SetPixelShader(CompileShader(ps_5_0,PS));}}
#define DECLARE_TECHNIQUE_GS(Name, VS, GS, PS) technique11 Name{ Pass P{SetVertexShader(CompileShader(vs_5_0,VS));SetGeometryShader(CompileShader(gs_5_0, GS));SetPixelShader(CompileShader(ps_5_0,PS));}}

#define GATHER_TEXTURE(Name, texCoord) Name.GatherRed(Name##T2D, texCoord)
#define GATHER_TEXTURE_RED(Name, texCoord) Name.GatherRed(Name##T2D, texCoord)
#define GATHER_TEXTURE_GREEN(Name, texCoord) Name.GatherGreen(Name##T2D, texCoord)
#define GATHER_TEXTURE_BLUE(Name, texCoord) Name.GatherBlue(Name##T2D, texCoord)
#define GATHER_TEXTURE_OFFSET(Name, texCoord, col, row) Name.GatherRed(Name##T2D, texCoord, int2(col, row))
#define GATHER_TEXTURE_OFFSET_A(Name, texCoord, col, row) Name.GatherAlpha(Name##T2D, texCoord, int2(col, row))

#endif

// Some say she's beautiful, but I think she's weird
#define PI  3.1415926535897932f

// Check if Gather() function is supported
#if SM5
	#define CAN_USE_GATHER4 1
#else
	#define CAN_USE_GATHER4 0
#endif

// Shading model types
#define SHADING_MODEL_UNLIT  0
#define SHADING_MODEL_LIT    1

// GBuffer
DECLARE_TEXTURE(GBuffer0, 0, MIN_MAG_MIP_POINT, Clamp, Clamp, 0, 1);
DECLARE_TEXTURE(GBuffer1, 1, MIN_MAG_MIP_POINT, Clamp, Clamp, 0, 1);
DECLARE_TEXTURE(GBuffer2, 2, MIN_MAG_MIP_POINT, Clamp, Clamp, 0, 1);
DECLARE_TEXTURE(GBuffer3, 3, MIN_MAG_MIP_POINT, Clamp, Clamp, 0, 1);
DECLARE_TEXTURE(Depth,    4, MIN_MAG_MIP_POINT, Clamp, Clamp, 0, 1);

// x-width, y-height, z-projectionA (Far / (Far - Near), w-projectionB (-Far * Near) / (Far - Near) / Far)
float4 GBufferInfo;

// x-near plane, y-far plane, z-Near / Far, w-unused
float4 ViewInfo;

// x-width, y-height, z-1/width, w-1/height
float4 ScreenSize;

// View position
float3 Pos;

// Matricies
float4x4 IV;
float4x4 WVP;

// GBuffer data
struct GBuffer
{
	float3 Normal;
	float  Roughness;
	float3 WorldPos;
	float  Metallic;
	float3 BaseColor;
	float  Specular;
	float3 Emissive;
	float  AO;
	float3 ViewPos;
	int    ShadingModel;
};

struct ModelInput
{
	float3 Position  : POSITION;
	float2 TexCoord  : TEXCOORD0;
	float4 Normal    : NORMAL;
	float4 Tangent   : TANGENT;
};

struct ModelInputPosOnly
{
	float3 Position  : POSITION;
};

struct Quad_ToVs
{
	float3 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
};

struct Quad_VsToPs
{
	float4 Position : SV_Position;
	float2 TexCoord : TEXCOORD0;
};

struct Model_VsToPs
{
	float4 Position  : SV_Position;
	float4 ScreenPos : TEXCOORD0;
};

// Vertex Shader for screen space quad rendering
Quad_VsToPs VS_Quad(Quad_ToVs input)
{
	Quad_VsToPs output;
	
	output.Position = float4(input.Position, 1);
	output.TexCoord = input.TexCoord;
	
	return output;
}

// Vertex Shader for models rendering
Model_VsToPs VS_Model(ModelInputPosOnly input)
{
	Model_VsToPs output;
	
	output.Position = mul(float4(input.Position, 1), WVP);
	//output.Position = input.Position.x * WVP[0] + (input.Position.y * WVP[1] + (input.Position.z * WVP[2] + WVP[3]));
	output.ScreenPos = output.Position;
	
	return output;
}

// Calculate pixel depth in light space biased adaptively
// Cosine of the angle between the normal and the light direction, clamped above 0
//  - light is at the vertical of the triangle -> 10
//  - light is perpendiular to the triangle -> 0
//  - light is behind the triangle -> 0
//    float cosTheta = clamp(NoL, 0, 1);
//    float bias = clamp(0.005 * tan(acos(cosTheta)), 0, 0.01);
//    ldepth -= bias;

/** Assemble the transform from tangent space into world space
half3x3 AssembleTangentToWorld( half3 TangentToWorld0, half4 TangentToWorld2 )
{
	// Will not be orthonormal after interpolation. This perfectly matches xNormal.
	// Any mismatch with xNormal will cause distortions for baked normal maps.

	// Derive the third basis vector off of the other two.
	// Flip based on the determinant sign
	half3 TangentToWorld1 = cross(TangentToWorld2.xyz,TangentToWorld0) * TangentToWorld2.w;// TangentZ.w contains sign of tangent basis determinant
	// Transform from tangent space to world space
	return half3x3(TangentToWorld0, TangentToWorld1, TangentToWorld2.xyz);
}
*/

// Default Attenuation Factor
//#define AttenuationFactor 30
// Very natural light attenuation
  // Surface-to-light vector. Don't normalize, the attenuation function needs the distance.	
 //float3 L = LPos.xyz - wsPos.xyz;
//float attenuationFactor = AttenuationFactor;
//float attenuation = dot(L, L) / pow(LRad, 2);	
//attenuation = 1 / (attenuation * attenuationFactor + 1);
//attenuationFactor = 1/ (attenuationFactor + 1); 
//attenuation = max(attenuation - attenuationFactor, 0);
//attenuation /= 1 - attenuationFactor;

	/*
/// Helps to visualize the resolution of a texture for debugging. This function
/// can be used to draw grid lines between the texels or to draw a checkerboard
/// pattern.
/// \param[in] texCoord    The current texture coordinate.
/// \param[in] textureSize The size of the texture as float2(width, height).
/// \param[in] gridSize    The relative size of the grid lines in percent, for
///                        instance 0.02. If this value is 0 or negative a
///                        checkerboard pattern is created instead of a grid.
///  \return  0 if a dark pixel should be drawn for this texture coordinate;
///           otherwise 1.
float VisualizeTextureGrid(float2 texCoord, float2 textureSize, float gridSize)
{
  if (gridSize > 0)
  {
    float2 t = frac(texCoord * textureSize);
    return (t.x < gridSize || t.x > 1 - gridSize)
           || (t.y < gridSize || t.y > 1 - gridSize);
  }
  else
  {
    int2 t = int2(texCoord * textureSize);
    return (t.x % 2 == 0 && t.y % 2 == 0)
           || (t.x % 2 == 1 && t.y % 2 == 1);
  }
}*/


float Square(float x)
{
	return x * x;
}

// Clamp the base, so it's never <= 0.0f (INF/NaN).
float ClampedPow(float X,float Y)
{
	return pow(max(abs(X),0.000001f),Y);
}
float2 ClampedPow(float2 X,float2 Y)
{
	return pow(max(abs(X),float2(0.000001f,0.000001f)),Y);
}
float3 ClampedPow(float3 X,float3 Y)
{
	return pow(max(abs(X),float3(0.000001f,0.000001f,0.000001f)),Y);
}  
float4 ClampedPow(float4 X,float4 Y)
{
	return pow(max(abs(X),float4(0.000001f,0.000001f,0.000001f,0.000001f)),Y);
} 

float BiasedNDotL(float NoL)
{
	return saturate(NoL * 1.08f - 0.08f);
}

bool RayHitSphere(float3 R, float3 SphereCenter, float SphereRadius)
{
	float3 ClosestPointOnRay = max(0, dot(SphereCenter, R)) * R;
	float3 CenterToRay = ClosestPointOnRay - SphereCenter;
	return dot(CenterToRay, CenterToRay) <= (SphereRadius * SphereRadius);
}

bool RayHitRect(float3 R, float3 RectCenter, float3 RectX, float3 RectY, float3 RectZ, float RectExtentX, float RectExtentY )
{
	float3 PointOnPlane = R * max(0, dot(RectZ, RectCenter) / dot(RectZ, R));
	bool InExtentX = abs(dot(RectX, PointOnPlane - RectCenter)) <= RectExtentX;
	bool InExtentY = abs(dot(RectY, PointOnPlane - RectCenter)) <= RectExtentY;
	return InExtentX && InExtentY;
}

float Luminance(float3 color)
{
	return dot(color, float3(0.299f, 0.587f, 0.114f));
}

// Compact Normal Storage for Small G-Buffers
// [Aras Pranckevièius 2010, http://aras-p.info/texts/CompactNormalStorage.html]
float3 EncodeNormal(float3 n)
{
	// Pack to [0;1] range
	return n * 0.5 + 0.5;
	
	// Store X&Y, reconstruct Z
	//return float4(n.xy * 0.5 + 0.5, 0, 0);
	
	// Spheremap Transform
	//float p = sqrt(n.z * 8 + 8);
	//return float4(n.xy / p + 0.5, 0, 0);
	
	// Stereographic Projection
	//float scale = 1.7777;
	//float2 enc = n.xy / (n.z + 1);
	//enc /= scale;
	//enc = enc * 0.5 + 0.5;
	//return float4(enc, 0, 0);
}

// Compact Normal Storage for Small G-Buffers
// [Aras Pranckevièius 2010, http://aras-p.info/texts/CompactNormalStorage.html]
float3 DecodeNormal(float3 enc)
{
	// Unpack from [0;1] range
	return normalize(enc * 2 - 1);
	
	// Store X&Y, reconstruct Z
	//float3 n;
	//n.xy = enc * 2.0 - 1.0;
	//n.z = sqrt(1 - dot(n.xy, n.xy));
	//return n;
	
	// Spheremap Transform
	//float2 fenc = enc * 4 - 2;
	//float f = dot(fenc, fenc);
	//float g = sqrt(1 - f / 4);
	//float3 n;
	//n.xy = fenc * g;
	//n.z = 1 - f / 2;
	//return normalize(n);
	
	// Stereographic Projection
	//float scale = 1.7777;
	//float3 nn = float3(2 * scale * enc.xy - scale, 1);
	//float g = 2.0 / dot(nn.xyz , nn.xyz);
	//float3 n;
	//n.xy = g * nn.xy;
	//n.z = g - 1;
	//return normalize(n);
}

float2 EncodeR5G6B5(in float3 rgb24)
{
	rgb24 *= 255.0f;
	int3 rgb16 = rgb24 / int3(8, 4, 8);
	float greenSplit = rgb16.g / 8.0f;
	float2 packed;
	packed.x = rgb16.r * 8 + floor(greenSplit);
	packed.y = frac(greenSplit) * 256 + rgb16.b;
	packed /= 255.0f;
	return packed;
}

float3 DecodeR5G6B5(in float2 packed) 
{
	packed *= 255.0f;
	float2 split = round(packed) / 8;
	split.y /= 4;
	float3 rgb16 = 0.0f.rrr;
	rgb16.gb = frac(split) * 256;
	rgb16.rg += floor(split) * 4;
	rgb16.r *= 2;
	rgb16 /= 255.0f;
	return rgb16;
}

// Calculate material diffuse color
float3 GetDiffuseColor(in GBuffer gBuffer)
{
	return gBuffer.BaseColor - gBuffer.BaseColor * gBuffer.Metallic;
}

// Calculate material specular color
float3 GetSpecularColor(in GBuffer gBuffer)
{
	return lerp(0.08 * gBuffer.Specular.xxx, gBuffer.BaseColor.rgb, gBuffer.Metallic.xxx);
}

// Sample raw device depth buffer
float SampleZ(in float2 uv)
{
	// Sample depth buffer
	return SAMPLE_RT(Depth, uv).r;
}

// Linearize raw device depth
float LinearizeZ(in float depth)
{
	// Linearize depth
	return GBufferInfo.w / (depth - GBufferInfo.z);
}

// Sample linear depth
float SampleDepth(in float2 uv)
{
	// Sample depth buffer
	float depth = SAMPLE_RT(Depth, uv).r;
	
	// Linearize depth
	float eye_z = GBufferInfo.w / (depth - GBufferInfo.z);
	
	return eye_z;
}

// Get view space position at given pixel coordinate
float3 GetViewPos(in float2 uv)
{
	// Sample depth buffer
	float depth = SAMPLE_RT(Depth, uv).r;
	
	// Linearize depth
	float eye_z = GBufferInfo.w / (depth - GBufferInfo.z);
	
	// Calculate depth value
	eye_z *= ViewInfo.y;
	
	// Map uvs to clip space
	uv = uv * float2(2.0, -2.0) - float2(1.0, -1.0);
	
	// Calculate view position
	return float3(uv * GBufferInfo.xy * eye_z, eye_z);
}

// Get view space position at given pixel coordinate with given linear depth
float3 GetViewPos(in float2 uv, in float eye_z)
{
	// Calculate depth value
	eye_z *= ViewInfo.y;
	
	// Map uvs to clip space
	uv = uv * float2(2.0, -2.0) - float2(1.0, -1.0);
	
	// Calculate view position
	return float3(uv * GBufferInfo.xy * eye_z, eye_z);
}

// Sample normal vector with pixel shading model
float3 SammpleNormal(in float2 uv, out int ShadingModel)
{
	// Sample GBuffer
	float4 gBuffer1 = SAMPLE_RT(GBuffer1, uv);
	
	// Decode normal and shading model
	ShadingModel = (int)(gBuffer1.a * 3.999);
	return DecodeNormal(gBuffer1.rgb);
}

// Sample GBuffer (full - with emissive color etc.)
GBuffer SampleGBufferFull(float2 uv)
{
	GBuffer gBuffer;
	
	// Sample GBuffer
	float4 gBuffer0 = SAMPLE_RT(GBuffer0, uv);
	float4 gBuffer1 = SAMPLE_RT(GBuffer1, uv);
	float4 gBuffer2 = SAMPLE_RT(GBuffer2, uv);
	float4 gBuffer3 = SAMPLE_RT(GBuffer3, uv);
	
	// Decode base color and roughness
	gBuffer.BaseColor = gBuffer0.rgb;
	gBuffer.Roughness = gBuffer0.a;
	
	// Decode normal and shading model
	gBuffer.Normal = DecodeNormal(gBuffer1.rgb);
	gBuffer.ShadingModel = (int)(gBuffer1.a * 3.999);
	
	// Calculate view space and world space positions
	gBuffer.ViewPos = GetViewPos(uv);
	gBuffer.WorldPos = mul(float4(gBuffer.ViewPos, 1), IV).xyz;
	
	// Decode GBuffer data
	gBuffer.Metallic = gBuffer2.b;
	gBuffer.Emissive = gBuffer3.rgb / gBuffer2.r;
	gBuffer.Specular = gBuffer2.a;
	gBuffer.AO = gBuffer3.a;
	
	return gBuffer;
}

// Sample GBuffer
GBuffer SampleGBuffer(float2 uv)
{
	GBuffer gBuffer = (GBuffer)0;
	
	// Sample GBuffer
	float4 gBuffer0 = SAMPLE_RT(GBuffer0, uv);
	float4 gBuffer1 = SAMPLE_RT(GBuffer1, uv);
	float4 gBuffer2 = SAMPLE_RT(GBuffer2, uv);
	
	// Decode base color and roughness
	gBuffer.BaseColor = gBuffer0.rgb;
	gBuffer.Roughness = gBuffer0.a;
	
	// Decode normal and shading model
	gBuffer.Normal = DecodeNormal(gBuffer1.rgb);
	gBuffer.ShadingModel = (int)(gBuffer1.a * 3.999);
	
	// Calculate view space and world space positions
	gBuffer.ViewPos = GetViewPos(uv);
	gBuffer.WorldPos = mul(float4(gBuffer.ViewPos, 1), IV).xyz;
	
	// Decode GBuffer data
	gBuffer.Metallic = gBuffer2.b;
	gBuffer.Specular = gBuffer2.a;
	
	return gBuffer;
}

// Sample GBuffer (fast - only few parametrs are being sampled)
GBuffer SampleGBufferFast(float2 uv)
{
	GBuffer gBuffer = (GBuffer)0;
	
	// Sample GBuffer
	float4 gBuffer1 = SAMPLE_RT(GBuffer1, uv);
	
	// Decode normal and shading model
	gBuffer.Normal = DecodeNormal(gBuffer1.rgb);
	gBuffer.ShadingModel = (int)(gBuffer1.a * 3.999);
	
	// Calculate view space position
	gBuffer.ViewPos = GetViewPos(uv);
	gBuffer.WorldPos = mul(float4(gBuffer.ViewPos, 1), IV).xyz;
	
	return gBuffer;
}

// Sample GBuffer normal vector, shading model and view space position
GBuffer SampleGBufferNormalVPos(float2 uv)
{
	GBuffer gBuffer = (GBuffer)0;
	
	// Sample GBuffer
	float4 gBuffer1 = SAMPLE_RT(GBuffer1, uv);
	
	// Decode normal and shading model
	gBuffer.Normal = DecodeNormal(gBuffer1.rgb);
	gBuffer.ShadingModel = (int)(gBuffer1.a * 3.999);
	
	// Calculate view space position
	gBuffer.ViewPos = GetViewPos(uv);
	
	return gBuffer;
}