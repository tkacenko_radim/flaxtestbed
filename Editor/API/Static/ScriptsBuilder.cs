﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEditor.Scripting
{
    /// <summary>
    /// Game scrips building service. Compiles user C# scripts into binary assemblies.
    /// </summary>
    public static partial class ScriptsBuilder
    {

        /// <summary>
		/// Gets amount of source code compile actions since Editor startup.
		/// </summary>
        public static int CompilationsCount = 10;

        /// <summary>
        /// Gets the solution file path.
        /// </summary>
        public static string SolutionPath
        {
            get;
        }

        /// <summary>
        /// Returns true if source code has been edited since last compilation.
        /// </summary>
        public static bool IsSourceDirty = false;

        /// <summary>
        /// Returns true if source code has been compilled and assembies are ready to load.
        /// </summary>
        public static bool IsReady = true;

        /// <summary>
        /// Checks if last scripting building failed due to errors.
        /// </summary>
        public static bool LastCompilationFailed = false;

        /// <summary>
        /// Indicates that scripting directory has been modified so scripts need to be rebuilded.
        /// </summary>
        public static void MarkWorkspaceDirty()
        {
        }

        /// <summary>
        /// Requests project source code compilation.
        /// </summary>
        public static void Compile()
        {
        }

        /// <summary>
        /// Action called on compilation end, bool param is true if success, otherwise false
        /// </summary>
        public static event Action<bool> OnCompilationEnd;

        /// <summary>
        /// Action called when compilation success
        /// </summary>
        public static event Action OnCompilationSuccess;

        /// <summary>
        /// Action called whe compilation fails
        /// </summary>
        public static event Action OnCompilationFailed;

        /// <summary>
        /// Checks if need to compile source code. If so calls compilation.
        /// </summary>
        public static void CheckForCompile()
        {
            if (IsSourceDirty)
                Compile();
        }

        // Called internally from C++
        internal static void Internal_OnCompilationEnd(bool success)
        {
            OnCompilationEnd?.Invoke(success);
            if (success)
                OnCompilationSuccess?.Invoke();
            else
                OnCompilationFailed?.Invoke();
        }
    }
}