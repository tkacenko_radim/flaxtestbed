﻿using FlaxEngine.Rendering;
using System;
using static FlaxEngine.Win32;
using static FlaxEngine.Win32.PeekMessageParams;

namespace FlaxEngine
{
    class Program
    {

        public static bool Running = true;

        [STAThread]
        static void Main(string[] args)
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            Globals.Init();
            RenderingService.Initialize();

            SplashScreen.Show();

            //SplashScreen.Update(); //TODO: remove... need better message loop
            FlaxEditor.Editor e = new FlaxEditor.Editor();
            e.Init();
            MSG Msg = new MSG();
            Time.TimeScale = 1;
            Time.Update();
            e.Windows.MainWindow.OnClosed += () => { Running = false; }; //To temrminate message loop when we closed main window (closed the whole editor)
            // Main message loop: 
            //System.Windows.Forms.Application.Run();
            while (Running)
            {
                Time.Update();
                e.Update();
                ContentPool.Instance.Update();
                Scripting.Internal_Update();
                Scripting.Internal_LateUpdate();
                Scripting.Internal_FixedUpdate();

                if (PeekMessage(out Msg, IntPtr.Zero, 0, 0, PM_REMOVE))
                {
                    TranslateMessage(ref Msg);
                    DispatchMessage(ref Msg);
                }
                else
                {
                    //Render 3D
                    RenderingService.ExecuteRenderTasks();

                    //Render UI
                    foreach (var item in Window.ActiveWindows)
                    {
                        item.Internal_Process();
                    }
                    foreach (var item in Window.ActiveWindows)
                    {
                        item.Internal_Paint();
                    }
                }
            }
            Render2D.Dispose();
        }
    }
}