﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    [Serializable]
    public struct CommonValue
    {
        public object Value;

        public static CommonValue Null
        {
            get
            {
                return new CommonValue((string)null);
            }
        }

        public bool IsNull
        {
            get
            {
                return Value == null;
            }
        }

        public CommonValue(object value)
        {
            Value = value;
        }

        public CommonValue(bool value)
        {
            Value = value;
        }

        public CommonValue(byte value)
        {
            Value = value;
        }

        public CommonValue(byte[] value)
        {
            Value = value;
        }

        public CommonValue(char value)
        {
            Value = value;
        }

        public CommonValue(short value)
        {
            Value = value;
        }

        public CommonValue(ushort value)
        {
            Value = value;
        }

        public CommonValue(int value)
        {
            Value = value;
        }

        public CommonValue(uint value)
        {
            Value = value;
        }

        public CommonValue(float value)
        {
            Value = value;
        }

        public CommonValue(double value)
        {
            Value = value;
        }

        public CommonValue(Vector2 value)
        {
            Value = value;
        }

        public CommonValue(Vector3 value)
        {
            Value = value;
        }

        public CommonValue(Vector4 value)
        {
            Value = value;
        }

        public CommonValue(Quaternion value)
        {
            Value = value;
        }

        public CommonValue(Matrix value)
        {
            Value = value;
        }

        public CommonValue(TimeSpan value)
        {
            Value = value;
        }

        public CommonValue(Color value)
        {
            Value = value;
        }

        public CommonValue(Transform value)
        {
            Value = value;
        }

        public CommonValue(string value)
        {
            Value = value;
        }

        public CommonValue(BoundingBox value)
        {
            Value = value;
        }

        public CommonValue(Guid value)
        {
            Value = value;
        }

        public CommonValue(Vector4[] value)
        {
            Value = value;
        }

        public CommonType GetValueType()
        {
            if (Value == null)
            {
                return CommonType.Null;
            }
            if (Value is bool)
            {
                return CommonType.Bool;
            }
            if (Value is byte)
            {
                return CommonType.Byte;
            }
            if (Value is char)
            {
                return CommonType.Char;
            }
            if (Value is Color)
            {
                return CommonType.Color;
            }
            if (Value is double)
            {
                return CommonType.Double;
            }
            if (Value is float)
            {
                return CommonType.Float;
            }
            if (Value is int)
            {
                return CommonType.Int;
            }
            if (Value is uint)
            {
                return CommonType.UInt;
            }
            if (Value is short)
            {
                return CommonType.Short;
            }
            if (Value is ushort)
            {
                return CommonType.UShort;
            }
            if (Value is Matrix)
            {
                return CommonType.Matrix;
            }
            if (Value is Quaternion)
            {
                return CommonType.Quaternion;
            }
            if (Value is System.TimeSpan)
            {
                return CommonType.TimeSpan;
            }
            if (Value is Vector2)
            {
                return CommonType.Vector2;
            }
            if (Value is Vector3)
            {
                return CommonType.Vector3;
            }
            if (Value is Vector4)
            {
                return CommonType.Vector4;
            }
            if (Value is Transform)
            {
                return CommonType.Transform;
            }
            if (Value is string)
            {
                return CommonType.String;
            }
            if (Value is BoundingBox)
            {
                return CommonType.BoundingBox;
            }
            if (Value is byte[])
            {
                return CommonType.ByteArray;
            }
            if (Value is System.Guid)
            {
                return CommonType.Guid;
            }
            if (Value is Vector4[])
            {
                return CommonType.Vector4Array;
            }
            return CommonType.Unknown;
        }


        public static CommonValue InitFor(CommonType t)
        {
            switch (t)
            {
                case CommonType.Bool:
                    return new CommonValue(false);
                case CommonType.Byte:
                    return new CommonValue(0);
                case CommonType.Char:
                    return new CommonValue('\0');
                case CommonType.Short:
                    return new CommonValue(0);
                case CommonType.UShort:
                    return new CommonValue(0);
                case CommonType.Int:
                    return new CommonValue(0);
                case CommonType.UInt:
                    return new CommonValue(0u);
                case CommonType.Float:
                    return new CommonValue(0f);
                case CommonType.Double:
                    return new CommonValue(0.0);
                case CommonType.Vector2:
                    return new CommonValue(Vector2.Zero);
                case CommonType.Vector3:
                    return new CommonValue(Vector3.Zero);
                case CommonType.Vector4:
                    return new CommonValue(Vector4.Zero);
                case CommonType.Quaternion:
                    return new CommonValue(Quaternion.Identity);
                case CommonType.Matrix:
                    return new CommonValue(Matrix.Identity);
                case CommonType.TimeSpan:
                    return new CommonValue(System.TimeSpan.Zero);
                case CommonType.Color:
                    return new CommonValue(Color.Black);
                case CommonType.Transform:
                    return new CommonValue(default(Transform));
                case CommonType.String:
                    return new CommonValue(string.Empty);
                case CommonType.BoundingBox:
                    return new CommonValue(new BoundingBox(new Vector3(-1f), new Vector3(1f)));
                case CommonType.ByteArray:
                    return new CommonValue(new byte[0]);
                case CommonType.Guid:
                    return new CommonValue(System.Guid.Empty);
                default:
                    throw new System.ArgumentException("Invalid CommonValue type.");
            }
        }

        public static implicit operator CommonValue(bool v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(byte v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(byte[] v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(char v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Color v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(float v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(int v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Matrix v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Quaternion v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(short v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(System.TimeSpan v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(uint v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(ushort v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Vector2 v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Vector3 v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Vector4 v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(double v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Transform v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(string v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(BoundingBox v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(System.Guid v)
        {
            return new CommonValue(v);
        }

        public static implicit operator CommonValue(Vector4[] v)
        {
            return new CommonValue(v);
        }
    }
}