﻿using SharpDX.DirectWrite;
using SharpDX.Mathematics.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    public class Font : Object
    {
        private string _family;
        private int _size;
        private TextFormat _textFormat;
        private int _height;

        /// <summary>
        /// Check if object exists
        /// </summary>
        /// <param name="obj">Object to check</param>
        public static implicit operator bool(Font obj)
        {
            return obj != null;
        }

        /// <summary>
        /// Gets font size.
        /// </summary>
        public int Size
        {
            get { return _size; }
        }

        /// <summary>
        /// Gets characters height.
        /// </summary>
        public int Height
        {
            get { return _height; }
        }

        /// <summary>
        /// Gets the largest vertical distance above the baseline for any character in the font.
        /// </summary>
        public int Ascender
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets the largest vertical distance below the baseline for any character in the font.
        /// </summary>
        public int Descender
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets the line gap property.
        /// </summary>
        public int LineGap
        {
            get { return 0; }
        }

        private Font() : base()
        {

        }

        public Font(string family, int size)
        {
            size = (int)(size / (72.0 / 96.0)); //PT to PX cause D2D1 takes size in px
            _size = size;
            _family = family;
            if (_textFormat != null)
            {
                _textFormat.Dispose();
                _textFormat = null;
            }
            _textFormat = new TextFormat(GPUContext.FactoryDirectWrite, _family, size);
            var tl = new TextLayout(GPUContext.FactoryDirectWrite, "", _textFormat, 1000000, 1000000, 1, false);
            _height = (int)tl.Metrics.Height;
            tl.Dispose();
        }

        ~Font()
        {
            _textFormat.Dispose();
        }

        public static implicit operator TextFormat (Font f)
        {
            return f._textFormat;
        }
        /// <summary>
        /// Measures minimum size of the rectangle that will be needed to draw given text.
        /// </summary>
        /// <param name="text">Input text.</param>
        /// <returns>Minimum size for that text and fot to render properly.</returns>
        public Vector2 MeasureText(string text)
        {
            var tl = new TextLayout(GPUContext.FactoryDirectWrite, text ?? "", _textFormat, 1000000, 1000000, 1, false);
            return new Vector2(tl.Metrics.Width, tl.Metrics.Height);
        }


        /// <summary>
        /// Calculates hited character index at given location.
        /// </summary>
        /// <param name="text">Input text to test.</param>
        /// <param name="location">Location to test.</param>
        /// <param name="layout">Layout properties.</param>
        /// <returns>Selected character position index (can be equal to text length if location is outside of the layout rectangle).</returns>
        public int HitTestText(string text, Vector2 location, TextLayoutOptions layout)
        {
            TextLayout tl = new TextLayout(GPUContext.FactoryDirectWrite, text, _textFormat, layout.Bounds.Width, layout.Bounds.Height, 1, false);

            SetupTextLayout(ref tl, ref layout);

            HitTestMetrics htm = tl.HitTestPoint(location.X, location.Y, out RawBool trailingHit, out RawBool isInside);
            tl.Dispose();
            return isInside ? htm.TextPosition : text.Length;
        }

        public static void SetupTextFormat(ref TextFormat tl, TextAlignment HorizontalAlignment, TextAlignment VerticalAlignment, TextWrapping TextWrapping)
        {
            switch (HorizontalAlignment)
            {
                case TextAlignment.Near:
                    tl.TextAlignment = SharpDX.DirectWrite.TextAlignment.Leading;
                    break;
                case TextAlignment.Center:
                    tl.TextAlignment = SharpDX.DirectWrite.TextAlignment.Center;
                    break;
                case TextAlignment.Far:
                    tl.TextAlignment = SharpDX.DirectWrite.TextAlignment.Trailing;
                    break;
            }

            switch (VerticalAlignment)
            {
                case TextAlignment.Near:
                    tl.ParagraphAlignment = ParagraphAlignment.Near;
                    break;
                case TextAlignment.Center:
                    tl.ParagraphAlignment = ParagraphAlignment.Center;
                    break;
                case TextAlignment.Far:
                    tl.ParagraphAlignment = ParagraphAlignment.Far;
                    break;
                default:
                    break;
            }

            switch (TextWrapping)
            {
                case TextWrapping.NoWrap:
                    tl.WordWrapping = WordWrapping.NoWrap;
                    break;
                case TextWrapping.WrapWords:
                    tl.WordWrapping = WordWrapping.WholeWord;
                    break;
                case TextWrapping.WrapChars:
                    tl.WordWrapping = WordWrapping.Character;
                    break;
            }
        }

        public static void SetupTextLayout(ref TextLayout tl, ref TextLayoutOptions layout)
        {
            switch (layout.HorizontalAlignment)
            {
                case TextAlignment.Near:
                    tl.TextAlignment = SharpDX.DirectWrite.TextAlignment.Leading;
                    break;
                case TextAlignment.Center:
                    tl.TextAlignment = SharpDX.DirectWrite.TextAlignment.Center;
                    break;
                case TextAlignment.Far:
                    tl.TextAlignment = SharpDX.DirectWrite.TextAlignment.Trailing;
                    break;
            }

            switch (layout.VerticalAlignment)
            {
                case TextAlignment.Near:
                    tl.ParagraphAlignment = ParagraphAlignment.Near;
                    break;
                case TextAlignment.Center:
                    tl.ParagraphAlignment = ParagraphAlignment.Center;
                    break;
                case TextAlignment.Far:
                    tl.ParagraphAlignment = ParagraphAlignment.Far;
                    break;
                default:
                    break;
            }

            switch (layout.TextWrapping)
            {
                case TextWrapping.NoWrap:
                    tl.WordWrapping = WordWrapping.NoWrap;
                    break;
                case TextWrapping.WrapWords:
                    tl.WordWrapping = WordWrapping.WholeWord;
                    break;
                case TextWrapping.WrapChars:
                    tl.WordWrapping = WordWrapping.Character;
                    break;
            }
        }

        /// <summary>
        /// Calculates hited character index at given location.
        /// </summary>
        /// <param name="text">Input text to test.</param>
        /// <param name="location">Location to test.</param>
        /// <returns>Selected character position index (can be equal to text length if location is outside of the layout rectangle).</returns>
        public int HitTestText(string text, Vector2 location)
        {
            TextLayout tl = new TextLayout(GPUContext.FactoryDirectWrite, text, _textFormat, 1000000, 1000000, 1, false);


            HitTestMetrics htm = tl.HitTestPoint(location.X, location.Y, out RawBool trailingHit, out RawBool isInside);
            tl.Dispose();
            return isInside ? htm.TextPosition : text.Length;
        }

        internal Vector2 GetCharPosition(string _text, int selectionLeft, TextLayoutOptions layout)
        {
            try
            {
                TextLayout tl = new TextLayout(GPUContext.FactoryDirectWrite, _text, _textFormat, layout.Bounds.Width, layout.Bounds.Height, 1, false);

                SetupTextLayout(ref tl, ref layout);

                HitTestMetrics htm = tl.HitTestTextPosition(selectionLeft, false, out float x, out float y);
                tl.Dispose();
                return new Vector2(x + layout.Bounds.X, y + layout.Bounds.Y);
            }
            catch
            {
                return Vector2.Zero;
            }
        }
    }
}
