﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaxEngine.Rendering.ShaderConstants;

using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.Direct3D;
using SharpDX;
using FlaxEngine.Rendering.DirectX11.ShaderConstants;

namespace FlaxEngine.Rendering.DirectX11
{
    internal class IncludeFX : Include
    {
        public IDisposable Shadow
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        private string BasePath;

        public IncludeFX(string basePath)
        {
            BasePath = basePath;
        }

        public Stream Open(IncludeType type, string fileName, Stream parentStream)
        {
            return new MemoryStream(File.ReadAllBytes(Path.Combine(BasePath, fileName + ".hlsl")));
        }

        public void Close(Stream stream)
        {
            stream.Close();
        }

        public void Dispose()
        {
        }
    }

    public sealed class ShaderDX11 : Shader, IDisposable
    {
        private static readonly ShaderMacro[] Macros = new ShaderMacro[]
        {
            new ShaderMacro("SM5", "1"),
            new ShaderMacro("STRUCT_TARGET0", "SV_Target0"),
            new ShaderMacro("STRUCT_TARGET1", "SV_Target1"),
            new ShaderMacro("STRUCT_TARGET2", "SV_Target2"),
            new ShaderMacro("STRUCT_TARGET3", "SV_Target3"),
            new ShaderMacro(null, null)
        };

        private static readonly InputElement[][] InputLayoutsTypes = new InputElement[][]
        {
            new InputElement[]
            {
                new InputElement
                {
                    SemanticName = "POSITION",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                }
            },
            new InputElement[]
            {
                new InputElement
                {
                    SemanticName = "POSITION",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "TEXCOORD",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32A32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                }
            },
            new InputElement[]
            {
                new InputElement
                {
                    SemanticName = "POSITION",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "TEXCOORD",
                    SemanticIndex = 0,
                    Format = Format.R32G32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                }
            },
            new InputElement[]
            {
                new InputElement
                {
                    SemanticName = "POSITION",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "TEXCOORD",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32A32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "TEXCOORD",
                    SemanticIndex = 1,
                    Format = Format.R32G32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                }
            },
            new InputElement[]
            {
                new InputElement
                {
                    SemanticName = "POSITION",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "NORMAL",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                }
            },
            new InputElement[]
            {
                new InputElement
                {
                    SemanticName = "POSITION",
                    SemanticIndex = 0,
                    Format = Format.R32G32B32_Float,
                    Slot = 0,
                    AlignedByteOffset = 0,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "TEXCOORD",
                    SemanticIndex = 0,
                    Format = Format.R16G16_Float,
                    Slot = 1,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "NORMAL",
                    SemanticIndex = 0,
                    Format = Format.R10G10B10A2_UNorm,
                    Slot = 1,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                },
                new InputElement
                {
                    SemanticName = "TANGENT",
                    SemanticIndex = 0,
                    Format = Format.R10G10B10A2_UNorm,
                    Slot = 1,
                    AlignedByteOffset = InputElement.AppendAligned,
                    Classification = InputClassification.PerVertexData,
                    InstanceDataStepRate = 0
                }
            }
        };


        public GPUContextDX11 Context;

        private string IncludePath;
        private string Source;

        private Effect _effect;
        private InputLayout[] _inputLayouts;
        private EffectPass[] _techniquesPasses;

        private ShaderDX11() : base()
        {

        }

        public ShaderDX11(GPUContextDX11 context) : base(context)
        {
            Context = context;
        }

        public override void Apply(int techniqueIndex)
        {
            Context.DeviceContext.InputAssembler.InputLayout = _inputLayouts[techniqueIndex];
            _techniquesPasses[techniqueIndex].Apply(Context.DeviceContext);
        }

        public override void CreateInputLayout(int index, InputLayoutType type)
        {
            if(index < 0 || index > _inputLayouts.Length)
            {
                Debug.LogErrorFormat("Invalid technique index: {0}.", index);
            }

            //Clear if exists
            if (_inputLayouts[index] != null)
            {
                _inputLayouts[index].Dispose();
                _inputLayouts[index] = null;
            }

            EffectPass pass = _techniquesPasses[index];
            if(pass == null || !pass.IsValid || !pass.Description.HasSignature)
            {
                Debug.LogErrorFormat("Technique: {0} has invalid pass.", index);
            }

            try
            {
                ShaderSignature signature = ShaderSignature.GetInputSignature(pass.Description.Signature);
                _inputLayouts[index] = new InputLayout(Context.Device, signature, InputLayoutsTypes[(int)type]);
            }
            catch (Exception ex)
            {
                Debug.LogException(new Exception("Cannot create InputLayout.", ex));
            }
        }

        public override ShaderConstantMatrix GetMatrixConstant(string name)
        {
            ShaderConstantMatrixDX11 newConst = null;
            if (_constants.TryGetValue(name, out ShaderConstant constant))
            {
                return (ShaderConstantMatrix)constant;
            }
            try
            {
                EffectVariable var = _effect.GetVariableByName(name);
                if (var == null || !var.IsValid)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' doesn't exist."));
                }
                EffectMatrixVariable matrixResource = var.AsMatrix();
                if (matrixResource == null)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' is not matrix."));
                }
                newConst = new ShaderConstantMatrixDX11(this, matrixResource);
                _constants.Add(name, newConst);
            }
            catch (Exception ex)
            {
                Debug.LogException(new Exception("Invalid name", ex));
            }
            return newConst;
        }

        public override ShaderConstantResource GetResourceConstant(string name)
        {
            ShaderConstantResourceDX11 newConst = null;
            if (_constants.TryGetValue(name, out ShaderConstant constant))
            {
                return (ShaderConstantResource)constant;
            }
            try
            {
                EffectVariable var = _effect.GetVariableByName(name);
                if (var == null || !var.IsValid)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' doesn't exist."));
                }
                EffectShaderResourceVariable resourceVar = var.AsShaderResource();
                if (resourceVar == null)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' is not resource."));
                }
                newConst = new ShaderConstantResourceDX11(this, resourceVar);
                _constants.Add(name, newConst);
            }
            catch (Exception ex)
            {
                Debug.LogException(new Exception("Invalid name", ex));
            }
            return newConst;
        }

        public override ShaderConstantScalar GetScalarConstant(string name)
        {
            ShaderConstantScalarDX11 newConst = null;
            if (_constants.TryGetValue(name, out ShaderConstant constant))
            {
                return (ShaderConstantScalar)constant;
            }
            try
            {
                EffectVariable var = _effect.GetVariableByName(name);
                if(var == null || !var.IsValid)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' doesn't exist."));
                }
                EffectScalarVariable scalarVar = var.AsScalar();
                if(scalarVar == null)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' is not scalar."));
                }
                newConst = new ShaderConstantScalarDX11(this, scalarVar);
                _constants.Add(name, newConst);
            }
            catch (Exception ex)
            {
                Debug.LogException(new Exception("Invalid name",ex));
            }
            return newConst;
        }

        public override ShaderConstantVector GetVectorConstant(string name)
        {
            ShaderConstantVectorDX11 newConst = null;
            if (_constants.TryGetValue(name, out ShaderConstant constant))
            {
                return (ShaderConstantVector)constant;
            }
            try
            {
                EffectVariable var = _effect.GetVariableByName(name);
                if (var == null || !var.IsValid)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' doesn't exist."));
                }
                EffectVectorVariable vectorVar = var.AsVector();
                if (vectorVar == null)
                {
                    Debug.LogException(new Exception("Constant '" + name + "' is not vector."));
                }
                newConst = new ShaderConstantVectorDX11(this, vectorVar);
                _constants.Add(name, newConst);
            }
            catch (Exception ex)
            {
                Debug.LogException(new Exception("Invalid name", ex));
            }
            return newConst;
        }

        public override void Load(string path)
        {
            Source = File.ReadAllText(path);

            IncludePath = Path.GetDirectoryName(path);

            using (CompilationResult compilationResult = ShaderBytecode.Compile(Source, "fx_5_0", ShaderFlags.OptimizationLevel3, EffectFlags.None, Macros, new IncludeFX(IncludePath), path))
            {
                if (compilationResult.HasErrors)
                {
                    Debug.LogException(new Exception(string.Format("Cannot compile shader. Result: {0}. {1}", compilationResult.ResultCode, compilationResult.Message)));
                }
                _effect = new Effect(Context.Device, compilationResult.Bytecode, EffectFlags.None, path);
            }
            cacheILTP();
        }

        private void cacheILTP()
        {
            int techniqueCount = _effect.Description.TechniqueCount;
            if (techniqueCount == 0)
            {
                Debug.LogException(new Exception("No techniques."));
            }
            _inputLayouts = new InputLayout[techniqueCount];
            _techniquesPasses = new EffectPass[techniqueCount];
            for (int i = 0; i < techniqueCount; i++)
            {
                using (EffectTechnique technique = _effect.GetTechniqueByIndex(i))
                {
                    if (technique != null && technique.IsValid)
                    {
                        //TODO: Probably support multiple passes :)
                        EffectPass pass = technique.GetPassByIndex(0);
                        if (pass != null && pass.IsValid)
                        {
                            _techniquesPasses[i] = pass;
                        }
                        else
                        {
                            Debug.LogErrorFormat("Technique: {0} has invalid pass.", i);
                        }
                    }
                    else
                    {
                        Debug.LogErrorFormat("Invalid technique index: {0}.", i);
                    }
                }
            }
        }

        public override void Optimize()
        {
            _effect.Optimize();
        }

        public void Dispose()
        {
            Disposer.Dispose(ref _effect);
        }
    }
}
