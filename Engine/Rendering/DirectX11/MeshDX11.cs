﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
namespace FlaxEngine.Rendering.DirectX11
{
    public sealed class MeshDX11 : Mesh
    {
        private GPUContextDX11 Context;
        private D3D11.Buffer _vertexBuffer0;
        private D3D11.Buffer _vertexBuffer1;
        private D3D11.Buffer _indexBuffer;
        private D3D11.VertexBufferBinding[] _binding;
        private DXGI.Format _indexBufferFormat;
        private int _indexCount;

        public MeshDX11(Model model, int lodIndex, int meshIndex) : base(model, lodIndex, meshIndex)
        {
            Context = (GPUContextDX11)RenderingService.Context;
        }

        ~MeshDX11()
        {
            Dispose();
        }

        internal override void Load(Stream data)
        {
            base.Load(data);

            int bufferSize = _vertices * 12; //sizeof(Vector3);
            byte[] rawBuffer = new byte[bufferSize];

            data.Read(rawBuffer, 0, bufferSize);

            RenderingService.Context.AddLocker();
            try
            {
                _vertexBuffer0 = D3D11.Buffer.Create(Context.Device, D3D11.BindFlags.VertexBuffer, rawBuffer, bufferSize, structureByteStride: 12);

                data.Read(rawBuffer, 0, bufferSize);

                _vertexBuffer1 = D3D11.Buffer.Create(Context.Device, D3D11.BindFlags.VertexBuffer, rawBuffer, bufferSize, structureByteStride: 12);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            finally
            {
                RenderingService.Context.RemoveLocker();
            }

            Box = data.ReadBoundingBox();

            _indexCount = _faces * 3;
            int elementSize = use16BitIndices ? 2 : 4; //sizeof(ushort) : sizeof(uint);
            _indexBufferFormat = use16BitIndices ? DXGI.Format.R16_UInt : DXGI.Format.R32_UInt;
            int indexBufferSize = _indexCount * elementSize;

            byte[] rawIndices = new byte[indexBufferSize];
            data.Read(rawIndices, 0, indexBufferSize);

            RenderingService.Context.AddLocker();
            try
            {
                _indexBuffer = D3D11.Buffer.Create(Context.Device, D3D11.BindFlags.VertexBuffer, rawIndices, indexBufferSize);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            finally
            {
                RenderingService.Context.RemoveLocker();
            }

            _binding = new D3D11.VertexBufferBinding[]
            {
                new D3D11.VertexBufferBinding(_vertexBuffer0, 12, 0),
                new D3D11.VertexBufferBinding(_vertexBuffer1, 12, 0)
            };

        }

        internal override void Draw()
        {
            D3D11.InputAssemblerStage assembler = Context.DeviceContext.InputAssembler;
            assembler.SetVertexBuffers(0, _binding);
            assembler.SetIndexBuffer(_indexBuffer, _indexBufferFormat, 0);
            assembler.PrimitiveTopology = SharpDX.Direct3D.PrimitiveTopology.TriangleList;
            Context.DeviceContext.DrawIndexed(_indexCount, 0, 0);
        }

        public override void Dispose()
        {
            Disposer.Dispose(ref _vertexBuffer0);
            Disposer.Dispose(ref _vertexBuffer1);
            Disposer.Dispose(ref _indexBuffer);
        }
    }
}
