﻿using SharpDX.Direct3D11;

using D3D11 = SharpDX.Direct3D11;
using D2D1 = SharpDX.Direct2D1;
using DW = SharpDX.DirectWrite;
using System;
using static FlaxEngine.Rendering.RenderTask;

namespace FlaxEngine.Rendering.DirectX11
{
    public sealed class GPUContextDX11 : GPUContext, IDisposable
    {
        internal D3D11.Device Device;
        internal D3D11.DeviceContext DeviceContext;

        private readonly Adapter _adapter;
        private readonly object _deviceLocker;

        public override Adapter Adapter => _adapter;
        public BasicShader shader;
        public GPUContextDX11() : base()
        {
            _deviceLocker = new object();
            FactoryDirect2D1 = new D2D1.Factory();
            FactoryDirectWrite = new DW.Factory(DW.FactoryType.Shared);

            _adapter = Adapter.FindBest();

            Device = new D3D11.Device(Adapter.AdapterDXGI, D3D11.DeviceCreationFlags.BgraSupport);
            DeviceContext = Device.ImmediateContext;

            Render2D.Renderer = new Render2DDX11();
        }
        
        public override void Clear(RenderTarget rt, Color color)
        {
            DeviceContext.ClearRenderTargetView((RenderTargetView)rt.Handle.rt, new SharpDX.Mathematics.Interop.RawColor4(color.R, color.G, color.B, color.A));
        }

        public override void ClearDepth(RenderTarget depthBuffer, float depthValue = 1)
        {
            DeviceContext.ClearDepthStencilView((DepthStencilView)depthBuffer.Handle.rt,DepthStencilClearFlags.Depth, depthValue, 0);
        }

        public override void DrawScene(RenderTask task, RenderTarget output, RenderBuffers buffers, RenderView view, ViewFlags flags, ViewMode mode, Actor[] customActors)
 	    {

            var rtv = (RenderTargetView)output.Handle.rt;

            DeviceContext.OutputMerger.SetRenderTargets(rtv);
            DeviceContext.Rasterizer.SetViewport(0, 0, output.Width, output.Height);
            Color clearColor = Color.Black;
            clearColor.A = 1;
            Clear(output, clearColor);

            DrawCall[] calls = task.Internal_Draw();
            

            var c = DeviceContext.Rasterizer.GetViewports<Viewport>();

            view.SetProjector(output.Width / (float)output.Height, 0.1f, 1000f, view.Position, view.Direction, Vector3.Up, 75);

            if(shader == null)
            {
                shader = new BasicShader();
            }

            if (customActors != null)
                foreach (Actor actor in customActors)
                {
                    if (actor as ModelActor)
                    {
                        
                        ModelActor ma = actor as ModelActor;
                        shader.World = Matrix.RotationQuaternion(ma.Transform.Orientation) * Matrix.Scaling(ma.Transform.Scale) * Matrix.Translation(ma.Transform.Translation); // Model
                        shader.Apply(view);
                        ma.Model.Draw();
                    }
                }

            if(calls != null)
                foreach (DrawCall call in calls)
                {
                    shader.World = call.World * Matrix.Scaling(10);
                    shader.Apply(view);

                    call.AssetModel.LODs[call.LodIndex].Meshes[call.MeshIndex].Draw();
                }
        }

        internal override RenderTarget CreateRenderTarget()
        {
            return new RenderTargetDX11(this);
        }

        internal override WindowRenderTarget CreateWindowRenderTarget(Window window)
        {
            return new WindowRenderTargetDX11(window);
        }

        internal override Mesh CreateMesh(Model model, int lodIndex, int meshIndex)
        {
            return new MeshDX11(model, lodIndex, meshIndex);
        }

        public void Dispose()
        {
            shader.Dispose();
        }

        internal override Shader CreateShader(string path)
        {
            ShaderDX11 shader = new ShaderDX11(this);
            shader.Load(path);
            return shader;
        }
    }
}
