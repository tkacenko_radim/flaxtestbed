﻿using System;

using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using D3D = SharpDX.Direct3D;

namespace FlaxEngine.Rendering.DirectX11
{
    public sealed class RenderTargetDX11 : RenderTarget
    {
        private GPUContextDX11 _context;
        private D3D11.Texture2D _texture;
        private D3D11.Texture2D _cpuReadBuffer;
        private D3D11.RenderTargetView _view;
        private D3D11.ShaderResourceView _sr;

        private int _width;
        private int _height;
        private PixelFormat _format;
        private bool _allocated = false;
        private RenderTargetHandle _handle;

        internal RenderTargetDX11(GPUContextDX11 context) : base()
        {
            _context=  context;
        }

        public override RenderTargetHandle Handle => _handle;
        public override PixelFormat Format => _format;
        public override bool IsAllocated => _allocated;
        public override int Width => _width;
        public override int Height => _height;
        public override Vector2 Size => new Vector2(_width, _height);
        public override bool IsPowerOfTwo => Mathf.IsPowerOfTwo(Width) && Mathf.IsPowerOfTwo(Height); 

        public D3D11.Texture2D GetCPUReadBuffer()
        {
            if (_cpuReadBuffer == null)
            {
                createCPUReadBuffer();
            }

            //Sync texture and cpu read buffer
            ((GPUContextDX11)RenderingService.Context).DeviceContext.CopyResource(_texture, _cpuReadBuffer);

            return _cpuReadBuffer;
        }

        private void createCPUReadBuffer()
        {
            //CPU Read buffer

            D3D11.Device device = ((GPUContextDX11)RenderingService.Context).Device;

            D3D11.Texture2DDescription description = default(D3D11.Texture2DDescription);
            description.Format = (DXGI.Format)_format;
            description.ArraySize = 1;
            description.MipLevels = 1;
            description.Width = _width;
            description.Height = _height;
            description.SampleDescription = new DXGI.SampleDescription(1, 0);
            description.Usage = D3D11.ResourceUsage.Staging;
            description.BindFlags = D3D11.BindFlags.None;
            description.CpuAccessFlags = D3D11.CpuAccessFlags.Read;
            description.OptionFlags = D3D11.ResourceOptionFlags.None;
            _cpuReadBuffer = new D3D11.Texture2D(device, description);
        }

        public override void Init(PixelFormat format, int width, int height, TextureFlags flags = TextureFlags.ShaderResource | TextureFlags.RenderTarget)
        {
            if (width == _width && height == _height)
                return;

            release();

            _width = width;
            _height = height;
            _format = format;
            try
            {
                D3D11.Device device = ((GPUContextDX11)RenderingService.Context).Device;

                //Texture
                D3D11.Texture2DDescription description = default(D3D11.Texture2DDescription);
                description.Format = (DXGI.Format)format;
                description.ArraySize = 1;
                description.MipLevels = 1;
                description.Width = _width;
                description.Height = _height;

                description.SampleDescription = new DXGI.SampleDescription(1, 0);
                description.Usage = D3D11.ResourceUsage.Default;
                description.BindFlags = flags.ToBindFlags(); //TODO: Better way than hardcoding (probably change the values in TextureFlags???)
                description.CpuAccessFlags = D3D11.CpuAccessFlags.None;
                description.OptionFlags = D3D11.ResourceOptionFlags.None;

                _texture = new D3D11.Texture2D(device, description);

                //TODO: If Depth format thna instead of RTV create DSV or split this back to Texture, DepthTexture, RenderTarget, RenderTargetCube 

                //RTV
                D3D11.RenderTargetViewDescription rtvDescription = default(D3D11.RenderTargetViewDescription);
                rtvDescription.Format = (DXGI.Format)format;
                rtvDescription.Texture2D.MipSlice = 0;
                rtvDescription.Dimension = D3D11.RenderTargetViewDimension.Texture2D;

                _view = new D3D11.RenderTargetView(device, _texture, rtvDescription);

                //SRV

                D3D11.ShaderResourceViewDescription srvDescription = default(D3D11.ShaderResourceViewDescription);
                srvDescription.Format = (DXGI.Format)format;
                srvDescription.Texture2D.MostDetailedMip = 0;
                srvDescription.Texture2D.MipLevels = 1;
                srvDescription.Dimension = D3D.ShaderResourceViewDimension.Texture2D;

                _sr = new D3D11.ShaderResourceView(device, _texture, srvDescription);

                //Handle
                _handle = new RenderTargetHandle(_view, _sr);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }
            _allocated = true;
        }

        private void release()
        {
            _allocated = false;
            Disposer.Dispose(ref _view);
            Disposer.Dispose(ref _sr);
            Disposer.Dispose(ref _texture);
            Disposer.Dispose(ref _cpuReadBuffer);
        }

        public override void Dispose()
        {
            release();
        }
    }
}
