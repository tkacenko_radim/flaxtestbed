﻿using System.Collections.Generic;

using WIC = SharpDX.WIC;
using DXGI = SharpDX.DXGI;
using D2D1 = SharpDX.Direct2D1;
using D2DE = SharpDX.Direct2D1.Effects;
using DW = SharpDX.DirectWrite;
using SharpDX.Mathematics.Interop;

namespace FlaxEngine.Rendering.DirectX11
{
    internal sealed class Render2DDX11 : IRender2D
    {
        internal static WindowRenderTargetDX11 RenderTarget;
        internal static D2D1.DeviceContext Curent => RenderTarget?._d2dContext;

        internal static Dictionary<Color, D2D1.Brush> BrushCache = new Dictionary<Color, D2D1.Brush> { };
        internal static Dictionary<WIC.BitmapSource, D2D1.Bitmap> BitmapCache = new Dictionary<WIC.BitmapSource, D2D1.Bitmap> { };

        public Vector2 Transform
        {
            get { return new Vector2(Curent.Transform.M31, Curent.Transform.M32); }
            set
            {
                Curent.Transform = new RawMatrix3x2(Curent.Transform.M11, Curent.Transform.M12, Curent.Transform.M21, Curent.Transform.M22, value.X, value.Y);
            }
        }

        public Rectangle ClipMask { get; private set; }

        public void Dispose()
        {
            foreach (D2D1.Bitmap b in BitmapCache.Values)
            {
                b.Dispose();
            }
            BitmapCache.Clear();

            foreach (D2D1.Brush b in BrushCache.Values)
            {
                b.Dispose();
            }
            BrushCache.Clear();
        }

        public void DrawLine(Vector2 p1, Vector2 p2, Color color, float thickness = 1, bool withAlpha = false)
        {
            D2D1.Brush brush;

            if (!BrushCache.ContainsKey(color))
            {
                brush = new D2D1.SolidColorBrush(Curent, new RawColor4(color.R, color.G, color.B, color.A));
                BrushCache.Add(color, brush);
            }
            else
            {
                brush = BrushCache[color];
            }
            Curent.DrawLine(new RawVector2(p1.X, p1.Y), new RawVector2(p2.X, p2.Y), brush, thickness);
        }

        public void DrawRectangle(Rectangle rect, Color color, bool withAlpha = false, float thickness = 1)
        {
            D2D1.Brush brush;

            if (!BrushCache.ContainsKey(color))
            {
                brush = new D2D1.SolidColorBrush(Curent, new RawColor4(color.R, color.G, color.B, color.A));
                BrushCache.Add(color, brush);
            }
            else
            {
                brush = BrushCache[color];
            }
            Curent.DrawRectangle(new RawRectangleF(rect.Left, rect.Top, rect.Right, rect.Bottom), brush, thickness);
        }

        public void DrawRenderTarget(RenderTarget rt, Rectangle rect, Color color, bool withAlpha = false)
        {
            using (DXGI.Surface _cpuSurface = ((RenderTargetDX11)rt).GetCPUReadBuffer().QueryInterface<DXGI.Surface>())
            {
                SharpDX.DataRectangle dataRect = _cpuSurface.Map(DXGI.MapFlags.Read, out SharpDX.DataStream stream);
                SharpDX.DataPointer dataPtr = new SharpDX.DataPointer(dataRect.DataPointer, rt.Height * dataRect.Pitch);
                D2D1.BitmapProperties properties = new D2D1.BitmapProperties(new SharpDX.Direct2D1.PixelFormat(SharpDX.DXGI.Format.R8G8B8A8_UNorm, D2D1.AlphaMode.Premultiplied));

                using (D2D1.Bitmap bitmap = new D2D1.Bitmap(Curent, new SharpDX.Size2(rt.Width, rt.Height), dataPtr, dataRect.Pitch, properties))
                {
                    Curent.DrawBitmap(bitmap, new RawRectangleF(rect.Left, rect.Top, rect.Right, rect.Bottom), 1, D2D1.BitmapInterpolationMode.Linear);
                }

                stream.Dispose();
            }
        }

        public void DrawSprite(Sprite sprite, Rectangle rect)
        {
            if (!sprite.IsValid)
                return;
            D2D1.Bitmap bitmap;

            if (!BitmapCache.ContainsKey(sprite.Atlas.Bitmap))
            {
                bitmap = D2D1.Bitmap.FromWicBitmap(Curent, sprite.Atlas.Bitmap);
                BitmapCache.Add(sprite.Atlas.Bitmap, bitmap);
            }
            else
            {
                bitmap = BitmapCache[sprite.Atlas.Bitmap];
            }
            Curent.DrawBitmap(bitmap, new RawRectangleF(rect.Left, rect.Top, rect.Right, rect.Bottom), 1, D2D1.BitmapInterpolationMode.Linear, new RawRectangleF(sprite.BoundingBox.Left, sprite.BoundingBox.Top, sprite.BoundingBox.Right, sprite.BoundingBox.Bottom));
        }

        public void DrawSprite(Sprite sprite, Rectangle rect, Color color, bool withAlpha = true)
        {

            if (!sprite.IsValid)
                return;

            D2D1.Bitmap bitmap;

            if (!BitmapCache.ContainsKey(sprite.Atlas.Bitmap))
            {
                bitmap = D2D1.Bitmap.FromWicBitmap(Curent, sprite.Atlas.Bitmap);
                BitmapCache.Add(sprite.Atlas.Bitmap, bitmap);
            }
            else
            {
                bitmap = BitmapCache[sprite.Atlas.Bitmap];
            }

            //TODO: Optimize this to one custom effect

            //Get sprite from atlas

            D2DE.Atlas atlas = new D2DE.Atlas(Curent)
            {
                InputCount = 1,
                InputRectangle = new RawVector4(sprite.BoundingBox.Left, sprite.BoundingBox.Top, sprite.BoundingBox.Right, sprite.BoundingBox.Bottom)
            };
            atlas.SetInput(0, bitmap, false);

            //Scale it to fit the target rectangle

            D2DE.Scale scale = new D2DE.Scale(Curent)
            {
                InputCount = 1,
                ScaleAmount = new SharpDX.Vector2(rect.Width / (float)sprite.BoundingBox.Width, rect.Height / (float)sprite.BoundingBox.Height)
            };
            scale.SetInputEffect(0, atlas, false);

            //Tint it with required color

            Tint tint = new Tint(Curent)
            {
                InputCount = 1,
                Color = new RawVector4(color.R, color.G, color.B, withAlpha ? color.A : 1)
            };
            tint.SetInputEffect(0, scale, false);

            //Draw it
            Curent.DrawImage(tint, new SharpDX.Vector2(rect.Left, rect.Top));
            tint.Dispose();
            atlas.Dispose();
            scale.Dispose();
        }

        public void DrawText(Font font, string text, Rectangle layoutRect, Color color, TextAlignment horizontalAlignment = TextAlignment.Near, TextAlignment verticalAlignment = TextAlignment.Near, TextWrapping textWrapping = TextWrapping.NoWrap, float baseLinesGapScale = 1, float scale = 1)
        {
            D2D1.Brush brush;

            if (!BrushCache.ContainsKey(color))
            {
                brush = new D2D1.SolidColorBrush(Curent, new RawColor4(color.R, color.G, color.B, color.A));
                BrushCache.Add(color, brush);
            }
            else
            {
                brush = BrushCache[color];
            }

            DW.TextFormat tf = font;
            Rendering.Font.SetupTextFormat(ref tf, horizontalAlignment, verticalAlignment, textWrapping);
            Curent.DrawText(text ?? "", tf, new RawRectangleF(layoutRect.Left, layoutRect.Top, layoutRect.Right, layoutRect.Bottom), brush, D2D1.DrawTextOptions.Clip);
        }

        public void DrawTexture(WIC.BitmapSource t, Rectangle rect, Color color, bool withAlpha = false)
        {
            D2D1.Bitmap bitmap;

            if (!BitmapCache.ContainsKey(t))
            {
                bitmap = D2D1.Bitmap.FromWicBitmap(Curent, t);
                BitmapCache.Add(t, bitmap);
            }
            else
            {
                bitmap = BitmapCache[t];
            }

            Curent.DrawBitmap(bitmap, new RawRectangleF(rect.Left, rect.Top, rect.Right, rect.Bottom), 1, D2D1.BitmapInterpolationMode.Linear);
        }

        public void FillRectangle(Rectangle rect, Color color, bool withAlpha = false)
        {
            D2D1.Brush brush;

            if (!BrushCache.ContainsKey(color))
            {
                brush = new D2D1.SolidColorBrush(Curent, new RawColor4(color.R, color.G, color.B, color.A));
                BrushCache.Add(color, brush);
            }
            else
            {
                brush = BrushCache[color];
            }
            Curent.FillRectangle(new RawRectangleF(rect.Left, rect.Top, rect.Right, rect.Bottom), brush);
        }

        public void PopClip()
        {
            Curent.PopAxisAlignedClip();
        }

        public void PushClip(Rectangle clipRect)
        {
            ClipMask = clipRect;
            Curent.PushAxisAlignedClip(new RawRectangleF(clipRect.Left, clipRect.Top, clipRect.Right, clipRect.Bottom), D2D1.AntialiasMode.Aliased);
        }
    }
}
