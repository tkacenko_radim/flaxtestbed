﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using D2D1 = SharpDX.Direct2D1;

namespace FlaxEngine.Rendering.DirectX11
{
    internal sealed class WindowRenderTargetDX11 : WindowRenderTarget
    {
        private const int RT_FPS = 60;

        private GPUContextDX11 _context;
        private bool drawing;

        private DXGI.SwapChain _swapChain;
        private D3D11.RenderTargetView _backBuffer;
        internal D2D1.DeviceContext _d2dContext;

        public WindowRenderTargetDX11(Window target) : base(target)
        {
            _context = (GPUContextDX11)RenderingService.Context;

            CreateSwapChain();
            CreateBackBuffer();
            CreateD2D1();
        }

        private void CreateD2D1()
        {
            D2D1.RenderTargetProperties rtProperties = new D2D1.RenderTargetProperties(new D2D1.PixelFormat(DXGI.Format.Unknown, D2D1.AlphaMode.Premultiplied));
            using (DXGI.Surface bbSurface = DXGI.Surface.FromSwapChain(_swapChain, 0))
            {
                using (D2D1.RenderTarget rt = new D2D1.RenderTarget(GPUContext.FactoryDirect2D1, bbSurface, rtProperties))
                {
                    _d2dContext = rt.QueryInterface<D2D1.DeviceContext>();
                }
                _d2dContext.AntialiasMode = D2D1.AntialiasMode.PerPrimitive;
                _d2dContext.TextAntialiasMode = D2D1.TextAntialiasMode.Cleartype;
            }
        }

        private void CreateBackBuffer()
        {
            using (D3D11.Texture2D backBuffer = _swapChain.GetBackBuffer<D3D11.Texture2D>(0))
            {
                _backBuffer = new D3D11.RenderTargetView(_context.Device, backBuffer);
            }
        }

        private void CreateSwapChain()
        {
            DXGI.SwapChainDescription swapChainDesc = new DXGI.SwapChainDescription()
            {
                ModeDescription = new DXGI.ModeDescription((int)_target.ClientSize.X, (int)_target.ClientSize.Y, new DXGI.Rational(RT_FPS, 1), DXGI.Format.R8G8B8A8_UNorm),
                SampleDescription = new DXGI.SampleDescription(1, 0),
                Usage = DXGI.Usage.RenderTargetOutput | DXGI.Usage.BackBuffer,
                BufferCount = 1,
                OutputHandle = _target.Handle,
                IsWindowed = _target.IsWindowed,
                Flags = DXGI.SwapChainFlags.None,
                SwapEffect = DXGI.SwapEffect.Discard
            };
            _swapChain = new DXGI.SwapChain(GPUContext.FactoryDXGI, _context.Device, swapChainDesc);
        }

        internal override void BeginDraw2D()
        {
            if (drawing) return;

            _context.DeviceContext.OutputMerger.SetRenderTargets(_backBuffer);
            _context.DeviceContext.ClearRenderTargetView(_backBuffer, new SharpDX.Mathematics.Interop.RawColor4(0, 0, 0, 1));

            Render2DDX11.RenderTarget = this;

            _d2dContext.BeginDraw();
            drawing = true;
        }

        internal override void EndDraw2D()
        {
            if (!drawing) return;
            try
            {
                _d2dContext.EndDraw();
            }catch(Exception ex)
            {
                Debug.LogException(ex);
            }
            drawing = false;
        }

        internal override void OnResize(int width, int height)
        {
            if (drawing) EndDraw2D();

            Disposer.Dispose(ref _d2dContext);
            Disposer.Dispose(ref _backBuffer);

            _context.DeviceContext.ClearState();

            _swapChain.ResizeBuffers(1, width, height, DXGI.Format.Unknown, DXGI.SwapChainFlags.AllowModeSwitch);

            CreateBackBuffer();
            CreateD2D1();
        }

        internal override void Present()
        {
            if (drawing) EndDraw2D();
            _swapChain.Present(1, DXGI.PresentFlags.None);
        }

        public override void Dispose()
        {
            Disposer.Dispose(ref _swapChain);
            Disposer.Dispose(ref _backBuffer);
            Disposer.Dispose(ref _d2dContext);
        }
    }
}
