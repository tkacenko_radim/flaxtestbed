﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    public class CustomRenderTask : RenderTask
    {
        public Action<GPUContext> OnRender;

        internal CustomRenderTask()
        {

        }

        internal override bool Internal_Begin(out IntPtr outputPtr)
        {
            base.Internal_Begin(out outputPtr);

            return OnRender != null;
        }

        internal override void Internal_Render(GPUContext context)
        {
            OnRender.Invoke(context);
        }
    }
}
