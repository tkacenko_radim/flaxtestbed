﻿using System;

namespace FlaxEngine.Rendering
{
    public struct RenderTargetHandle
    {
        public object rt;
        public object sr;

        internal RenderTargetHandle(object renderView, object shaderView)
        {
            rt = renderView;
            sr = shaderView;
        }
    }

    public abstract class RenderTarget : Object, IDisposable
    {
        public abstract RenderTargetHandle Handle { get; }
        public abstract PixelFormat Format { get; }
        public abstract bool IsAllocated { get; }
        public abstract int Width { get; }
        public abstract int Height { get; }
        public abstract Vector2 Size { get; }
        public abstract bool IsPowerOfTwo { get; }

        public static RenderTarget Create()
        {
            return RenderingService.Context.CreateRenderTarget();
        }

        protected RenderTarget() : base()
        {

        }

        public abstract void Init(PixelFormat format, int width, int height, TextureFlags flags = TextureFlags.ShaderResource | TextureFlags.RenderTarget);
        public abstract void Dispose();
    }
}
