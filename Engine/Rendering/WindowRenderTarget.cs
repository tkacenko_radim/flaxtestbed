﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    internal abstract class WindowRenderTarget : Object, IDisposable
    {
        protected Window _target;

        internal WindowRenderTarget(Window target)
        {
            _target = target;
        }

        internal abstract void OnResize(int width, int height);
        internal abstract void BeginDraw2D();
        internal abstract void EndDraw2D();
        internal abstract void Present();

        public abstract void Dispose();
    }
}
