﻿using FlaxEngine.Rendering.DirectX11;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    public class RenderingService
    {
        public static List<RenderTask> Tasks = new List<RenderTask> { };

        internal static GPUContext Context;


        public static void Initialize()
        {
            //Modify this
            Context = new GPUContextDX11();
        }

        public static void ExecuteRenderTasks()
        {
            foreach (RenderTask task in Tasks)
            {
                if(task.Internal_Begin(out IntPtr outpupt))
                {
                    task.Internal_Render(Context);

                }
            }
        }
    }
}
