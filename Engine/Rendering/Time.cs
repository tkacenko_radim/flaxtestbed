﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    /// <summary>
    /// The interface to get time information from Flax.
    /// </summary>
    public static partial class Time
    {
        /// <summary>
        /// Gets time in seconds it took to complete the last frame, <see cref="TimeScale"/> dependent
        /// </summary>
        public static float DeltaTime
        {
            get; private set;
        }

        /// <summary>
        /// Gets time in seconds it took to complete the last frame in ticks, <see cref="TimeScale"/> dependent
        /// </summary>
        public static long DeltaTimeTicks
        {
            get; private set;
        }

        /// <summary>
        /// Gets real time in seconds since the game started
        /// </summary>
        public static float RealtimeSinceStartup
        {
            get; private set;
        }

        /// <summary>
        /// Gets real time in seconds since the game started in ticks
        /// </summary>
        public static long RealtimeSinceStartupTicks
        {
            get; private set;
        }

        /// <summary>
        /// Gets time at the beginning of this frame. This is the time in seconds since the start of the game. <see cref="TimeScale"/> dependent
        /// </summary>
        public static float TotalTimeSinceStartup
        {
            get; private set;
        }

        /// <summary>
        /// Gets time at the beginning of this frame  in ticks. This is the time in seconds since the start of the game. <see cref="TimeScale"/> dependent
        /// </summary>
        public static long TotalTimeSinceStartupTicks
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets scale at which the time is passing. This can be used for slow motion effects.
        /// </summary>
        public static float TimeScale
        {
            get; set;
        }

        /// <summary>
        /// The time this frame has started (Read Only). This is the time in seconds since the last level has been loaded.
        /// </summary>
        public static float TimeSinceLevelLoad
        {
            get; private set;
        }

        /// <summary>
        /// The time this frame has started in ticks. This is the time in seconds since the last level has been loaded.
        /// </summary>
        public static long TimeSinceLevelLoadTicks
        {
            get; private set;
        }

        /// <summary>
        /// Gets timeScale-independent time in seconds it took to complete the last frame.
        /// </summary>
        public static float UnscaledDeltaTime
        {
            get; private set;
        }

        /// <summary>
        /// Gets timeScale-independent time in seconds it took to complete the last frame in ticks.
        /// </summary>
        public static long UnscaledDeltaTimeTicks
        {
            get; private set;
        }

        /// <summary>
        /// Gets timeScale-independant time at the beginning of this frame. This is the time in seconds since the start of the game.
        /// </summary>
        public static float UnscaledTime
        {
            get; private set;
        }

        /// <summary>
        /// Gets timeScale-independant time at the beginning of this frame in ticks. This is the time in seconds since the start of the game.
        /// </summary>
        public static long UnscaledTimeTicks
        {
            get; private set;
        }

        private static Stopwatch Timer = Stopwatch.StartNew();

        /// <summary>
        /// Used to update internal times and all public values
        /// </summary>

        public static void Update()
        {
            Timer.Stop();
            //Update unscaled deltas
            UnscaledDeltaTimeTicks = Timer.ElapsedTicks;
            UnscaledDeltaTime = Timer.ElapsedMilliseconds / 1000f;

            //Add deltas to unscaled times
            UnscaledTimeTicks += UnscaledDeltaTimeTicks;
            UnscaledTime += UnscaledDeltaTime;

            //Update Since level load and cause we don't have level loading yet we just use Unscaled time
            TimeSinceLevelLoad = UnscaledTime;
            TimeSinceLevelLoadTicks = UnscaledTimeTicks;

            //Update TimeScale dependent times
            DeltaTime = UnscaledDeltaTime * TimeScale;
            DeltaTimeTicks = UnscaledDeltaTimeTicks * (long)TimeScale;

            //Update RealTime and cause i'm lazy we just use Unscaled time
            //TODO: Capture time on first startup and than just update this to difference Current - startup
            RealtimeSinceStartup = UnscaledTime;
            RealtimeSinceStartupTicks = UnscaledTimeTicks;

            //Update TimeScale dependent times
            TotalTimeSinceStartup = RealtimeSinceStartup * TimeScale;
            TotalTimeSinceStartupTicks = RealtimeSinceStartupTicks * (long)TimeScale;

            Timer.Restart();
        }

    }
}