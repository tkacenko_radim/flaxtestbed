﻿using FlaxEngine.Rendering.ShaderConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    public enum BasicShaderMode
    {
        Basic,
        VertexColor
    }

    public sealed class BasicShader : IDisposable
    {
        private FlaxEngine.Shader _shader;
        private ShaderConstantVector _shader_Col;
        private ShaderConstantMatrix _shader_WVP;
        public BasicShaderMode Mode = BasicShaderMode.Basic;
        public Matrix World = Matrix.Identity;
        public Color DiffuseColor = Color.White;
        public float Alpha = 1;

        public BasicShader()
        {
            _shader = Content.LoadInternal<FlaxEngine.Shader>("Engine/Shaders/BasicShader");
            if (!_shader)
            {
                Debug.LogException(new Exception("Failed to load BasicShader"));
                return;
            }

            _shader.Effect.CreateInputLayout(0, InputLayoutType.Position);
            _shader.Effect.CreateInputLayout(1, InputLayoutType.PositionColor);
            _shader_Col = _shader.Effect.GetVectorConstant("Col");
            _shader_WVP = _shader.Effect.GetMatrixConstant("WVP");
        }

        ~BasicShader()
        {
            Dispose();
        }

        public void Apply(Matrix view, Matrix projection)
        {
            Vector4 color = new Vector4(DiffuseColor.ToVector3(), Alpha);
            _shader_Col.Set(ref color);
            _shader_WVP.Set(World * view * projection);
            _shader.Effect.Apply((int)Mode);
        }

        public void Apply(RenderView view)
        {
            Vector4 color = new Vector4(DiffuseColor.ToVector3(), Alpha);
            _shader_Col.Set(ref color);
            _shader_WVP.Set(World * view.View * view.Projection);
            _shader.Effect.Apply((int)Mode);
        }

        public void Dispose()
        {
            if (_shader == null)
                return;
            _shader.RefCount--;
            _shader = null;
        }
    }
}
