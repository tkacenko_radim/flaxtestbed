﻿using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using D2D1 = SharpDX.Direct2D1;
using DW = SharpDX.DirectWrite;
using System;
using System.Threading;

namespace FlaxEngine.Rendering
{
    /// <summary>
	/// Allows to perform custom graphics commands using GPU device and rendering pipeline.
	/// </summary>
    public abstract class GPUContext : Object
    {

        internal static DXGI.Factory1 FactoryDXGI;
        internal static D2D1.Factory FactoryDirect2D1;
        internal static DW.Factory FactoryDirectWrite;

        public abstract Adapter Adapter { get; }

        private object _deviceLocker;

        protected GPUContext() : base()
        {
            _deviceLocker = new object();
        }

        /// <summary>
        /// Draws scene.
        /// </summary>
        /// <param name="task">Calling render task.</param>
        /// <param name="output">Output texture.</param>
        /// <param name="buffers">Frame rendering buffers.</param>
        /// <param name="view">Rendering view description structure.</param>
        /// <param name="flags">Custom view flags collection.</param>
        /// <param name="mode">Custom view mode option.</param>
        /// <param name="customActors">Custom set of actors to render.</param>
        public abstract void DrawScene(RenderTask task, RenderTarget output, RenderBuffers buffers, RenderView view, ViewFlags flags, ViewMode mode, Actor[] customActors);


        /// <summary>
        /// Clears texture surface with a color.
        /// </summary>
        /// <param name="rt">Target surface.</param>
        /// <param name="color">Clear color.</param>
        public abstract void Clear(RenderTarget rt, Color color);

        /// <summary>
		/// Clears depth buffer.
		/// </summary>
		/// <param name="depthBuffer">Depth buffer to clear.</param>
		/// <param name="depthValue">Clear depth value.</param>
        public abstract void ClearDepth(RenderTarget depthBuffer, float depthValue = 1.0f);

        /// <summary>
        /// Creates new render target
        /// </summary>
        internal abstract RenderTarget CreateRenderTarget();

        /// <summary>
        /// Creates new mesh
        /// </summary>
        internal abstract Mesh CreateMesh(Model model, int lodIndex, int meshIndex);

        /// <summary>
        /// Creates new window render target
        /// </summary>
        /// <param name="window">Window to draw on</param>
        /// <returns>Winder render target</returns>
        internal abstract WindowRenderTarget CreateWindowRenderTarget(Window window);

        internal abstract Shader CreateShader(string path);

        internal bool IsLocked()
        {
            if (Monitor.TryEnter(_deviceLocker, 0))
            {
                Monitor.Exit(_deviceLocker);
                return false;
            }
            return true;
        }

        internal void AddLocker()
        {
            Monitor.Enter(_deviceLocker);
        }

        internal bool AddLocker(int timeout)
        {
            return Monitor.TryEnter(_deviceLocker, timeout);
        }

        internal void RemoveLocker()
        {
            Monitor.Exit(_deviceLocker);
        }
    }
}
