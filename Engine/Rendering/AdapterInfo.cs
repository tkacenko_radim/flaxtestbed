﻿namespace FlaxEngine.Rendering
{
    /// <summary>
    /// Describes an adapter (or video card).
    /// </summary>
    /// <remarks>
    /// Corresponds to DXGI_ADAPTER_DESC  (https://msdn.microsoft.com/cs-cz/library/windows/desktop/bb173058.aspx)
    /// </remarks>
    public struct AdapterInfo
    {
        /// <summary>
        /// A string that contains the adapter description.
        /// </summary>
        public string Description;

        /// <summary>
        /// The PCI ID of the hardware vendor.
        /// </summary>
        public int VendorId;

        /// <summary>
        /// The PCI ID of the hardware device. 
        /// </summary>
        public int DeviceId;

        /// <summary>
        /// The PCI ID of the sub system.
        /// </summary>
        public int SubsystemId;

        /// <summary>
        /// The PCI ID of the revision number of the adapter.
        /// </summary>
        public int Revision;

        /// <summary>
        /// Number of bytes of dedicated memory that are not shared with the CPU.
        /// </summary>
        public long DedicatedVideoMemory;

        /// <summary>
        /// The number of bytes of dedicated system memory that are not shared with the CPU. This memory is allocated from available system memory at boot time.
        /// </summary>
        public long DedicatedSystemMemory;

        /// <summary>
        /// The number of bytes of shared system memory. This is the maximum value of system memory that may be consumed by the adapter during operation. Any incidental memory consumed by the driver as it manages and uses video memory is additional.
        /// </summary>
        public long SharedSystemMemory;

        /// <summary>
        /// A unique value that identifies the adapter.
        /// </summary>
        public long Luid;

        /// <summary>
        /// <c>true</c> if adapter's vendor is AMD/ATI; otherwise, <c>false</c>.
        /// </summary>
        public bool IsAMD
        {
            get
            {
                return VendorId == 4098;
            }
        }

        /// <summary>
        /// <c>true</c> if adapter's vendor is Intel; otherwise, <c>false</c>.
        /// </summary>
        public bool IsIntel
        {
            get
            {
                return VendorId == 32902;
            }
        }

        /// <summary>
        /// <c>true</c> if adapter's vendor is nVidia; otherwise, <c>false</c>.
        /// </summary>
        public bool IsNVidia
        {
            get
            {
                return VendorId == 4318;
            }
        }
    }
}
