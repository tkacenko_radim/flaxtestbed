﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    interface IRender2D : IDisposable
    {
        Vector2 Transform { get; set; }

        void PushClip(Rectangle clipRect);
        void PopClip();

        Rectangle ClipMask { get; }

        void DrawText(Rendering.Font font, string text, Rectangle layoutRect, Color color, TextAlignment horizontalAlignment = TextAlignment.Near, TextAlignment verticalAlignment = TextAlignment.Near, TextWrapping textWrapping = TextWrapping.NoWrap, float baseLinesGapScale = 1.0f, float scale = 1.0f);
        void FillRectangle(Rectangle rect, Color color, bool withAlpha = false);
        void DrawRectangle(Rectangle rect, Color color, bool withAlpha = false, float thickness = 1.0f);
        void DrawTexture(SharpDX.WIC.BitmapSource t, Rectangle rect, Color color, bool withAlpha = false);
        void DrawLine(Vector2 p1, Vector2 p2, Color color, float thickness = 1.0f, bool withAlpha = false);
        void DrawSprite(Sprite sprite, Rectangle rect);
        void DrawSprite(Sprite sprite, Rectangle rect, Color color, bool withAlpha = true);
        void DrawRenderTarget(Rendering.RenderTarget rt, Rectangle rect, Color color, bool withAlpha = false);
    }
}
