﻿using System;
using System.Collections.Generic;

using DXGI = SharpDX.DXGI;
using D3D = SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

namespace FlaxEngine.Rendering
{
    /// <summary>
    /// Manages DXGI adapters
    /// </summary>
    public sealed class Adapter : IDisposable
    {
        private static Adapter _bestAdapter;
        private static DXGI.Adapter[] _adapters;
        private readonly AdapterInfo _info;
        private readonly int _index;
        private readonly bool _isPrimary;

        /// <summary>
        /// DXGI adapter
        /// </summary>
        internal DXGI.Adapter AdapterDXGI;

        /// <summary>
        /// Primary output of this adapter
        /// </summary>
        internal DXGI.Output PrimaryOutput;

        /// <summary>
        /// <c>true</c> if is primary adapter; otherwise, <c>false</c>.
        /// </summary>
        public bool IsPrimary => _isPrimary;
        /// <summary>
        /// Index of adapter
        /// </summary>
        public int Index => _index;

        /// <summary>
        /// Info about this adapter
        /// </summary>
        public AdapterInfo Info => _info;

        /// <summary>
        /// <c>true</c> if is default adapter; otherwise, <c>false</c>.
        /// </summary>
        public bool IsDefaultAdapter => _index == 0;
        public D3D.FeatureLevel FeatureLevel => D3D11.Device.GetSupportedFeatureLevel(AdapterDXGI);

        /// <summary>
        /// Init
        /// </summary>
        /// <param name="adapter">DXGI adapter</param>
        /// <param name="isPriamry">Is primary adapter?</param>
        /// <param name="index">Index of adapter</param>
        private Adapter(DXGI.Adapter adapter, bool isPriamry, int index)
        {
            if(index < 0 || adapter == null)
            {
                throw new ArgumentException();
            }

            AdapterDXGI = adapter;
            _index = index;
            _isPrimary = isPriamry;
            if(AdapterDXGI.GetOutputCount() > 0)
            {
                PrimaryOutput = AdapterDXGI.GetOutput(0);
            }
            DXGI.AdapterDescription description = AdapterDXGI.Description;

            _info = new AdapterInfo
            {
                Description = description.Description.Trim(),
                DeviceId = description.DeviceId,
                Luid = description.Luid,
                Revision = description.Revision,
                SubsystemId = description.SubsystemId,
                VendorId = description.VendorId,
                SharedSystemMemory = description.SharedSystemMemory,
                DedicatedSystemMemory = description.DedicatedSystemMemory,
                DedicatedVideoMemory = description.DedicatedVideoMemory
            };
        }

        public override string ToString() => _info.Description;

        public void Dispose()
        {
            Disposer.Dispose(ref AdapterDXGI);
            PrimaryOutput = null;
        }

        internal static void DisposeAll()
        {
            _adapters = null;
            Disposer.Dispose(ref _bestAdapter);
        }

        /// <summary>
        /// Finds best availible graphics adapter
        /// </summary>
        /// <returns>Te best adapter</returns>
        internal static Adapter FindBest()
        {
            if(GPUContext.FactoryDXGI == null)
            {
                GPUContext.FactoryDXGI = new SharpDX.DXGI.Factory1();
            }

            if(_bestAdapter == null)
            {
                _adapters = GPUContext.FactoryDXGI.Adapters;

                //TODO: Probably remove Software devices, shouldn't be needed though

                if(_adapters == null || _adapters.Length == 0)
                {
                    throw new NotSupportedException("No DXGI adapters found!");
                }else if(_adapters.Length == 1)
                {
                    _bestAdapter = new Adapter(_adapters[0], true, 0);
                }
                else
                {
                    List<DXGI.Adapter> sorted = new List<DXGI.Adapter>(_adapters);
                    sorted.Sort(sortAdapters);
                    if(sorted.Count == 0)
                    {
                        throw new NotSupportedException("No DXGI adapter after ranking!");
                    }
                    _bestAdapter = new Adapter(sorted[0], sorted[0] == _adapters[0], getAdapterIndex(sorted[0]));
                }
            }
            return _bestAdapter;
        }

        private static int sortAdapters(DXGI.Adapter left, DXGI.Adapter right)
        {
            //-1 = left is better
            //1 = right is better

            DXGI.AdapterDescription desc_left = left.Description;
            DXGI.AdapterDescription desc_right = right.Description;
            D3D.FeatureLevel sfl_left = D3D11.Device.GetSupportedFeatureLevel(left);
            D3D.FeatureLevel sfl_right = D3D11.Device.GetSupportedFeatureLevel(right);

            if(sfl_left != sfl_right)
            {
                if(sfl_left >= sfl_right)
                {
                    return -1;
                }
                return 1;
            }
            else
            {
                long ssml = desc_left.SharedSystemMemory;
                long ssmr = desc_right.SharedSystemMemory;

                if(ssml == ssmr)
                {
                    return getAdapterIndex(left).CompareTo(getAdapterIndex(right));
                }
                if(ssml >= ssmr)
                {
                    return -1;
                }
                return 1;
            }
        }

        private static int getAdapterIndex(DXGI.Adapter adapter)
        {
            for (int i = 0; i < _adapters.Length; i++)
            {
                if(_adapters[i] == adapter)
                {
                    return i;
                }
            }
            throw new Exception("Data leak");
        }
    }
}
