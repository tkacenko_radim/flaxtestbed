﻿namespace FlaxEngine.Rendering.ShaderConstants
{
    public abstract class ShaderConstantMatrix : ShaderConstant
    {
        protected ShaderConstantMatrix(GPUContext context) : base(context)
        {
        }

        public abstract void Set(Matrix value);

        public abstract void Set(ref Matrix value);

        public abstract void Set(Matrix[] value);
    }
}
