﻿using System;

namespace FlaxEngine.Rendering.ShaderConstants
{
    public abstract class ShaderConstant : IDisposable
    {
        protected GPUContext Context;

        protected ShaderConstant(GPUContext context)
        {
            Context = context;
        }

        ~ShaderConstant()
        {
            Dispose();
        }

        public abstract void Dispose();
    }
}
