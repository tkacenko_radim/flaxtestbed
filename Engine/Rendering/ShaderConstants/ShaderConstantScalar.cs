﻿namespace FlaxEngine.Rendering.ShaderConstants
{
    public abstract class ShaderConstantScalar : ShaderConstant
    {
        protected ShaderConstantScalar(GPUContext context) : base(context)
        {
        }

        public abstract void Set(bool value);
        public abstract void Set(int value);
        public abstract void Set(float value);
        public abstract void Set(float[] value);
    }
}
