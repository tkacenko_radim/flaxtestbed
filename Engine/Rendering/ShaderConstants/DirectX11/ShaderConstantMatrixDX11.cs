﻿using FlaxEngine.Rendering.ShaderConstants;
using SharpDX.Direct3D11;

namespace FlaxEngine.Rendering.DirectX11.ShaderConstants
{
    internal sealed class ShaderConstantMatrixDX11 : ShaderConstantMatrix
    {
        private EffectMatrixVariable _handle;

        internal ShaderConstantMatrixDX11(ShaderDX11 parent, EffectMatrixVariable handle) : base(parent.Context)
        {
            _handle = handle;
        }

        public override void Set(Matrix value)
        {
            _handle.SetMatrix(ref value);
        }

        public override void Set(ref Matrix value)
        {
            _handle.SetMatrix(ref value);
        }

        public override void Set(Matrix[] value)
        {
            _handle.SetMatrix(value);
        }

        public override void Dispose()
        {
            if (_handle != null)
            {
                _handle.Dispose();
                _handle = null;
            }
        }
    }
}
