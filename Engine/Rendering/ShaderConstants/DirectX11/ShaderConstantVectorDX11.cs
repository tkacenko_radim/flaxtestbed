﻿using FlaxEngine.Rendering.ShaderConstants;
using SharpDX.Direct3D11;

namespace FlaxEngine.Rendering.DirectX11.ShaderConstants
{
    internal sealed class ShaderConstantVectorDX11 : ShaderConstantVector
    {
        private EffectVectorVariable _handle;

        internal ShaderConstantVectorDX11(ShaderDX11 parent, EffectVectorVariable handle) : base(parent.Context)
        {
            _handle = handle;
        }

        public override void Set(Vector2 value)
        {
            _handle.Set(value);
        }

        public override void Set(Vector3 value)
        {
            _handle.Set(value);
        }

        public override void Set(Vector4 value)
        {
            _handle.Set(value);
        }

        public override void Set(ref Vector2 value)
        {
            _handle.Set(value);
        }

        public override void Set(ref Vector3 value)
        {
            _handle.Set(value);
        }

        public override void Set(ref Vector4 value)
        {
            _handle.Set(value);
        }

        public override void Set(Vector4[] value)
        {
            _handle.Set(value);
        }

        public override void Dispose()
        {
            if (_handle != null)
            {
                _handle.Dispose();
                _handle = null;
            }
        }
    }
}