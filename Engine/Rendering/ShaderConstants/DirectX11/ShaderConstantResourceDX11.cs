﻿using FlaxEngine.Rendering.ShaderConstants;
using SharpDX.Direct3D11;

namespace FlaxEngine.Rendering.DirectX11.ShaderConstants
{
    internal sealed class ShaderConstantResourceDX11 : ShaderConstantResource
    {
        private EffectShaderResourceVariable _handle;

        private ShaderResourceView _value;

        internal ShaderConstantResourceDX11(ShaderDX11 parent, EffectShaderResourceVariable handle) : base(parent.Context)
        {
            _handle = handle;
            _value = null;
        }

        
        public override void Dispose()
        {
            if (_handle != null)
            {
                _handle.Dispose();
                _handle = null;
            }
            _value = null;
        }
    }
}
