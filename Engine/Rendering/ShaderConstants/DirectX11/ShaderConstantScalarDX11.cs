﻿using FlaxEngine.Rendering.ShaderConstants;
using SharpDX.Direct3D11;

namespace FlaxEngine.Rendering.DirectX11.ShaderConstants
{
    internal sealed class ShaderConstantScalarDX11 : ShaderConstantScalar
    {
        private EffectScalarVariable _handle;

        internal ShaderConstantScalarDX11(ShaderDX11 parent, EffectScalarVariable handle) : base(parent.Context)
        {
            _handle = handle;
        }

        public override void Set(bool value)
        {
            _handle.Set(value);
        }

        public override void Set(int value)
        {
            _handle.Set(value);
        }

        public override void Set(float value)
        {
            _handle.Set(value);
        }

        public override void Set(float[] value)
        {
            _handle.Set(value);
        }

        public override void Dispose()
        {
            if (_handle != null)
            {
                _handle.Dispose();
                _handle = null;
            }
        }
    }
}
