﻿namespace FlaxEngine.Rendering.ShaderConstants
{
    public abstract class ShaderConstantVector : ShaderConstant
    {
        protected ShaderConstantVector(GPUContext context) : base(context)
        {
        }

        public abstract void Set(Vector2 value);
        public abstract void Set(Vector3 value);
        public abstract void Set(Vector4 value);
        public abstract void Set(ref Vector2 value);
        public abstract void Set(ref Vector3 value);
        public abstract void Set(ref Vector4 value);
        public abstract void Set(Vector4[] value);
    }
}
