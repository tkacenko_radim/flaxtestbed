﻿using FlaxEngine.Rendering.ShaderConstants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    public enum InputLayoutType
    {
        Position,
        PositionColor,
        PositionTexcoord,
        PositionColorTexcoord,
        PositionNormal,
        ModelInput
    }

    public abstract class Shader : Object
    {
        protected Dictionary<string, ShaderConstant> _constants = new Dictionary<string, ShaderConstant> { };

        public bool IsLoaded = false;

        protected Shader() : base()
        {

        }

        public Shader(GPUContext context)
        {

        }

        public abstract void Load(string path);

        public abstract void CreateInputLayout(int index, InputLayoutType type);
        public abstract void Apply(int techniqueIndex);
        public abstract void Optimize();

        public abstract ShaderConstantScalar GetScalarConstant(string name);
        public abstract ShaderConstantVector GetVectorConstant(string name);
        public abstract ShaderConstantMatrix GetMatrixConstant(string name);
        public abstract ShaderConstantResource GetResourceConstant(string name);
    }
}
