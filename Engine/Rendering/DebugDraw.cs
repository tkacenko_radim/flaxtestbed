﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public static class DebugDraw
    {
        public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.0f, bool depthTest = true)
        {

        }

        public static void DrawBox(BoundingBox box, Color color, float duration = 0.0f, bool depthTest = true)
        {

        }

        public static void DrawFrustum(BoundingFrustum frustum, Color color, float duration = 0.0f, bool depthTest = true)
        {

        }

        public static void DrawBox(OrientedBoundingBox box, Color color, float duration = 0.0f, bool depthTest = true)
        {

        }

        public static void DrawSphere(Vector3 position, float radius, Color color, float duration = 0.0f, bool depthTest = true)
        {

        }

        public static void DrawCircle(Vector3 position, Vector3 normal, float radius, Color color, float duration = 0.0f, bool depthTest = true)
        {

        }

        public static void DrawTube(Vector3 position, Quaternion orientation, float radius, float length, Color color, float duration = 0.0f, bool depthTest = true)
        {

        }

        public static void DrawSphere(BoundingSphere sphere, Color color, float duration = 0.0f, bool depthTest = true)
        {
            DrawSphere(sphere.Center, sphere.Radius, color, duration, depthTest);
        }

        public static void Draw(FlaxEngine.Rendering.RenderTask task, Actor[] selectedActors)
        {

        }
    }
}
