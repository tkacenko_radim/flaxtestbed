﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    public class RenderTask : Object
    {
        public bool Enabled
        {
            get;set;
        }

        public int Order
        {
            get; set;
        }

        public static T Create<T>() where T : SceneRenderTask
        {
            SceneRenderTask task = new SceneRenderTask();
            RenderingService.Tasks.Add(task);
            return (T)task;
        }

        protected RenderTask() : base()
        {

        }

        /// <summary>
        /// Disposes render task data and child components (output and buffers).
        /// </summary>
        public virtual void Dispose()
        {
            Enabled = false;

            RenderingService.Tasks.Remove(this);
        }

        internal virtual bool Internal_Begin(out IntPtr outputPtr)
        {
            outputPtr = IntPtr.Zero;
            return true;
        }

        internal virtual void Internal_Render(GPUContext context)
        {
        }

        [StructLayout(LayoutKind.Sequential)]
        internal struct DrawCall
        {
            public StaticFlags Flags;
            public int LodIndex;
            public int MeshIndex;
            public int Padding;
            public Model AssetModel;
            public MaterialBase AssetMaterialBase;
            public Matrix World;
        }

        internal virtual DrawCall[] Internal_Draw()
        {
            return null;
        }

    }
}
