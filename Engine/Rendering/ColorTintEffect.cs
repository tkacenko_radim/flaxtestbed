﻿using SharpDX.Direct2D1;
using SharpDX.Mathematics.Interop;

namespace FlaxEngine.Rendering
{
    /// <summary>
    /// Built in Tint effect.
    /// </summary>
    public class Tint : Effect
    {
        /// <summary>
        /// Initializes a new instance of <see cref="Tint"/> effect.
        /// </summary>
        /// <param name="context"></param>
        public Tint(DeviceContext context) : base(context,Tint)
        {

        }

        /// <summary>
        /// Colors from the source image are multiplied by this value.
        /// </summary>
        public RawVector4 Color
        {
            get
            {
                return GetVector4Value((int)TintProp.Color);
            }
            set
            {
                SetValue((int)TintProp.Color, value);
            }
        }

        /// <summary>
        /// Whether or not to clamp the output values to the range [0, 1].
        /// </summary>
        public bool ClampOutput
        {
            get
            {
                return GetBoolValue((int)TintProp.ClampOutput);
            }
            set
            {
                SetValue((int)TintProp.ClampOutput, value);
            }
        }
    }
}
