﻿using FlaxEngine.Rendering;
using SharpDX.Mathematics.Interop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;
using D3D = SharpDX.Direct3D;
using D2D1 = SharpDX.Direct2D1;
using D2DE = SharpDX.Direct2D1.Effects;
using DW = SharpDX.DirectWrite;

namespace FlaxEngine
{
    public static class Render2D
    {
        internal static IRender2D Renderer;

        /// <summary>
        /// Gets or sets transform applied to all rendering operations (only offset).
        /// </summary>
        public static Vector2 Transform
        {
            get { return Renderer.Transform; }
            set { Renderer.Transform = value;}
        }

        /// <summary>
        /// Push clipping rectangle mask
        /// </summary>
        /// <param name="clipRect">Axis aligned clipping mask rectangle</param>
        public static void PushClip(Rectangle clipRect)
        {
            Renderer.PushClip(clipRect);
        }

        /// <summary>
        /// Pop clippng rectangle mask
        /// </summary>
        public static void PopClip()
        {
            Renderer.PopClip();
        }

        /// <summary>
        /// Gets current clipping mask (transformed).
        /// </summary>
        public static Rectangle ClipMask => Renderer.ClipMask;

        /// <summary>
        /// Draw text
        /// </summary>
        /// <param name="font">Font to use</param>
        /// <param name="text">Text to render</param>
        /// <param name="layoutRect">The size and position of the area in which the text is drawn</param>
        /// <param name="color">Text color</param>
        /// <param name="horizontalAlignment">Horizontal alignment of the text in a layout rectangle</param>
        /// <param name="verticalAlignment">Vetical alignment of the text in a layout rectangle</param>
        /// <param name="textWrapping">Describes how wrap text inside a layout rectangle</param>
        /// <param name="baseLinesGapScale">Scale for distance one baseline from another. Default is 1.</param>
        /// <param name="scale">Text drawing scale. Default is 1.</param>
        public static void DrawText(Rendering.Font font, string text, Rectangle layoutRect, Color color, TextAlignment horizontalAlignment = TextAlignment.Near, TextAlignment verticalAlignment = TextAlignment.Near, TextWrapping textWrapping = TextWrapping.NoWrap, float baseLinesGapScale = 1.0f, float scale = 1.0f)
        {
            Renderer.DrawText(font, text, layoutRect, color, horizontalAlignment, verticalAlignment, textWrapping, baseLinesGapScale, scale);
        }

        /// <summary>
        /// Fill rectangle area
        /// </summary>
        /// <param name="rect">Rectangle to fill</param>
        /// <param name="color">Color to use</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled</param>
        public static void FillRectangle(Rectangle rect, Color color, bool withAlpha = false)
        {
            Renderer.FillRectangle(rect, color, withAlpha);
        }

        /// <summary>
        /// Fill rectangle area
        /// </summary>
        /// <param name="rect">Rectangle to fill</param>
        /// <param name="color0">Color to use for upper left vertex</param>
        /// <param name="color1">Color to use for upper right vertex</param>
        /// <param name="color2">Color to use for bottom right vertex</param>
        /// <param name="color3">Color to use for bottom left vertex</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled</param>
       /* public static void FillRectangle(Rectangle rect, Color color0, Color color1, Color color2, Color color3, bool withAlpha = false)
        {
        }*/

        /// <summary>
        /// Draw rectangle borders
        /// </summary>
        /// <param name="rect">Rectangle to draw</param>
        /// <param name="color">Color to use</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled</param>
        /// <param name="thickness">Lines thickness (in pixels)</param>
        public static void DrawRectangle(Rectangle rect, Color color, bool withAlpha = false, float thickness = 1.0f)
        {
            Renderer.DrawRectangle(rect, color, withAlpha, thickness);
        }

        /// <summary>
        /// Draw texture
        /// </summary>
        /// <param name="t">Texture to draw</param>
        /// <param name="rect">Rectangle to draw</param>
        /// <param name="color">Color to use</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled.</param>
        public static void DrawTexture(SharpDX.WIC.BitmapSource t, Rectangle rect, Color color, bool withAlpha = false)
        {
            Renderer.DrawTexture(t, rect, color, withAlpha);
        }

        /// <summary>
        /// Draw line
        /// </summary>
        /// <param name="p1">Start point</param>
        /// <param name="p2">End point</param>
        /// <param name="color">Color to use</param>
        /// <param name="thickness">Lines thickness (in pixels)</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled.</param>
        public static void DrawLine(Vector2 p1, Vector2 p2, Color color, float thickness = 1.0f, bool withAlpha = false)
        {
            Renderer.DrawLine(p1, p2, color, thickness, withAlpha);
        }

        /// <summary>
        /// Draw Bezier curve
        /// </summary>
        /// <param name="p1">Start point</param>
        /// <param name="p2">First control point</param>
        /// <param name="p3">Second control point</param>
        /// <param name="p4">End point</param>
        /// <param name="color">Color to use</param>
        /// <param name="thickness">Lines thickness (in pixels)</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled.</param>
        /*public static void DrawBezier(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, Color color, float thickness = 1.0f, bool withAlpha = false)
        {
        }*/
        /// <summary>
        /// Draws sprite.
        /// </summary>
        /// <param name="sprite">Sprite to draw.</param>
        /// <param name="rect">Rectangle to draw.</param>
        public static void DrawSprite(Sprite sprite, Rectangle rect)
        {
            Renderer.DrawSprite(sprite, rect);
        }

        /// <summary>
        /// Draw texture
        /// </summary>
        /// <param name="rt">Render target to draw</param>
        /// <param name="rect">Rectangle to draw</param>
        /// <param name="color">Color to use</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled.</param>
        public static void DrawRenderTarget(Rendering.RenderTarget rt, Rectangle rect, Color color, bool withAlpha = false)
        {
            Renderer.DrawRenderTarget(rt, rect, color, withAlpha);
        }

        /// <summary>
        /// Draws sprite.
        /// </summary>
        /// <param name="sprite">Sprite to draw.</param>
        /// <param name="rect">Rectangle to draw.</param>
        /// <param name="color">Color to multiply all texture pixels.</param>
        /// <param name="withAlpha">True if use alpha blending, otherwise it will be disabled.</param>
        public static void DrawSprite(Sprite sprite, Rectangle rect, Color color, bool withAlpha = true)
        {
            Renderer.DrawSprite(sprite, rect, color, withAlpha);
        }

        public static void Dispose()
        {
            Renderer.Dispose();
        }
    }
}
