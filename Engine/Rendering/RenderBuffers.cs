﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Rendering
{
    public sealed class RenderBuffers : Object
    {
        public int Width
        {
            get;
        }

        public int Height
        {
            get;
        }

        public float AspectRatio => Width / (float)Height;
        public Vector2 Size { get => new Vector2(Width, Height); set {

            } }

        private RenderBuffers() : base()
        {

        }

        public static RenderBuffers Create()
        {
            return new RenderBuffers();
        }

        public void Init(int width, int height)
        {

        }

        public void Dispose()
        {

        }

    }
}
