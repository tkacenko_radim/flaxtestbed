﻿using FlaxEngine.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlaxEngine
{
    public class LButtonHitEventArgs : EventArgs
    {
        public WindowHitCodes HitCode = WindowHitCodes.NoWhere;
        public bool Handled = false;
    }

    public class HitTestEventArgs : EventArgs
    {
        public Vector2 MousePosition;
        public WindowHitCodes HitCode = WindowHitCodes.NoWhere;
        public bool Handled = false;
    }

    public class CustomForm : Form
    {
        public EventHandler<LButtonHitEventArgs> LButtonHit;
        public EventHandler<HitTestEventArgs> HitTest;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0xA1) //WM_NCLBUTTONDOWN
            {
                LButtonHitEventArgs ea = new LButtonHitEventArgs()
                {
                    HitCode = (WindowHitCodes)m.WParam
                };
                LButtonHit(this, ea);
                if (ea.Handled)
                {
                    m.Result = IntPtr.Zero;
                    return;
                }
            }
            base.WndProc(ref m);

            //Do this afer WndProc has complement because we want to check the value
            if(m.Msg == (int)Win32.WindowsMessages.WM_NCHITTEST)
            {
                if(m.Result == (IntPtr)WindowHitCodes.Client)
                {
                    HitTestEventArgs htea = new HitTestEventArgs()
                    {
                        MousePosition = new Vector2(Win32.GET_X_LPARAM(m.LParam), Win32.GET_Y_LPARAM(m.LParam)),
                        HitCode = WindowHitCodes.Client
                    };

                    HitTest(this, htea);

                    if (htea.Handled)
                    {
                        m.Result = (IntPtr)htea.HitCode;
                    }
                }
            }
        }
    }


    public partial class Window : IDisposable
    {
        private CustomForm _form;
        private CreateWindowSettings settings;
        private WindowRenderTarget RenderTarget;

        private bool[] KeyState = new bool[256];
        private bool[] MouseButtonState = new bool[17];

        public static List<Window> ActiveWindows = new List<Window> { };

        public void Internal_Process()
        {
            Internal_OnUpdate(Time.DeltaTime);
        }

        public void Internal_Paint()
        {
            if (!IsVisible || !RenderingEnabled)
                return;

            RenderTarget.BeginDraw2D();
            try
            {
                Internal_OnDraw();
            }
            finally
            {
                RenderTarget.EndDraw2D();
            }
            RenderTarget.Present();
            if (settings.ShowAfterFirstPaint)
            {
                Show();
                //Reuse the property, set it to false so we don't show window again
                settings.ShowAfterFirstPaint = false;
            }
        }

        private Window(CreateWindowSettings settings)
        {
            //Initialize and reset state arrays
            for (int i = 0; i < 256; i++)
            {
                KeyState[i] = false;
            }

            for (int i = 0; i < 17; i++)
            {
                MouseButtonState[i] = false;
            }

            this.settings = settings;

            //Create gui
            GUI = new GUI.Window(this);

            //TODO: Probably do something about this
            FormBorderStyle borderStyle = settings.IsRegularWindow ? settings.HasSizingFrame ? FormBorderStyle.Sizable : settings.HasBorder ? FormBorderStyle.FixedSingle : FormBorderStyle.None : settings.HasSizingFrame ? FormBorderStyle.SizableToolWindow : settings.HasBorder ? FormBorderStyle.FixedToolWindow : FormBorderStyle.None;

            //Parse parameters and create internal form from it
            _form = new CustomForm()
            {
                StartPosition = settings.StartPosition == WindowStartPosition.CenterParent ? FormStartPosition.CenterParent : settings.StartPosition == WindowStartPosition.CenterScreen ? FormStartPosition.CenterScreen : FormStartPosition.Manual,
                Text = settings.Title,
                FormBorderStyle = borderStyle,
                Location = new System.Drawing.Point((int)settings.Position.X, (int)settings.Position.Y),
                MaximumSize = new System.Drawing.Size((int)settings.MaximumSize.X, (int)settings.MaximumSize.Y),
                MinimumSize = new System.Drawing.Size((int)settings.MinimumSize.X, (int)settings.MinimumSize.Y),
                Size = new System.Drawing.Size((int)settings.Size.X, (int)settings.Size.Y),
                AllowTransparency = settings.SupportsTransparency,
                ShowInTaskbar = settings.ShowInTaskbar,
                MinimizeBox = settings.AllowMinimize,
                MaximizeBox = settings.AllowMaximize,
                AllowDrop = settings.AllowDragAndDrop,
                TopMost = settings.IsTopmost,
                ShowIcon = false
            };

            //Create RenderTarget (can't be done befor form, because it's Handle dependant)
            RenderTarget = RenderingService.Context.CreateWindowRenderTarget(this);

            //Register as active window
            ActiveWindows.Add(this);


            _form.FormClosed += _form_FormClosed;
            _form.FormClosing += _form_FormClosing;
            _form.GotFocus += _form_GotFocus;
            _form.KeyDown += _form_KeyDown;
            _form.KeyUp += _form_KeyUp;
            _form.LostFocus += _form_LostFocus;
            _form.MouseDoubleClick += _form_MouseDoubleClick;
            _form.MouseDown += _form_MouseDown;
            _form.MouseLeave += _form_MouseLeave;
            _form.MouseMove += _form_MouseMove;
            _form.MouseUp += _form_MouseUp;
            _form.MouseWheel += _form_MouseWheel;
            _form.Resize += _form_Resize;
            _form.LButtonHit += _form_LButtonHit;
            _form.HitTest += _form_HitTest;
            //Manual resize, because some resizing happens before we hooked Resize event
            Internal_OnResize(_form.ClientSize.Width, _form.ClientSize.Height);

            _form.Shown += _form_Shown;

            if (settings.ParentPtr == IntPtr.Zero)
            {
                _form.Show();
            }
            else
            {
                _form.Show(Control.FromHandle(settings.ParentPtr));
            }
        }

        private void _form_HitTest(object sender, HitTestEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Internal_OnHitTest(ref e.MousePosition, ref e.HitCode, ref e.Handled);
        }

        private void _form_LButtonHit(object sender, LButtonHitEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Internal_OnLButtonHit(e.HitCode, ref e.Handled);
        }

        private void _form_Shown(object sender, EventArgs e)
        {
            if (settings.ActivateWhenFirstShown)
            {
                _form.Activate();
                settings.ActivateWhenFirstShown = false;
            }

            Internal_OnShow();
        }

        private void _form_Resize(object sender, EventArgs e)
        {

            Internal_OnResize(_form.ClientSize.Width, _form.ClientSize.Height);
        }

        private void _form_MouseWheel(object sender, MouseEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Vector2 location = new Vector2(e.Location.X, e.Location.Y);
            Internal_OnMouseWheel(ref location, e.Delta);
        }

        private void _form_MouseUp(object sender, MouseEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Vector2 location = new Vector2(e.Location.X, e.Location.Y);
            //NOTE: WinForms mouse buttons are for some reason (n^2)*1024*1024
            MouseButtons btn = (MouseButtons)((int)e.Button / (1024 * 1024));
            Internal_OnMouseUp(ref location, btn);
            MouseButtonState[(int)btn] = false;
        }

        private void _form_MouseMove(object sender, MouseEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Vector2 location = new Vector2(e.Location.X, e.Location.Y);
            Internal_OnMouseMove(ref location);
        }

        private void _form_MouseLeave(object sender, EventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Internal_OnMouseLeave();
        }

        private void _form_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Vector2 location = new Vector2(e.Location.X, e.Location.Y);
            //NOTE: WinForms mouse buttons are for some reason (n^2)*1024*1024
            Internal_OnMouseDoubleClick(ref location, (MouseButtons)((int)e.Button / (1024 * 1024)));
        }

        private void _form_MouseDown(object sender, MouseEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Vector2 location = new Vector2(e.Location.X, e.Location.Y);
            //NOTE: WinForms mouse buttons are for some reason (n^2)*1024*1024
            MouseButtons btn = (MouseButtons)((int)e.Button / (1024 * 1024));
            Internal_OnMouseDown(ref location, btn);
            MouseButtonState[(int)btn] = true;
        }

        private void _form_KeyUp(object sender, KeyEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            Internal_OnKeyUp((KeyCode)e.KeyCode);
            KeyState[(int)e.KeyCode] = false;
        }

        private void _form_KeyDown(object sender, KeyEventArgs e)
        {
            if (!settings.AllowInput)
                return;
            //NOTE: Ensure 100% compatibility with WinFroms Keys
            Internal_OnKeyDown((KeyCode)e.KeyCode);
            KeyState[(int)e.KeyCode] = true;
        }

        private void _form_LostFocus(object sender, EventArgs e)
        {
            Internal_OnLostFocus();
        }

        private void _form_GotFocus(object sender, EventArgs e)
        {
            Internal_OnGotFocus();
            //NOTE: probably should reset states here or when LostFocus
        }

        private void _form_FormClosed(object sender, FormClosedEventArgs e)
        {
            Internal_OnClosed();
            ActiveWindows.Remove(this);
        }

        private void _form_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool cancel = e.Cancel;
            Internal_OnClosing(ClosingReason.CloseEvent, ref cancel);
            e.Cancel = cancel;
        }

        public void Dispose()
        {
            Disposer.Dispose(ref RenderTarget);
        }
    }
}
