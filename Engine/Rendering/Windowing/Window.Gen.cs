﻿using FlaxEngine.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static FlaxEngine.Win32;
using static FlaxEngine.Win32.WindowLongFlags;

namespace FlaxEngine
{
    /// <summary>
    /// Provides the ability to create, configure, show, and manage the lifetime of windows.
    /// </summary>
    public partial class Window : Object
    {
        /// <summary>
        /// Creates the new managed window using the specified settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <returns>Created window or null if failed.</returns>
        public static Window Create(CreateWindowSettings settings)
        {
            return new Window(settings);
        }

        /// <summary>
        /// Gets or sets a value that indicates whether a window is in a fullscreen mode.
        /// </summary>
        public bool IsFullscreen => false;

        /// <summary>
        /// Gets or sets a value that indicates whether a window is visible (hidden or shown).
        /// </summary>
        public bool IsVisible
        {
            get => _form.Visible;
            set => _form.Visible = value;
        }

        /// <summary>
        /// Gets a value that indicates whether a window is minimized.
        /// </summary>
        public bool IsMinimized => _form.WindowState == System.Windows.Forms.FormWindowState.Minimized;

        /// <summary>
        /// Gets a value that indicates whether a window is maximized.
        /// </summary>
        public bool IsMaximized => _form.WindowState == System.Windows.Forms.FormWindowState.Maximized;

        /// <summary>
        /// Gets or sets the position of the mouse in the window space coordinates.
        /// </summary>
        public Vector2 MousePosition
        {
            get
            {
                //Probably query Form directly cause we are doing a lot of redundant memory alocations
                return ScreenToClient(new Vector2(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y));
            }
            set
            {
                value = ClientToScreen(value);
                System.Windows.Forms.Cursor.Position = new System.Drawing.Point((int)value.X, (int)value.Y);
            } 
        }

        /// <summary>
        /// Gets the native window handle (platform specific).
        /// </summary>
        public IntPtr Handle => _form.Handle;

        /// <summary>
        /// Shows the window.
        /// </summary>
        public void Show() => _form.Show();

        /// <summary>
        /// Hides the window.
        /// </summary>
        public void Hide() => _form.Hide();

        /// <summary>
        /// Minimizes the window.
        /// </summary>
        public void Minimize() => _form.WindowState = System.Windows.Forms.FormWindowState.Minimized;

        /// <summary>
        /// Maximizes the window.
        /// </summary>
        public void Maximize() => _form.WindowState = System.Windows.Forms.FormWindowState.Maximized;

        /// <summary>
        /// Restores the window state before minimizing or maximazing.
        /// </summary>
        public void Restore() => _form.WindowState = System.Windows.Forms.FormWindowState.Normal;

        /// <summary>
        /// Closes the window.
        /// </summary>
        /// <param name="reason">The closing reason.</param>
        public void Close(ClosingReason reason = ClosingReason.CloseEvent)
        {
            _form.Close();
        }

        /// <summary>
        /// Gets or sets the client bounds of the window (client area not including border).
        /// </summary>
        public Rectangle ClientBounds
        {
            get
            {
                System.Drawing.Rectangle rct = _form.RectangleToScreen(_form.ClientRectangle);
               return new Rectangle(rct.X, rct.Y, rct.Width, rct.Height);
            }
            set
            {
                ClientPosition = value.Location;
                ClientSize = value.Size;
            }
        }

        /// <summary>
        /// Gets or sets the window position (in screen coordinates).
        /// </summary>
        public Vector2 Position
        {
            get => new Vector2(_form.Location.X, _form.Location.Y);
            set => _form.Location = new System.Drawing.Point((int)value.X, (int)value.Y);
        }

        /// <summary>
        /// Gets or sets the client position of the window (client area not including border).
        /// </summary>
        public Vector2 ClientPosition
        {
            get
            {
                System.Drawing.Rectangle rct = _form.RectangleToScreen(_form.ClientRectangle);
                return new Vector2(rct.X,rct.Y);
            }
            set
            {
                System.Drawing.Rectangle rct = _form.RectangleToScreen(_form.ClientRectangle);

                //Get size of window border
                int top = rct.Top - _form.Location.Y;
                int left = rct.Left - _form.Location.X;

                //Offset Client Position to make it Position and then set it
                Position = new Vector2(value.X - left, value.Y - top);
            }
        }

        /// <summary>
        /// Gets the window size (including border).
        /// </summary>
        public Vector2 Size => new Vector2(_form.Size.Width, _form.Size.Height);

        /// <summary>
        /// Gets or sets the size of the client area of the window (not including border).
        /// </summary>
        public Vector2 ClientSize
        {
            get => new Vector2(_form.ClientSize.Width, _form.ClientSize.Height);
            set => _form.ClientSize = new System.Drawing.Size((int)value.X, (int)value.Y);
        }

        /// <summary>
        /// Converts screen space location into window space coordinates.
        /// </summary>
        /// <param name="screenPos">The screen position.</param>
        /// <returns>The client space position.</returns>
        public Vector2 ScreenToClient(Vector2 screenPos)
        {
            System.Drawing.Point point = _form.PointToClient(new System.Drawing.Point((int)screenPos.X, (int)screenPos.Y));
            return new Vector2(point.X,point.Y);
        }

        /// <summary>
        /// Converts window space location into screen space coordinates.
        /// </summary>
        /// <param name="clientPos">The client position.</param>
        /// <returns>The screen space position.</returns>
        public Vector2 ClientToScreen(Vector2 clientPos)
        {
            System.Drawing.Point point = _form.PointToScreen(new System.Drawing.Point((int)clientPos.X, (int)clientPos.Y));
            return new Vector2(point.X, point.Y);
        }

        /// <summary>
        /// Gets or sets window title.
        /// </summary>
        public string Title
        {
            get => _form.Text;
            set => _form.Text = value;
        }

        /// <summary>
        /// Gets or set window opacity value (vaild only for windows created with SupportsTransparency flag). Opacity values are normalized to range [0;1].
        /// </summary>
        public float Opacity
        {
            get => (float)_form.Opacity;
            set => _form.Opacity = value;
        }

        /// <summary>
        /// Determines whether this window is focused.
        /// </summary>
        public bool IsFocused => _form.Focused;

        /// <summary>
        /// Focuses this window.
        /// </summary>
        public void Focus()
        {
            _form.Focus();
        }

        /// <summary>
        /// Brings window to the front of the Z order.
        /// </summary>
        /// <param name="force">True if move to the front by force, otheriwse false.</param>
        public void BringToFront(bool force = false)
        {
            _form.BringToFront();
        }

        /// <summary>
        /// Flashes the window to bring use attention.
        /// </summary>
        public void FlashWindow()
        {
        }

        /// <summary>
        /// Starts drag and drop operation
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns>The result.</returns>
        public DragDropEffect DoDragDrop(string data)
        {
            return DragDropEffect.None;
        }

        /// <summary>
        /// Starts the mouse tracking.
        /// </summary>
        /// <param name="useMouseScreenOffset">If set to true will use mouse screen offset.</param>
        public void StartTrackingMouse(bool useMouseScreenOffset)
        {
            _form.Capture = true;
        }

        /// <summary>
        /// Ends the mouse tracking.
        /// </summary>
        public void EndTrackingMouse()
        {
            _form.Capture = false;
        }

        private CursorType _cursor = CursorType.Default;

        /// <summary>
        /// Gets or sets the mouse cursor.
        /// </summary>
        public CursorType Cursor
        {
            get => _cursor;
            set { _cursor = value; System.Windows.Forms.Cursor.Current = value.ToWinForms(); }
        }

        /// <summary>
        /// Gets keyboard key state.
        /// </summary>
        /// <param name="key">Key ID to check.</param>
        /// <returns>True if user holds down the key identified by id, otherwise false.</returns>
        public bool GetKey(KeyCode key) => KeyState[(int)key];

        /// <summary>
        /// Gets keyboard key down state.
        /// </summary>
        /// <param name="key">Key ID to check.</param>
        /// <returns>True if user starts pressing down the key, otherwise false.</returns>
        public bool GetKeyDown(KeyCode key) => KeyState[(int)key];

        /// <summary>
        /// Gets keyboard key up state.
        /// </summary>
        /// <param name="key">Key ID to check.</param>
        /// <returns>True if user releases the key, otherwise false.</returns>
        public bool GetKeyUp(KeyCode key) => !KeyState[(int)key];

        /// <summary>
        /// Gets mouse button state.
        /// </summary>
        /// <param name="button">Mouse button to check.</param>
        /// <returns>True if user holds down the button, otherwise false.</returns>
        public bool GetMouseButton(MouseButtons button) => MouseButtonState[(int)button];

        /// <summary>
        /// Gets mouse button down state.
        /// </summary>
        /// <param name="button">Mouse button to check.</param>
        /// <returns>True if user starts pressing down the button, otherwise false.</returns>
        public bool GetMouseButtonDown(MouseButtons button) => MouseButtonState[(int)button];

        /// <summary>
        /// Gets mouse button up state.
        /// </summary>
        /// <param name="button">Mouse button to check.</param>
        /// <returns>True if user releases the button, otherwise false.</returns>
        public bool GetMouseButtonUp(MouseButtons button) => !MouseButtonState[(int)button];

        /// <summary>
        /// Gets or sets value indicating whenever window rendering is enabled.
        /// </summary>
        public bool RenderingEnabled
        {
            get; set;
        } = true;
    }
}