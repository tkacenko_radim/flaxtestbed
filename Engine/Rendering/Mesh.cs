﻿////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2012-2017 Flax Engine. All rights reserved.
////////////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;

namespace FlaxEngine
{
    /// <summary>
    /// Represents part of the model that is made of verticies which can be rendered (can have own transformation and material).
    /// </summary>
    public abstract class Mesh : Object, IDisposable
    {
        internal Model _model;
        internal readonly int _lodIndex;
        internal readonly int _meshIndex;

        /// <summary>
        /// Gets the parent model asset.
        /// </summary>
        /// <value>
        /// The parent model.
        /// </value>
        public Model ParentModel => _model;

        /// <summary>
        /// Gets the parent level of detail object.
        /// </summary>
        /// <value>
        /// The parent LOD object.
        /// </value>
        public ModelLOD ParentLOD => _model.LODs[_lodIndex];

        public BoundingBox Box { get; protected set; }

        public Transform Transform;

        internal Mesh(Model model, int lodIndex, int meshIndex)
        {
            _model = model;
            _lodIndex = lodIndex;
            _meshIndex = meshIndex;
        }

        protected int _vertices;
        protected int _faces;
        protected bool use16BitIndices;

        internal virtual void Load(Stream data)
        {
            //NOTE: probably expose theese variables
            string name = data.ReadStringUTF8(Model.TypeID);
            Transform = data.ReadTransform();
            bool twoSided = data.ReadBool();
            _vertices = data.ReadInt();
            _faces = data.ReadInt();
            use16BitIndices = data.ReadBool();

            if(_vertices == 0 || _faces == 0)
            {
                Debug.LogException(new Exception("Corrupted model."));
            }

            //No materials yet
            Guid matId = data.ReadGuid();
        }

        internal abstract void Draw();
        public abstract void Dispose();
    }
}