﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FlaxEngine
{
    /// <summary>
    /// Basic types of the content assets base types
    /// </summary>
    public enum ContentDomain
    {
        Invalid,
        Texture,
        CubeTexture,
        Material,
        Model,
        Prefab,
        Document,
        Other,
        Shader,
        Font,
        IESProfile,
        Scene
    }

    /// <summary>
    /// Assets objects base class
    /// </summary>
    public abstract class Asset : Object
    {
        private ContentPool Pool;
        protected bool loadError;
        /// <summary>
		/// Gets asset name
		/// </summary>
        public string Name
        {
            get;
        }

        /// <summary>
        /// Gets asset name with path, without extension
        /// </summary>
        public string FullName;

        /// <summary>
		/// Gets amount of references to that asset
		/// </summary>
        public int RefCount
        {
            get;set;
        }

        /// <summary>
		/// Returns true if asset is loaded.
		/// </summary>
        public bool IsLoaded { get; protected set; } = false;

        protected Asset() : base()
        {
        }

        public Asset(ContentPool pool, string name)
        {
            Pool = pool;
            name = Path.Combine(Path.GetDirectoryName(name),Path.GetFileNameWithoutExtension(name));
            FullName = name.Replace('/', '\\');
            int lastIndex = name.LastIndexOf('\\');
            Name = ((lastIndex != -1) ? name.Substring(lastIndex + 1) : name);
        }

        /// <summary>
        /// Load the asset
        /// </summary>
        internal void Load()
        {
            loadError = false;
            try
            {
                load();
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
                loadError = true;
            }
        }

        protected abstract void load();

        /// <summary>
		/// Stops the current thread execution and waits until asset will be loaded (loading will fail, success or be cancelled).
		/// </summary>
		/// <param name="timeoutInMiliseconds">Custom timeout value in miliseconds.</param>
		/// <returns>True if cannot load that asset (failed or has been cancelled), otherwise false.</returns>
        public bool WaitForLoaded(double timeoutInMiliseconds = 10000.0)
        {
            double elapsed = 0;
            while (!IsLoaded && (elapsed < timeoutInMiliseconds) && !loadError)
            {
                Thread.Sleep(10);
                elapsed += 10;
            }
            return !IsLoaded;

            //NOTE: More elegant but not sure about it, will test it with bigger scenes so i can check for sure
            //return !SpinWait.SpinUntil(()=>IsLoaded, TimeSpan.FromMilliseconds(timeoutInMiliseconds));
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{Name} ({GetType().Name})";
        }

        protected virtual void Unload()
        {

        }

        internal void Dispose()
        {
            if(Pool != null)
            {
                Pool.assetDisposed(this);
                Unload();
            }
        }
    }
}