﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaxEngine.Rendering;
using FlaxEngine.Utilities;

namespace FlaxEngine
{
    public class FontAsset : BinaryAsset
    {
        /// <summary>
        /// The font asset type unique ID.
        /// </summary>
        public const int TypeID = 6;

        /// <summary>
        /// The asset type content domain.
        /// </summary>
        public const ContentDomain Domain = ContentDomain.Font;


        public string FontFamily;

        protected FontAsset() : base()
        {

        }

        public FontAsset(ContentPool pool, string filename) : base(pool,filename)
        {

        }

        public Font CreateFont(int v)
        {
            return new Font(FontFamily, v);
        }

        protected override void load()
        {
            string[] parts = FullName.Split('/');
            FontFamily = parts[parts.Length - 1];
            IsLoaded = true;
        }
    }
}
