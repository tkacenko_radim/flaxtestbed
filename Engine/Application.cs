﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FlaxEngine.Win32;
using static FlaxEngine.Win32.SystemMetric;
namespace FlaxEngine
{
    /// <summary>
    /// Application management utilities.
    /// </summary>
    public static partial class Application
    {
        /// <summary>
        /// Returns true if is running 64 bit application (otherwise 32 bit).
        /// </summary>
        public static bool Is64bitApp => Environment.Is64BitProcess;

        /// <summary>
        /// Gets the name of the computer machine.
        /// </summary>
        public static string ComputerName => Environment.MachineName;

        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        public static string UserName => Environment.UserName;

        /// <summary>
        /// Gets the current user locale culture name eg. "pl-PL" or "en-US".
        /// </summary>
        public static string UserLocaleName => System.Globalization.CultureInfo.CurrentCulture.Name; 

        /// <summary>
        /// Gets size of the primary desktop.
        /// </summary>
        public static Vector2 DesktopSize => new Vector2(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));

        /// <summary>
        /// Gets size of the virtual desktop made of all the monitors attached.
        /// </summary>
        public static Vector2 VirtualDesktopSize => new Vector2(GetSystemMetrics(SM_CXVIRTUALSCREEN), GetSystemMetrics(SM_CYVIRTUALSCREEN));

        /// <summary>
        /// Gets or sets the current mouse position in the screen coordinates.
        /// </summary>
        public static Vector2 MousePosition
        {
            get { Win32.GetCursorPos(out Win32.POINT point); return new Vector2(point.X, point.Y); }
            set { Win32.SetCursorPos((int)value.X, (int)value.Y); }
        }

        /// <summary>
        /// Requests normal engine exit.
        /// </summary>
        public static void Exit()
        {
        }

        /// <summary>
        /// Immediately released all the engine resources and closes the application. Used when fatal error occurred.
        /// </summary>
        /// <param name="msg">Fatal error message. Will be saved to the log. Should contain basic information about the error.</param>
        public static void Fatal(string msg)
        {
        }
    }
}