﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;

namespace FlaxEngine
{
    /// <summary>
    /// This class is responsible for Generating and Validating passwords
    /// </summary>
    /// <remarks>
    /// This is modified version of https://github.com/defuse/password-hashing/blob/master/PasswordStorage.cs 
    /// </remarks>
    public sealed class Password
    {
        // NOTE: Can be changed without breaking existing
        private const int SALT_BYTE_SIZE = 24;
        private const int HASH_BYTE_SIZE = 24;
        private const int PBKDF2_ITERATIONS = 1000;

        // NOTE: Can't be changed without breaking existing
        private const int ITERATION_INDEX = 0;
        private const int SALT_INDEX = 1;
        private const int PBKDF2_INDEX = 2;

        private const string CHAR_SET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()";
        private const int CHAR_SET_LENGTH = 72;

        /// <summary>
        /// Generate random password of specified length
        /// </summary>
        /// <param name="length">Password length</param>
        /// <returns>Generated password</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string Generate(int length)
        {
            return Generate(new Random(), length);
        }

        /// <summary>
        /// Generate random password of specified length using speicific instance of <see cref="Random"/> class
        /// </summary>
        /// <param name="rand">Instance of <see cref="Random"/> class</param>
        /// <param name="length">Password length</param>
        /// <returns>Generated password</returns>
        public static string Generate(Random rand, int length)
        {
            StringBuilder stringBuilder = new StringBuilder();

            //Generate random password
            while (0 < length--)
            {
                stringBuilder.Append(CHAR_SET[rand.Next(CHAR_SET_LENGTH)]);
            }

            //Convert it to string
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Hashes the password
        /// </summary>
        /// <param name="password">Password to hash</param>
        /// <returns>Hashed password</returns>
        public static string CreateHash(string password)
        {
            if (password == null || password.Length < 1)
            {
                throw new ArgumentException("Invalid password.");
            }
            //Calculate salt
            byte[] salt = new byte[SALT_BYTE_SIZE];
            using (RNGCryptoServiceProvider rNGCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                rNGCryptoServiceProvider.GetBytes(salt);
            }

            byte[] hash = PBKDF2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);

            //Store all required informations about the password to one big string, so we can use it when validating
            return string.Concat(PBKDF2_ITERATIONS, ":", Convert.ToBase64String(salt), ":", Convert.ToBase64String(hash));
        }

        /// <summary>
        /// Validates if password and hash are equal
        /// </summary>
        /// <param name="password">Password to check</param>
        /// <param name="correctHash">Hash to check</param>
        /// <returns>True if valid else false</returns>
        public static bool ValidatePassword(string password, string correctHash)
        {
            if (password == null || password.Length == 0)
            {
                return false;
            }

            string[] parts = correctHash.Split(':');
            int iterations = int.Parse(parts[0]);
            byte[] salt = Convert.FromBase64String(parts[1]);
            byte[] hash = Convert.FromBase64String(parts[2]);
            byte[] testHash = PBKDF2(password, salt, iterations, hash.Length);
            return SlowEquals(hash, testHash);
        }

        private static bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
            {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }

        private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
        {
            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, salt))
            {
                pbkdf2.IterationCount = iterations;
                return pbkdf2.GetBytes(outputBytes);
            }
        }
    }
}
