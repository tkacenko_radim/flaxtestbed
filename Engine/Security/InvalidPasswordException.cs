﻿using System;

namespace FlaxEngine
{
    public sealed class InvalidPasswordException : Exception
    {
        private string _target;

        public override string Message
        {
            get
            {
                string str = "Invalid password";
                if (_target != null)
                {
                    str += " for " + _target;
                }
                return str + ".";
            }
        }

        public InvalidPasswordException()
        {
        }

        public InvalidPasswordException(string target)
        {
            _target = target;
        }
    }
}