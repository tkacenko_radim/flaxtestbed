﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public abstract class BinaryAsset : Asset
    {
        protected string _importPath = null;

        protected BinaryAsset() : base()
        {

        }

        public BinaryAsset(ContentPool pool, string filename) : base(pool, filename)
        {

        }

        /// <summary>
        /// Reimports asset from the source file.
        /// </summary>
        public virtual void Reimport()
        {
        }

        /// <summary>
        /// Gets imported file path from the asset metadata (may be null or empty if not available).
        /// </summary>
        public string ImportPath => _importPath;
    }
}
