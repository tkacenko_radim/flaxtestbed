﻿using FlaxEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    /// <summary>
 	/// Interop class used to access Flax global variables via C# API.
  	/// </summary>
    public static class Globals
    {
        /// <summary>
        /// Gets the Main engine directory path.
        /// </summary>
        public static string StartupPath { get; private set; }

        /// <summary>
        /// Gets the Temporary folder path.
        /// </summary>
        public static string TemporaryFolder { get; private set; }

        /// <summary>
        /// Gets the project directory path.
        /// </summary>
        public static string ProjectFolder { get; private set; }

        /// <summary>
        /// Gets the Engine private data folder path.
        /// </summary>
        public static string EngineFolder { get; private set; }

        /// <summary>
        /// Gets the development files fodler path.
        /// Valid only in development builds.
        /// </summary>
        public static string DevelopmentFolder { get; private set; }

        /// <summary>
        /// Gets the Editor private data folder path.
        /// Valid only in Editor.
        /// </summary>
        public static string EditorFolder { get; private set; }

        /// <summary>
        /// Gets the external tools folder path.
        /// </summary>
        public static string ToolsFolder { get; private set; }

        /// <summary>
        /// Gets the localization data folder path.
        /// </summary>
        public static string LocalizationFolder { get; private set; }

        /// <summary>
        /// Gets the configuration directory path.
        /// </summary>
        public static string ConfigFolder { get; private set; }

        // Content directory path
        /// <summary>
        /// Gets the content directory path.
        /// </summary>
        public static string ContentFolder { get; private set; }

        /// <summary>
        /// Gets the game source code directory path.
        /// </summary>
        public static string SourceFolder { get; private set; }

        /// <summary>
        /// Gets the project specific cache folder path.
        /// </summary>
        public static string ProjectCacheFolder { get; private set; }

        public static void Init() {
            string base_path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            StartupPath = Path.Combine(base_path);
            TemporaryFolder = Path.Combine(base_path,"Temp");
            ProjectFolder = Path.Combine(base_path,"Project");
            EngineFolder = Path.Combine(base_path,"Engine");
            EditorFolder = Path.Combine(base_path,"Editor");
            ToolsFolder = Path.Combine(base_path,"Tools");
            LocalizationFolder = Path.Combine(base_path,"Local");
            ConfigFolder = Path.Combine(base_path, "Config");
            ContentFolder = Path.Combine(base_path, "Content");
            SourceFolder = Path.Combine(base_path, "Source");
            ProjectCacheFolder = Path.Combine(base_path, "Cache");
        }
    }
}
