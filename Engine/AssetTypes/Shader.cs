// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class Shader : BinaryAsset
    {
        /// <summary>
        /// The shader asset type unique ID.
        /// </summary>
        public const int TypeID = 7;

        /// <summary>
        /// Extension for this asset type
        /// </summary>
        public const string Extension = ".hlsl";

        /// <summary>
        /// The asset type content domain.
        /// </summary>
        public const ContentDomain Domain = ContentDomain.Shader;

        public Rendering.Shader Effect; //Would preffer Shader... but would need to change class name :)


        public Shader(ContentPool pool, string filename) : base(pool, filename)
        {
        }

        protected override void load()
        {
            var metadata = MetaFile.LoadMetadata(FullName + ".meta");
            _importPath = (string)metadata["ImportPath"].Value;
            id = metadata.ID;

            Effect = Rendering.RenderingService.Context.CreateShader(FullName + metadata.Extension);

            IsLoaded = true;
        }
    }
}
