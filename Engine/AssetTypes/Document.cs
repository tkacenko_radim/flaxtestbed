﻿// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class Document : Asset
    {
        public Document(ContentPool pool, string name) : base(pool, name)
        {
        }

        protected override void load()
        {
        }
    }
}
