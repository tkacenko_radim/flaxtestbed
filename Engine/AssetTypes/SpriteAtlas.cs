﻿using SharpCompress.Compressors;
using SharpCompress.Compressors.Deflate;
using SharpDX.IO;
using SharpDX.WIC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    /// <summary>
    /// Sprite handle contains basic information about a sprite.
    /// </summary>
    public struct Sprite
    {
        /// <summary>
        /// Invalid sprite handle.
        /// </summary>
        public static Sprite Invalid;

        /// <summary>
        /// The parent sprite atlas.
        /// </summary>
        public SpriteAtlas Atlas;

        /// <summary>
        /// The sprite index.
        /// </summary>
        public int Index;

        /// <summary>
        /// Returns true if sprite is valid.
        /// </summary>
        /// <returns>True if this sprite handle is valid, otherwise false.</returns>
        public bool IsValid => Atlas != null && Index != -1;

        /// <summary>
        /// Contains informations about where in atlas and how big the sprite is located in pixels
        /// </summary>
        internal Rectangle BoundingBox;
    }

    public class SpriteAtlas : BinaryAsset, IDisposable
    {
        private static ImagingFactory imagingFactory = new ImagingFactory();
        private byte[] bitmapData;
        internal BitmapSource Bitmap;

        private Dictionary<string, Sprite> SpriteIndex;

        /// <summary>
        /// The sprite atlas asset type unique ID.
        /// </summary>
        public const int TypeID = 10;

        /// <summary>
        /// Extension for this asset type
        /// </summary>
        public const string Extension = ".flax";

        /// <summary>
        /// The asset type content domain.
        /// </summary>
        public const ContentDomain Domain = ContentDomain.Texture;

        protected SpriteAtlas() : base()
        {

        }

        public SpriteAtlas(ContentPool pool, string filename) : base(pool, filename)
        {
        }

        private void loadTexture(byte[] data)
        {
            BitmapDecoder bitmapDecoder = new BitmapDecoder(imagingFactory, new MemoryStream(data), DecodeOptions.CacheOnLoad);
            BitmapFrameDecode frame = bitmapDecoder.GetFrame(0);
            FormatConverter converter = new FormatConverter(imagingFactory);
            converter.Initialize(frame, PixelFormat.Format32bppPRGBA);

            Bitmap = converter;
        }

        /// <summary>
        /// Gets the sprite by name.
        /// </summary>
        /// <param name="name">The sprite name.</param>
        /// <returns>Sprite handle (may be invalid if cannot find it).</returns>
        public Sprite GetSprite(string name)
        {
            if (SpriteIndex.ContainsKey(name))
            {
                return SpriteIndex[name];
            }
            else
            {
                return Sprite.Invalid;
            }
        }

        public void Dispose()
        {
            Bitmap.Dispose();
        }

        protected override void load()
        {
            var metadata = MetaFile.LoadMetadata(FullName + ".meta");
            _importPath = (string)metadata["ImportPath"].Value;
            id = metadata.ID;

            using(MemoryStream data = new MemoryStream(File.ReadAllBytes(FullName + Extension)))
            {
                int spriteCount = data.ReadInt();

                SpriteIndex = new Dictionary<string, Sprite>(spriteCount);

                for (int i = 0; i < spriteCount; i++)
                {
                    string name = data.ReadStringUTF8(TypeID);
                    float x = data.ReadFloat();
                    float y = data.ReadFloat();
                    float w = data.ReadFloat();
                    float h = data.ReadFloat();

                    SpriteIndex.Add(name, new Sprite() { Atlas = this, Index = i, BoundingBox = new Rectangle(x, y, w, h) });
                }

                int dataLength = data.ReadInt();

                if (data.ReadByte() != 126)
                {
                    throw new Exception("Corupted file");
                }

                byte[] gzippedBitmap = new byte[dataLength];
                data.Read(gzippedBitmap, 0, dataLength);

                using (MemoryStream gzippedStream = new MemoryStream(gzippedBitmap))
                {
                    using (GZipStream bitmapStream = new GZipStream(gzippedStream, CompressionMode.Decompress))
                    {
                        byte[] segment = new byte[4096];
                        using (MemoryStream decompressedBitmap = new MemoryStream(gzippedStream.Capacity))
                        {
                            int segmentLength;
                            do
                            {
                                //Read segment of data, maximum 4096 bytes
                                segmentLength = bitmapStream.Read(segment, 0, 4096);
                                //If we read some data, then write them to stream
                                if (segmentLength > 0)
                                {
                                    decompressedBitmap.Write(segment, 0, segmentLength);
                                }
                            }
                            while (segmentLength > 0);
                            bitmapData = decompressedBitmap.ToArray();
                        }
                    }
                }


                if (data.ReadByte() != 126)
                {
                    throw new Exception("Corupted file");
                }

                if (dataLength > 0)
                    loadTexture(bitmapData);
            }

            IsLoaded = true;
        }
    }
}
