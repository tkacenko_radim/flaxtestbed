// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class IESProfile : BinaryAsset
    {
        /// <summary>
        /// The IES Profile asset type unique ID.
        /// </summary>
        public const int TypeID = 12;

        /// <summary>
        /// The asset type content domain.
        /// </summary>
        public const ContentDomain Domain = ContentDomain.IESProfile;

        public IESProfile(ContentPool pool, string filename) : base(pool, filename)
        {
        }

        protected override void load()
        {
        }
    }
}
