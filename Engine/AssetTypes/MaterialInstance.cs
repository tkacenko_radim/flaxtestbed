// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class MaterialInstance : MaterialBase
    {
        /// <summary>
        /// The material instance asset type unique ID.
        /// </summary>
        public const int TypeID = 4;

        /// <summary>
        /// Extension for this asset type
        /// </summary>
        public const string Extension = ".flax";

        /// <summary>
        /// The asset type content domain.
        /// </summary>
        public const ContentDomain Domain = ContentDomain.Material;

        public MaterialInstance(ContentPool pool, string filename) : base(pool, filename)
        {
        }

        /// <summary>
		/// Creates new <see cref="MaterialInstance"/> object.
		/// </summary>
		private MaterialInstance() : base()
        {
        }

        protected override void load()
        {
        }

        /// <summary>
		/// Gets or sets the base material. If value gets changed parameters collection is restored to the default values of the new material.
		/// </summary>
        public Material BaseMaterial
        {
			get; set;
        }
    }
}
