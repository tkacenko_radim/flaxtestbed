﻿// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class Model : BinaryAsset
    {
        /// <summary>
        /// Header used to identify model file
        /// </summary>
        public static byte[] Header = new byte[] { (byte)'M', (byte)'F', 0 };

        /// <summary>
        /// Should importer optimize meshes when importing?
        /// </summary>
        public static bool Import_OptimizeMeshes = false;

        /// <summary>
        /// Model scale when importing
        /// </summary>
        public static float Import_ModelScale = 1f;
        /// <summary>
        /// The model asset type unique ID.
        /// </summary>
        public const int TypeID = 3;

        /// <summary>
        /// Extension for this asset type
        /// </summary>
        public const string Extension = ".flax";

        /// <summary>
        /// The asset type content domain.
        /// </summary>
        public const ContentDomain Domain = ContentDomain.Model;

        /// <summary>
        /// Gets amount of loaded model LODs.
        /// </summary>
        public int LoadedLODs
        {
            get;
            private set;
        }
        /// <summary>
        /// Total count of Loaded and not loaded LODs
        /// </summary>
        private ModelLOD[] _lods;

        /// <summary>
        /// Gets the model level of details collection. Each level of detail contains array of meshes.
        /// </summary>
        /// <value>
        /// The LODs array. It's null if model has not been loaded yet.
        /// </value>
        public ModelLOD[] LODs => _lods;

        internal void Internal_OnUnload()
        {
            // Clear cached data
            _lods = null;
        }

        private Model() : base()
        {

        }

        public Model(ContentPool pool, string filename) : base(pool, filename)
        {
        }

        public BoundingBox Box
        {
            get {
                Vector3 min = Vector3.Maximum;
                Vector3 max = Vector3.Minimum;
                if (LoadedLODs > 0)
                {
                    for (int i = 0; i < _lods[0].Meshes.Length; i++)
                    {
                        min = Vector3.Min(min, _lods[0].Meshes[i].Box.Minimum);
                        max = Vector3.Max(max, _lods[0].Meshes[i].Box.Maximum);
                    }
                }
                return new BoundingBox(min, max);

            }
        }

        protected override void load()
        {
            var metadata = MetaFile.LoadMetadata(FullName + ".meta");
            _importPath = (string)metadata["ImportPath"].Value;
            id = metadata.ID;

            using (MemoryStream data = new MemoryStream(File.ReadAllBytes(FullName + Extension)))
            {
                byte[] header = new byte[3];
                data.Read(header, 0, 3);

                int version = data.ReadInt();
                switch (version)
                {
                    case 1:
                        {
                            int lodCount = data.ReadInt();

                            if (lodCount < 1)
                            {
                                Debug.LogException(new Exception("LOD count musts be 1 or higher"));
                            }

                            _lods = new ModelLOD[lodCount];

                            for (int i = 0; i < lodCount; i++)
                            {
                                int meshCount = data.ReadInt();
                                _lods[i] = new ModelLOD(this, i, meshCount, data);
                                LoadedLODs++;
                            }

                            if (data.ReadByte() != 126)
                            {
                                Debug.LogException(new Exception("Invalid data"));
                            }
                        }
                        break;
                    default:
                        Debug.LogException(new Exception($"This version is not supported (${version})"));
                        return;
                }
            }
            Console.WriteLine("{0} is loaded",this.ToString());
            IsLoaded = true;
        }

        public void Draw()
        {
            if(LoadedLODs > 0)
            {
                for (int i = 0; i < LODs[0].Meshes.Length; i++)
                {
                    LODs[0].Meshes[i].Draw();
                }
            }
        }
    }
}
