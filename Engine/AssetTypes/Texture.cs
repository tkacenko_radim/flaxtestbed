﻿// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class Texture : BinaryAsset
    {
        /// <summary>
        /// The texture asset type unique ID.
        /// </summary>
        public const int TypeID = 1;

        /// <summary>
        /// The asset type content domain.
        /// </summary>
        public const ContentDomain Domain = ContentDomain.Texture;

        public Texture(ContentPool pool, string filename) : base(pool, filename)
        {
        }

        protected override void load()
        {
        }
    }
}
