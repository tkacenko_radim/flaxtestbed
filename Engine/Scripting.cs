﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public static partial class Scripting
    {
        /// <summary>
        /// Occurs on scripting update.
        /// </summary>
        public static event Action OnUpdate;

        /// <summary>
        /// Occurs on scripting 'late' update.
        /// </summary>
        public static event Action OnLateUpdate;

        /// <summary>
        /// Occurs on scripting fxied update.
        /// </summary>
        public static event Action OnFixedUpdate;

        public static void Internal_Update()
        {
            OnUpdate?.Invoke();
        }

        public static void Internal_LateUpdate()
        {
            OnLateUpdate?.Invoke();
        }

        public static void Internal_FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }
    }
}