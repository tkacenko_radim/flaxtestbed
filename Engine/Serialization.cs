﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public static class Serialization
    {
        public static void WriteUTF8(this Stream fs, string data, byte offset = 0)
        {
            if (string.IsNullOrEmpty(data))
            {
                fs.Write(0);
                return;
            }

            byte[] text = Encoding.UTF8.GetBytes(data);

            fs.Write(text.Length);

            if (offset > 0)
                for (int i = 0; i < text.Length; i++)
                {
                    text[i] += offset;
                }

            fs.Write(text, 0, text.Length);
        }

        public static void Write(this Stream fs, bool value)
        {
            fs.WriteByte(value ? (byte)1 : (byte)0);
        }

        public static void Write(this Stream fs, char value)
        {
            fs.WriteByte((byte)value);
        }

        public static void Write(this Stream fs, short value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            fs.Write(bytes, 0, 2);
        }

        public static void Write(this Stream fs, ushort value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            fs.Write(bytes, 0, 2);
        }

        public static void Write(this Stream fs, int value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            fs.Write(bytes, 0, 4);
        }

        public static void Write(this Stream fs, uint value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            fs.Write(bytes, 0, 4);
        }

        public static void Write(this Stream fs, long value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            fs.Write(bytes, 0, 8);
        }

        public static void Write(this Stream fs, float value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            fs.Write(bytes, 0, 4);
        }

        public static void Write(this Stream fs, float[] value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                byte[] bytes = BitConverter.GetBytes(value[i]);
                fs.Write(bytes, 0, 4);
            }
        }

        public static void Write(this Stream fs, Half val)
        {
            fs.Write(val.RawValue);
        }

        public static void Write(this Stream fs, Half2 val)
        {
            fs.Write(val.X.RawValue);
            fs.Write(val.Y.RawValue);
        }

        public static void Write(this Stream fs, Half3 val)
        {
            fs.Write(val.X.RawValue);
            fs.Write(val.Y.RawValue);
            fs.Write(val.Z.RawValue);
        }

        public static void Write(this Stream fs, Half4 val)
        {
            fs.Write(val.X.RawValue);
            fs.Write(val.Y.RawValue);
            fs.Write(val.Z.RawValue);
            fs.Write(val.W.RawValue);
        }

        public static void Write(this Stream fs, ref Half val)
        {
            fs.Write(val.RawValue);
        }

        public static void Write(this Stream fs, ref Half2 val)
        {
            fs.Write(val.X.RawValue);
            fs.Write(val.Y.RawValue);
        }

        public static void Write(this Stream fs, ref Half3 val)
        {
            fs.Write(val.X.RawValue);
            fs.Write(val.Y.RawValue);
            fs.Write(val.Z.RawValue);
        }

        public static void Write(this Stream fs, ref Half4 val)
        {
            fs.Write(val.X.RawValue);
            fs.Write(val.Y.RawValue);
            fs.Write(val.Z.RawValue);
            fs.Write(val.W.RawValue);
        }

        public static void Write(this Stream fs, Guid value)
        {
            byte[] bytes = value.ToByteArray();
            fs.Write(bytes, 0, 16);
        }

        public static string ReadStringUTF8(this Stream fs, byte offset = 0)
        {
            int length = fs.ReadInt();

            if (length <= 0)
                return string.Empty;

            byte[] text = new byte[length];
            fs.Read(text, 0, text.Length);

            if (offset > 0)
                for (int i = 0; i < length; i++)
                {
                    text[i] -= offset;
                }

            return Encoding.UTF8.GetString(text);
        }

        public static void Write(this Stream fs, BoundingBox val)
        {
            fs.Write(ref val.Minimum);
            fs.Write(ref val.Maximum);
        }

        public static void Write(this Stream fs, Vector2 val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, ref Vector2 val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, Vector3 val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, ref Vector3 val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, Vector4 val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, ref Vector4 val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, Quaternion val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, ref Quaternion val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, Transform val)
        {
            fs.Write(ref val.Translation);
            fs.Write(ref val.Orientation);
            fs.Write(ref val.Scale);
        }

        public static void Write(this Stream fs, ref Transform val)
        {
            fs.Write(ref val.Translation);
            fs.Write(ref val.Orientation);
            fs.Write(ref val.Scale);
        }

        public static void Write(this Stream fs, double val)
        {
            byte[] bytes = BitConverter.GetBytes(val);
            fs.Write(bytes, 0, 8);
        }

        public static void Write(this Stream fs, Matrix val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, TimeSpan val)
        {
            byte[] bytes = BitConverter.GetBytes(val.Ticks);
            fs.Write(bytes, 0, 8);
        }

        public static void Write(this Stream fs, Color val)
        {
            fs.Write(val.ToArray());
        }

        public static void Write(this Stream fs, Vector4[] val)
        {
            if(val == null || val.Length == 0)
            {
                fs.Write(0);
                return;
            }
            fs.Write(val.Length);
            for (int i = 0; i < val.Length; i++)
            {
                fs.Write(val[i]);
            }
        }

        public static void Write(this Stream fs, CommonValue val)
        {
            CommonType valueType = val.GetValueType();
            fs.WriteByte((byte)valueType);
            switch (valueType)
            {
                case CommonType.Bool:
                    fs.Write((bool)val.Value);
                    return;
                case CommonType.Byte:
                    fs.WriteByte((byte)val.Value);
                    return;
                case CommonType.Char:
                    fs.Write((char)val.Value);
                    return;
                case CommonType.Short:
                    fs.Write((short)val.Value);
                    return;
                case CommonType.UShort:
                    fs.Write((ushort)val.Value);
                    return;
                case CommonType.Int:
                    fs.Write((int)val.Value);
                    return;
                case CommonType.UInt:
                    fs.Write((uint)val.Value);
                    return;
                case CommonType.Float:
                    fs.Write((float)val.Value);
                    return;
                case CommonType.Double:
                    fs.Write((double)val.Value);
                    return;
                case CommonType.Vector2:
                    fs.Write((Vector2)val.Value);
                    return;
                case CommonType.Vector3:
                    fs.Write((Vector3)val.Value);
                    return;
                case CommonType.Vector4:
                    fs.Write((Vector4)val.Value);
                    return;
                case CommonType.Quaternion:
                    fs.Write((Quaternion)val.Value);
                    return;
                case CommonType.Matrix:
                    fs.Write((Matrix)val.Value);
                    return;
                case CommonType.TimeSpan:
                    fs.Write((TimeSpan)val.Value);
                    return;
                case CommonType.Color:
                    fs.Write((Color)val.Value);
                    return;
                case CommonType.Transform:
                    fs.Write((Transform)val.Value);
                    return;
                case CommonType.String:
                    fs.WriteUTF8((string)val.Value);
                    return;
                case CommonType.BoundingBox:
                    fs.Write((BoundingBox)val.Value);
                    return;
                case CommonType.ByteArray:
                    fs.Write(((byte[])val.Value).Length);
                    fs.Write((byte[])val.Value, 0, ((byte[])val.Value).Length);
                    return;
                case CommonType.Guid:
                    fs.Write((Guid)val.Value);
                    return;
                case CommonType.Vector4Array:
                    fs.Write((Vector4[])val.Value);
                    return;
                default:
                    throw new SystemException("Invalid Common Value type: " + (int)valueType);
            }
        }

        public static bool ReadBool(this Stream fs)
        {
            return fs.ReadByte() == 1;
        }

        public static short ReadShort(this Stream fs)
        {
            byte[] array = new byte[2];
            fs.Read(array, 0, 2);
            return BitConverter.ToInt16(array, 0);
        }

        public static ushort ReadUShort(this Stream fs)
        {
            byte[] array = new byte[2];
            fs.Read(array, 0, 2);
            return BitConverter.ToUInt16(array, 0);
        }

        public static int ReadInt(this Stream fs)
        {
            byte[] array = new byte[4];
            fs.Read(array, 0, 4);
            return BitConverter.ToInt32(array, 0);
        }
        public static uint ReadUInt(this Stream fs)
        {
            byte[] array = new byte[4];
            fs.Read(array, 0, 4);
            return BitConverter.ToUInt32(array, 0);
        }

        public static long ReadLong(this Stream fs)
        {
            byte[] array = new byte[8];
            fs.Read(array, 0, 8);
            return BitConverter.ToInt64(array, 0);
        }

        public static ulong ReadULong(this Stream fs)
        {
            byte[] array = new byte[8];
            fs.Read(array, 0, 8);
            return BitConverter.ToUInt64(array, 0);
        }

        public unsafe static float ReadFloat(this Stream fs)
        {
            byte[] array = new byte[4];
            fs.Read(array, 0, 4);
            return BitConverter.ToSingle(array, 0);
        }

        public static Guid ReadGuid(this Stream fs)
        {
            byte[] array = new byte[16];
            fs.Read(array, 0, 16);
            return new Guid(array);
        }

        public static double ReadDouble(this Stream fs)
        {
            byte[] array = new byte[8];
            fs.Read(array, 0, 8);
            return BitConverter.ToDouble(array, 0);
        }

        public unsafe static float[] ReadFloats(this Stream fs, uint size)
        {
            float[] array = new float[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = fs.ReadFloat();
            }
            return array;
        }

        public static Vector2 ReadVector2(this Stream fs)
        {
            return new Vector2(fs.ReadFloats(2));
        }

        public static Vector3 ReadVector3(this Stream fs)
        {
            return new Vector3(fs.ReadFloats(3));
        }

        public static Vector4 ReadVector4(this Stream fs)
        {
            return new Vector4(fs.ReadFloats(4));
        }

        public static Quaternion ReadQuaternion(this Stream fs)
        {
            return new Quaternion(fs.ReadFloats(4));
        }

        public static Matrix ReadMatrix(this Stream fs)
        {
            return new Matrix(fs.ReadFloats(16));
        }

        public static TimeSpan ReadTimeSpan(this Stream fs)
        {
            return new TimeSpan(fs.ReadLong());
        }

        public static Color ReadColor(this Stream fs)
        {
            return new Color(fs.ReadFloats(4));
        }

        public static Transform ReadTransform(this Stream fs)
        {
            return new Transform(fs.ReadVector3(), fs.ReadQuaternion(), fs.ReadVector3());
        }

        public static BoundingBox ReadBoundingBox(this Stream fs)
        {
            return new BoundingBox(fs.ReadVector3(), fs.ReadVector3());
        }

        public static Vector4[] ReadVector4s(this Stream fs)
        {
            int count = fs.ReadInt();
            Vector4[] array;
            if (count > 0)
            {
                array = new Vector4[count];
                for (int i = 0; i < count; i++)
                {
                    array[i] = fs.ReadVector4();
                }
            }
            else
            {
                array = new Vector4[0];
            }
            return array;
        }

        public static CommonValue ReadCommonValue(this Stream fs)
        {
            CommonType commonType = (CommonType)fs.ReadByte();
            switch (commonType)
            {
                case CommonType.Bool:
                    return new CommonValue(fs.ReadBool());
                case CommonType.Byte:
                    return new CommonValue(fs.ReadByte());
                case CommonType.Char:
                    return new CommonValue((char)fs.ReadByte());
                case CommonType.Short:
                    return new CommonValue(fs.ReadShort());
                case CommonType.UShort:
                    return new CommonValue(fs.ReadUShort());
                case CommonType.Int:
                    return new CommonValue(fs.ReadInt());
                case CommonType.UInt:
                    return new CommonValue(fs.ReadUInt());
                case CommonType.Float:
                    return new CommonValue(fs.ReadFloat());
                case CommonType.Double:
                    return new CommonValue(fs.ReadDouble());
                case CommonType.Vector2:
                    return new CommonValue(fs.ReadVector2());
                case CommonType.Vector3:
                    return new CommonValue(fs.ReadVector3());
                case CommonType.Vector4:
                    return new CommonValue(fs.ReadVector4());
                case CommonType.Quaternion:
                    return new CommonValue(fs.ReadQuaternion());
                case CommonType.Matrix:
                    return new CommonValue(fs.ReadMatrix());
                case CommonType.TimeSpan:
                    return new CommonValue(fs.ReadTimeSpan());
                case CommonType.Color:
                    return new CommonValue(fs.ReadColor());
                case CommonType.Transform:
                    return new CommonValue(fs.ReadTransform());
                case CommonType.String:
                    return new CommonValue(fs.ReadStringUTF8());
                case CommonType.BoundingBox:
                    return new CommonValue(fs.ReadBoundingBox());
                case CommonType.ByteArray:
                    {
                        int length = fs.ReadInt();
                        byte[] data = new byte[length];
                        fs.Read(data, 0, length);
                        return new CommonValue(data);
                    }
                case CommonType.Guid:
                    return new CommonValue(fs.ReadGuid());
                case CommonType.Vector4Array:
                    return new CommonValue(fs.ReadVector4s());
                default:
                    throw new SystemException("Invalid Common Value type: " + (int)commonType);
            }
        }
    }
}
