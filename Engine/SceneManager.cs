﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public class SceneManager
    {
        public static Action<Scene, Guid> OnSceneSaveError { get; set; }
        public static Action<Scene, Guid> OnSceneLoaded { get; set; }
        public static Action<Scene, Guid> OnSceneLoadError { get; set; }
        public static Action<Scene, Guid> OnSceneLoading { get; set; }
        public static Action<Scene, Guid> OnSceneSaved { get; set; }
        public static Action<Scene, Guid> OnSceneSaving { get; set; }
        public static Action<Scene, Guid> OnSceneUnloaded { get; set; }
        public static Action<Scene, Guid> OnSceneUnloading { get; set; }
        public static Scene[] Scenes { get; set; }

        public static int LoadedScenesCount { get; }

        public static void LoadSceneAsync(Guid guid)
        {
            throw new NotImplementedException();
        }

        public static void UnloadSceneAsync(Scene scene)
        {
            throw new NotImplementedException();
        }

        public static void SaveAllScenesAsync()
        {
            throw new NotImplementedException();
        }

        public static void SaveSceneAsync(Scene scene)
        {
            throw new NotImplementedException();
        }
    }
}
