﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    /// <summary>
	/// Base class for all actor types on the scene
	/// </summary>
    public abstract class Actor : Object, ITransformable
    {
        /// <summary>
		/// Gets or sets parent actor (or null if actor has no parent)
		/// </summary>
        public Actor Parent
        {
            get => null;
            set { }
        }

        /// <summary>
        /// Sets parent actor with parameters
        /// </summary>
        /// <param name="newParent">New parent to assign</param>
        /// <param name="worldPositionLock">Should actor world positions remain the same after parent change</param>

        public void SetParent(Actor newParent, bool worldPositionLock = false)
        {

        }

        /// <summary>
        /// Gets or sets zero-based index in parent actor children list.
        /// </summary>
        public int OrderInParent
        {
            get => 0;
            set { }
        }

        /// <summary>
        /// Gets or sets the actor name.
        /// </summary>
        public string Name
        {
            get => "Actor";
            set { }
        }

        /// <summary>
        /// Gets or sets actor static fags
        /// </summary>
        public StaticFlags StaticFlags
        {
            get => StaticFlags.None;
            set { }
        }

        /// <summary>
        /// Gets or sets active state of actor
        /// </summary>
        public bool IsActive
        {
            get => true;
            set { }
        }

        /// <summary>
        /// Gets actor activation state in hierarchy
        /// </summary>
        public bool IsActiveInHierarchy
        {
            get => true;
        }

        /// <summary>
        /// Gets or sets actor's world transform position
        /// </summary>
        public Vector3 Position
        {
            get => Vector3.Zero;
            set { }
        }

        /// <summary>
        /// Gets or sets actor's world transform orientation
        /// </summary>
        public Quaternion Orientation
        {
            get => Quaternion.Identity;
            set { }
        }

        /// <summary>
        /// Gets or sets actor's world transform scale
        /// </summary>
        public Vector3 Scale
        {
            get => Vector3.One;
            set { }
        }

        /// <summary>
        /// Gets or sets actor's world a three dimensional mathematical transformation
        /// </summary>
        public Transform Transform
        {
            get => Transform.Identity;
            set { }
        }

        /// <summary>
        /// Gets or sets actor's local transform position
        /// </summary>
        public Vector3 LocalPosition
        {
            get => Vector3.Zero;
            set { }
        }

        /// <summary>
        /// Gets or sets actor's local transform orientation
        /// </summary>
        public Quaternion LocalOrientation
        {
            get => Quaternion.Identity;
            set { }
        }

        /// <summary>
        /// Gets or sets actor's local transform scale
        /// </summary>
        public Vector3 LocalScale
        {
            get => Vector3.One;
            set { }
        }

        /// <summary>
        /// Gets or sets actor's world a three dimensional mathematical transformation
        /// </summary>
        public Transform LocalTransform
        {
            get => Transform.Identity;
            set { }
        }

        /// <summary>
        /// Gets amount of children
        /// </summary>
        public int ChildCount
        {
            get => 0;
        }

        /// <summary>
        /// Creates new <see cref="Actor"/> object.
        /// </summary>
        protected Actor() : base()
        {

        }

        /// <summary>
        /// Gets child actor at given index
        /// </summary>
        /// <param name="index">Child's actor index</param>
        /// <returns>Child actor</returns>
        public Actor GetChild(int index)
        {
            return null;

        }

        /// <summary>
        /// Gets child actor with given name
        /// </summary>
        /// <param name="name">Child's actor name</param>
        /// <returns>Child actor</returns>
        public Actor GetChild(string name)
        {
            return null;
        }

        /// <summary>
        /// Tries to find actor with given name in this actor tree
        /// </summary>
        /// <param name="name">Actor's name to find</param>
        /// <returns>Actor instance if found, null otherwise</returns>
        public Actor FindActor(string name)
        {
            return null;
        }

        /// <summary>
        /// Tries to find actor with given name on the scene
        /// </summary>
        /// <param name="name">Actor's name to find</param>
        /// <returns>Actor instance if found, null otherwise</returns>
        public static Actor Find(string name)
        {
            return null;
        }

        /// <summary>
        /// Searches for a child actor of a specific type. If there are multiple actors matching the type, only the first one found is returned.
        /// </summary>
        /// <typeparam name="T">Type of the actor to search for. Includes any actors derived from the type.</typeparam>
        /// <returns>Actor instance if found, null otherwise</returns>
        public T GetChild<T>() where T : Actor
        {
            return null;
        }

        /// <summary>
        /// Searches for all actors of a specific type.
        /// </summary>
        /// <typeparam name="T">Type of the actor to search for. Includes any actors derived from the type.</typeparam>
        /// <returns>All actors matching the specified type</returns>
        public T[] GetChildren<T>() where T : Actor
        {
            return Array.ConvertAll(new Actor[] { }, x => (T)x);
        }

        /// <summary>
        /// Returns a list of all actors attached to this object.
        /// </summary>
        /// <returns>All actors attached to this object.</returns>
        public Actor[] GetChildren()
        {
            return new Actor[] { };
        }

        /// <summary>
        /// Searches for a child script of a specific type. If there are multiple scripts matching the type, only the first one found is returned.
        /// </summary>
        /// <typeparam name="T">Type of the actor to search for. Includes any scripts derived from the type.</typeparam>
        /// <returns>Script instance if found, null otherwise.</returns>
        public T GetScript<T>() where T : Script
        {
            return null;
        }

        /// <summary>
        /// Searches for all scripts of a specific type.
        /// </summary>
        /// <typeparam name="T">Type of the script to search for. Includes any scripts derived from the type.</typeparam>
        /// <returns>All scripts matching the specified type.</returns>
        public T[] GetScripts<T>() where T : Script
        {
            return Array.ConvertAll(new Script[] { }, x => (T)x);
        }

        /// <summary>
        /// Returns a list of all scripts attached to this object.
        /// </summary>
        /// <returns>All scripts attached to this object.</returns>
        public Script[] GetScripts()
        {
            return new Script[] { };
        }

        /// <summary>
        /// Gets bounding box that contains actor object (single actor, no children included)
        /// </summary>
        public BoundingBox Box
        {
            get;
        }

        /// <summary>
        /// Gets bounding box that contains actor object and all it's children (children included in recursive way)
        /// </summary>
        public BoundingBox BoxWithChildren
        {
            get;
        }

        /// <summary>
        /// Returns true if actor has loaded content
        /// </summary>
        public bool HasContentLoaded
        {
            get;
        }

        /// <summary>
        /// Returns true if actor has fully loaded content
        /// </summary>
        public bool HasContentFullyLoaded
        {
            get;
        }

        /// <summary>
        /// Resets actor local transform
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ResetLocalTransform()
        {
            LocalTransform = Transform.Identity;
        }

        /// <summary>
        /// Returns true if actor has parent
        /// </summary>
        public bool HasParent => Parent != null;

        /// <summary>
        /// Returns true if actor has any children
        /// </summary>
        public bool HasChildren => ChildCount > 0;

        /// <summary>
        /// Returns true if actor object has child actor with given name
        /// </summary>
        /// <param name="name">Actor name</param>
        /// <returns></returns>
        public bool HasChild(string name)
        {
            return false;
        }

        /// <summary>
        /// Sets actor parent to this object
        /// </summary>
        /// <param name="actor">Actor to link</param>
        /// <param name="worldPositionStays">Should actor world positions remain the same after parent change?</param>
        public void AddChild(Actor actor, bool worldPositionStays = true)
        {
            actor?.SetParent(this);
        }
        /// <inheritdoc />
        public override string ToString()
        {
            return $"{Name} ({GetType().Name})";
        }

        /// <summary>
        /// Gets or sets the actor direction vector (aka forward direction).
        /// </summary>
        public Vector3 Direction
        {
            get { return Vector3.ForwardLH * Orientation; }
            set
            {
                Vector3 right = Vector3.Cross(value, Vector3.Up);
                Vector3 up = Vector3.Cross(right, value);
                //up = Vector3.Up;
                Orientation = Quaternion.LookRotation(value, up);
            }
        }

        /// <summary>
        /// The local rotation as Euler angles in degrees.
        /// The x, y, and z angles represent a rotation z degrees around the z axis, x degrees around the x axis, and y degrees around the y axis (in that order).
        /// Angles order (xyz): pitch, yaw and roll.
        /// </summary>
        public Vector3 LocaEulerAngles
        {
			get { return LocalOrientation.EulerAngles; }
            set
            {
                Quaternion orientation;
                Quaternion.Euler(ref value, out orientation);
                LocalOrientation = orientation;
            }
        }

        /// <summary>
        /// The rotation as Euler angles in degrees.
        /// The x, y, and z angles represent a rotation z degrees around the z axis, x degrees around the x axis, and y degrees around the y axis (in that order).
        /// Angles order (xyz): pitch, yaw and roll.
        /// </summary>
        public Vector3 EulerAngles
        {
			get { return Orientation.EulerAngles; }
            set
            {
                Quaternion orientation;
                Quaternion.Euler(ref value, out orientation);
                Orientation = orientation;
            }
        }

        /// <summary>
        /// Returns true if object is fully static on the scene
        /// </summary>
        public bool IsStatic => StaticFlags == StaticFlags.FullyStatic;

        /// <summary>
 		/// Determines whether the specified object is in a hierarchy (one of the children or lower).
 		/// </summary>
 		/// <param name="actor">The actor to check,</param>
 		/// <returns>True if given actor is part of the hierarchy, otherwise false.</returns>
        public bool ContainsInHierarchy(Actor actor)
        {
            return false;
        }

        /// <summary>
 		/// Determines whether the specified object is one of the children.
 		/// </summary>
 		/// <param name="actor">The actor to check,</param>
 		/// <returns>True if given actor is a child, otherwise false.</returns>
        public bool ContainsChild(Actor actor)
        {
            return false;
        }

        /// <summary>
        /// Determines if there is an intersection between the actor and a ray.
        /// </summary>
        /// <param name="ray">The ray to test.</param>
        /// <param name="distance">When the method completes and returns true, contains the distance of the intersection.</param>
        /// <returns>True if the actor is intersected by the ray, otherwise false.</returns>
        public bool IntersectsItself(ref Ray ray, ref float distance)
        {
            return false;
        }
    }
}
