﻿////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2012-2017 Flax Engine. All rights reserved.
////////////////////////////////////////////////////////////////////////////////////

namespace FlaxEngine
{
    /// <summary>
    /// Represents a part of the model actor mesh infos collection. Contains information about how to render <see cref="Mesh"/>.
    /// </summary>
    public sealed class MeshInfo
    {
        internal ModelActor _modelActor;
        internal readonly int _index;

        /// <summary>
        /// Gets the parent model actor.
        /// </summary>
        /// <value>
        /// The parent model actor.
        /// </value>
        public ModelActor ParentActor => _modelActor;

        /// <summary>
        /// Gets or sets the mesh local transform.
        /// </summary>
        /// <value>
        /// The lcoal transform.
        /// </value>
        public Transform Transform
        {
            get =>_modelActor.Model.LODs[0].Meshes[_index].Transform;
            set => _modelActor.Model.LODs[0].Meshes[_index].Transform = value;
        }

        /// <summary>
        /// Gets or sets the material used to render the mesh.
        /// If value if null then model asset mesh default material will be used as a fallback.
        /// </summary>
        /// <value>
        /// The material.
        /// </value>
        public MaterialBase Material
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the scale in lightmap (per mesh).
        /// Final mesh scale in lightmap is alsow multiplied by <see cref="ModelActor.ScaleInLightmap"/> and global scene scale parameter.
        /// </summary>
        /// <value>
        /// The scale in lightmap.
        /// </value>
        public float ScaleInLightmap
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="MeshInfo"/> is visible.
        /// </summary>
        /// <value>
        ///   <c>true</c> if visible; otherwise, <c>false</c>.
        /// </value>
        public bool Visible
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the mesh index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public int Index => _index;

        internal MeshInfo(ModelActor model, int index)
        {
            _modelActor = model;
            _index = index;
        }
    }
}