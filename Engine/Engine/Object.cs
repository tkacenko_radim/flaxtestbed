﻿using System;

namespace FlaxEngine
{
    /// <summary>
    /// Base class for all objects Flax can reference.
    /// </summary>
    public abstract class Object
    {
        //NOTE: We don't have any unmanaged stuff, that's why everything related to it is not implemented

        [NonSerialized]
        internal Guid id = Guid.Empty;

        /// <summary>
        /// Gets unique object ID
        /// </summary>
        public Guid ID
        {
            get { return id; }
        }

        protected Object()
        {

        }

        /// <summary>
        /// Check if object exists
        /// </summary>
        /// <param name="obj">Object to check</param>
        public static implicit operator bool(Object obj)
        {
            return obj != null;
        }

        /// <summary>
        /// Destroys the specified object.
        /// The object obj will be destroyed now or ather the time specified in seconds from now.
        /// If obj is a Script it will remove the component from the Actor and destroy it.
        /// If obj is a Actor it will remove it from the Scene and destroy it and all its Scripts and all children of the Actor.
        /// Actual object destruction is always delayed until after the current Update loop, but will always be done before rendering.
        /// </summary>
        /// <param name="obj">The object to destroy.</param>
        /// <param name="timeLeft">The time left to destroy object (in seconds).</param>
        public static void Destroy(ref Object obj, float timeLeft = 0.0f)
        {
            Disposer.Dispose(ref obj);
            //TODO: Implement, after implementing, replace all Dispose methods with this
        }
    }
}
