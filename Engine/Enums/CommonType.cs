﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    //NOTE: Expand this, when physics is implemented
    public enum CommonType
    {
        Null,
        Unknown,
        Bool,
        Byte,
        Char,
        Short,
        UShort,
        Int,
        UInt,
        Float,
        Double,
        Vector2,
        Vector3,
        Vector4,
        Quaternion,
        Matrix,
        TimeSpan,
        Color,
        Transform,
        String,
        BoundingBox,
        ByteArray,
        Guid,
        Vector4Array
    }
}
