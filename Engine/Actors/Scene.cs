﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class Scene : Actor
    {
        private Scene() : base()
        {

        }

        /// <summary>
        /// Gets path to the scene file. It's valid only in Editor.
        /// </summary>
        public string Path
        {
			get; set;
        }

        /// <summary>
        /// Gets filename of the scene file. It's valid only in Editor.
        /// </summary>
        public string Filename
        {
			get; set;
        }

        /// <summary>
        /// Gets path to the scene data folder. It's valid only in Editor.
        /// </summary>
        public string DataFolderPath
        {
			get; set;
        }

        /// <summary>
        /// Unloads given scene. Done in sync, in the end of the next engine tick.
        /// </summary>
        public static void Unload()
        {
        }

        /// <summary>
        /// Saves this scene to the asset.
        /// </summary>
        /// <returns>True if action fails, otherwise false.</returns>
        public bool SaveScene(Scene scene)
        {
            // return SceneManager.SaveScene(this);
            return false;
        }

        /// <summary>
        /// Saves this scene to the asset. Done in the background.
        /// </summary>
        public void SaveSceneAsync(Scene scene)
        {
            SceneManager.SaveSceneAsync(this);
        }

        /// <summary>
        /// Unloads this scene.
        /// </summary>
        /// <returns>True if action fails, otherwise false.</returns>
        public bool UnloadScene(Scene scene)
        {
            // return SceneManager.UnloadScene(this);
            return false;
        }

        /// <summary>
        /// Unloads this scene. Done in the background.
        /// </summary>
        public void UnloadSceneAsync(Scene scene)
        {
            SceneManager.UnloadSceneAsync(this);
        }

    }
}
