﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine.Actors
{
    public sealed class BoxBrush : Actor
    {
        private BoxBrush() : base()
        {

        }

        public float ScaleInLightmap
        {
            get; set;
        }

        public Vector3 Size
        {
            get; set;
        }

        public BrushMode Mode
        {
            get; set;
        }
    }
}
