﻿// Flax Engine scripting API

namespace FlaxEngine
{

    public sealed class EnvironmentProbe : Actor
    {
        private EnvironmentProbe() : base()
        {

        }

        public bool AffectsWorld
        {
            get; set;
        }

        public float Brightness
        {
            get; set;
        }

        public float Radius
        {
            get; set;
        }

        public float ScaledRadius
        {
            get { return Scale.MaxValue * Radius; }
        }
    }
}