﻿// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class Skybox : Actor
    {
        private Skybox() : base()
        {

        }
        /// <summary>
        /// Gets or sets value indicating if visual element affects the world
        /// </summary>
        public bool AffectsWorld
        {
			get; set;
        }

        /// <summary>
        /// Gets or sets skybox color
        /// </summary>
        public Color Color
        {
			get; set;
        }
    }
}