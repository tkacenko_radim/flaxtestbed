﻿// Flax Engine scripting API

namespace FlaxEngine
{
    public sealed class DirectionalLight : Actor
    {
        private DirectionalLight() : base()
        {

        }

        public bool AffectsWorld
        {
            get; set;
        }

        public Color Color
        {
            get; set;
        }

        public float Brughtness
        {
            get; set;
        }

        public float ShadowsDistance
        {
            get; set;
        }

        public float ShadowsFadeDistance
        {
            get; set;
        }

        public ShadowsCastingMode ShadowsMode
        {
            get; set;
        }

        public float MinimumRoughness
        {
            get; set;
        }
    }
}