﻿// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class PointLight : Actor
    {
        private PointLight() : base()
        {

        }

        /// <summary>
        /// Gets or sets value indicating if visual element affects the world
        /// </summary>
        public bool AffectsWorld
        {
			get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Color Color
        {
			get; set;
        }

        /// <summary>
        /// Gets or sets light brightness parameter
        /// </summary>
        public float Brightness
        {
			get; set;
        }

        /// <summary>
        /// Gets or sets light radius parameter
        /// </summary>
        public float Radius
        {
			get; set;
        }

        /// <summary>
        /// Gets or sets light source bulb radius parameter
        /// </summary>
        public float SourceRadius
        {
			get; set;
        }

        /// <summary>
        /// Gets light scaled radius parameter
        /// </summary>
        public float ScaledRadius
        {
			get; set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float MinimumRoughness
        {
			get; set;
        }

        /// <summary>
        /// Gets or sets value indicating if how visual element casts shadows
        /// </summary>
        public ShadowsCastingMode ShadowsMode
        {
			get; set;
        }
    }
}