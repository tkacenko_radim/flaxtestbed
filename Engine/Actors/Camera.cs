﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public class Camera : Actor
    {
        public Matrix View
        {
            get => Matrix.Identity;
        }

        public Matrix Projection
        {
            get => Matrix.Identity;
        }

        public bool UsePerspective
        {
            get; set;
        }

        public float FieldOfView
        {
            get; set;
        }

        public float NearPlane
        {
            get; set;
        }

        public float FarPlane
        {
            get; set;
        }

        public Viewport Viewport
        {
            get;
        }

        public static Camera MainCamera
        {
            get;
        } = new Camera();

        public float CustomAspectRatio
        {
            get; set;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{Name} ({GetType().Name})";
        }

        private Camera() : base()
        {

        }

        public static bool IntersectsItselfEditor(ref Ray ray, ref float distance)
        {
            //TODO: Implement + move to actor class
            return false;
        }
    }
}
