﻿// Flax Engine scripting API

using System;

namespace FlaxEngine
{

    public sealed class ModelActor : Actor
    {
        private MeshInfo[] _meshes;

        /// <summary>
        /// Gets the model mesh infos collection. Each <see cref="MeshInfo"/> contains data how to render each mesh (transformation, material, shadows casting, etc.).
        /// </summary>
        /// <value>
        /// The mesh infos array. It's null if the <see cref="Model"/> property is null or asset is not loaded yet.
        /// </value>
        public MeshInfo[] Meshes
        {
            get
            {
                // Check if has cached data
                if (_meshes != null)
                    return _meshes;

                // Cache data
                var model = Model;
                if (model && model.IsLoaded)
                {
                    var meshesCount = model.LODs[0].Meshes.Length;
                    _meshes = new MeshInfo[meshesCount];
                    for (int i = 0; i < meshesCount; i++)
                    {
                        _meshes[i] = new MeshInfo(this, i);
                    }
                }

                return _meshes;
            }
        }
        

        private ModelActor() : base()
        {

        }

        /// <summary>
        /// Gets or sets model scale in lightmap parameter
        /// </summary>
        public float ScaleInLightmap
        {
            get; set;
        }

        private Model _model;

        /// <summary>
        /// Gets or sets model asset
        /// </summary>
        public Model Model
        {
            get => _model; set {
                if (_model != value)
                {
                    if(_model)
                        _model.RefCount--;

                    if(value)
                        value.RefCount++;

                    _model = value;

                    // Clear cached data
                    _meshes = null;
                }
            }
        }

        /// <summary>
        /// Temporary method, until refatoring is done
        /// </summary>
        /// <returns>Instance</returns>
        public static ModelActor New()
        {
            return new ModelActor();
        }
    }
}