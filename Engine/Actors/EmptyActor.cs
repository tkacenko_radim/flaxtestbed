﻿// Flax Engine scripting API

namespace FlaxEngine
{
    /// <summary>
    /// Actor without any custom properties. It's useful when designing scene hierarchy or to hold scripts.
    /// </summary>
    public sealed class EmptyActor : Actor
    {
        /// <summary>
 		/// Creates new <see cref="EmptyActor"/> object.
 		/// </summary>
        private EmptyActor() : base()
        {

        }
    }
}