﻿// Flax Engine scripting API

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class Sky : Actor
    {
        private Sky() : base()
        {

        }
        /// <summary>
        /// Gets or sets value indicating if visual element affects the world
        /// </summary>
        public bool AffectsWorld
        {
			get; set;
        }

        /// <summary>
        /// Gets or sets linked directional light actor that is used to simulate sun
        /// </summary>
        public DirectionalLight SunLight
        {
			get; set;
        }
    }
}