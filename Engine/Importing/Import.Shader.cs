﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public partial class Import
    {
        public static byte[] Shader(string path, out int TypeID, out string Extension)
        {
            TypeID = FlaxEngine.Shader.TypeID;
            Extension = FlaxEngine.Shader.Extension;

            

            return File.ReadAllBytes(path); //Return raw file, no custom format yet :) (raw better for development
        }
    }
}
