﻿using Assimp;
using FlaxEngine.Utilities.Optimizations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public partial class Import : IDisposable
    {
        //NOTE: Probably move to Model class


        private static AssimpContext Assimp = new AssimpContext();

        public static byte[] Model(string path, out int TypeID, out string Extension)
        {
            TypeID = FlaxEngine.Model.TypeID;
            Extension = FlaxEngine.Model.Extension;
            Assimp.Scene scene = null;
            try
            {
                PostProcessSteps preset = FlaxEngine.Model.Import_OptimizeMeshes ? PostProcessPreset.TargetRealTimeMaximumQuality | PostProcessSteps.ImproveCacheLocality | PostProcessSteps.Triangulate | PostProcessSteps.OptimizeMeshes : PostProcessPreset.TargetRealTimeFast;
                preset |= PostProcessSteps.MakeLeftHanded;
                Assimp.Scale = FlaxEngine.Model.Import_ModelScale;
                scene = Assimp.ImportFile(path, preset);

            }
            catch (Exception ex)
            {
                Debug.LogException(new Exception(string.Format("Cannot import model '{0}' {1}", path, ex.Message), ex));
            }

            using(MemoryStream result = new MemoryStream())
            {
                result.Write(FlaxEngine.Model.Header, 0, 3);
                result.Write(1); //v1

                List<Assimp.Mesh> meshes = new List<Assimp.Mesh>(scene.MeshCount);

                for (int i = 0; i < scene.MeshCount; i++)
                {
                    //If mesh is valid (has vertices, has faces and is formed from triangles)
                    if(scene.Meshes[i].HasVertices && scene.Meshes[i].HasFaces && scene.Meshes[i].Faces[0].IndexCount == 3)
                    {
                        meshes.Add(scene.Meshes[i]);
                    }
                }
                if(meshes.Count == 0)
                {
                    Debug.LogException(new Exception("Model " + path + " has no valid meshes."));
                }
                result.Write(1); //We have only one LOD

                //for every LOD:
                //Agregate space for some other metadata (like minDistance and maxDistance for specific LOD or something like this)
                result.Write(meshes.Count); //Mesh count in LOD 0

                NodeCollection children = scene.RootNode.Children;

                for (int i = 0; i < meshes.Count; i++)
                {
                    Assimp.Mesh mesh = meshes[i];
                    string name = mesh.Name;
                    for (int j = 0; j < children.Count; j++)
                    {
                        if (children[j].MeshIndices != null && children[j].MeshIndices.Count > 0 && children[j].MeshIndices[0] == j)
                        {
                            name = children[j].Name;
                            break;
                        }
                    }
                    result.WriteUTF8(name, FlaxEngine.Model.TypeID);
                    result.Write(Transform.Identity);
                    result.Write(false); //Should we force two sided rendering
                    result.Write(mesh.VertexCount);
                    result.Write(mesh.Faces.Count);
                    // TODO: Decide on vertex count limit

                    // Check if we can use ushort for storing indices
                    bool useUshort = mesh.Faces.Count * 3 <= ushort.MaxValue;
                    result.Write(useUshort);
                    result.Write(Guid.Empty); //Material ID, we don't have any at the time of importing, assigned in editor

                    bool hasUVs = mesh.HasTextureCoords(0);
                    List<Vector3D> UVs = null;
                    if (hasUVs)
                    {
                        UVs = mesh.TextureCoordinateChannels[0];
                    }

                    //NOTE: Probably try load custom light map UVs here (channel 1)

                    List<Vector3D> vertices = mesh.Vertices;
                    List<Vector3D> normals = mesh.Normals;
                    List<Vector3D> tangents = mesh.Tangents;

                    bool hasNormals = mesh.HasNormals;
                    bool hasTangentBasis = mesh.HasTangentBasis;

                    Vector3 minimum = Vector3.Maximum;
                    Vector3 maximum = Vector3.Minimum;

                    //Write data in sequence of VBO 0 and VBO 1, it's easier to load data to buffers
                    //VBO 0 is just a position
                    //VBO 1 is the rest
                    using (MemoryStream vertexBuffer1Data = new MemoryStream(vertices.Count * 3 * 4))
                    {
                        for (int j = 0; j < mesh.VertexCount; j++)
                        {
                            Vector3 position = vertices[j].GetVector3();
                            //Calculate min and max for bounding box
                            Vector3.Min(ref minimum, ref position, out minimum);
                            Vector3.Max(ref maximum, ref position, out maximum);

                            Half2 uv = hasUVs ? UVs[j].GetUVs() : Half2.Zero;
                            Vector3 normal;
                            if (hasNormals)
                            {
                                normal = normals[j].GetVector3();
                                normal.Normalize();
                            }
                            else
                            {
                                normal = Vector3.UnitZ;
                            }

                            Vector3 tangent;
                            if (hasTangentBasis)
                            {
                                tangent = tangents[j].GetVector3();
                                tangent.Normalize();
                            }
                            else
                            {
                                tangent = Vector3.UnitX;
                            }

                            result.Write(position);
                            vertexBuffer1Data.Write(uv);

                            Vector3 bitangent = Vector3.Normalize(Vector3.Cross(tangent, normal));
                            byte alpha = (Vector3.Dot(tangent, Vector3.Cross(bitangent, normal)) > 0f) ? (byte)0 : (byte)1;

                            Vector2 tbn = new Vector2(new FloatR10G10B10A2(normal * 0.5f + 0.5f).RawValue, new FloatR10G10B10A2(tangent * 0.5f + 0.5f, alpha).RawValue);

                            vertexBuffer1Data.Write(tbn);
                        }
                        //Check if vbo 1 data is valid size (vertices * 3 (num of components) * sizeof(component) (component is uint or float, which both are 4 bytes large)
                        if (vertexBuffer1Data.Length != vertices.Count * 3 * 4)
                        {
                            throw new Exception("Invalid vertex buffer 1 size");
                        }
                        //write VBO 1
                        result.Write(vertexBuffer1Data.ToArray(), 0, vertices.Count * 3 * 4);

                    }

                    //Write bounding box
                    result.Write(new BoundingBox(minimum, maximum));

                    int[] indices = new int[mesh.Faces.Count * 3];
                    for (int j = 0, k = 0; j < mesh.Faces.Count; j++)
                    {
                        Face face = mesh.Faces[j];
                        indices[k++] = face.Indices[0];
                        indices[k++] = face.Indices[1];
                        indices[k++] = face.Indices[2];
                    }

                    //Optimize indices
                    int[] optimized = TriangleOrderOptimizer.Tipsify(indices, mesh.VertexCount, 30);

                    if (optimized.Length != indices.Length)
                    {
                        throw new Exception("Cannot optimize index buffer.");
                    }

                    if (useUshort)
                    {
                        for (int j = 0; j < optimized.Length; j++)
                        {
                            result.Write((ushort)optimized[j]);
                        }
                    }
                    else
                    {
                        for (int j = 0; j < optimized.Length; j++)
                        {
                            result.Write((uint)optimized[j]);
                        }
                    }
                }

                result.Write('~');
                scene.Clear();
                return result.ToArray();
            }
        }

        public void Dispose()
        {
            Disposer.Dispose(ref Assimp);
        }
    }
}
