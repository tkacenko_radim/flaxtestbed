﻿using SharpCompress.Compressors;
using SharpCompress.Compressors.Deflate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
	public partial class Import
    {
		public static byte[] SpriteAtlas(string path, out int TypeID, out string Extension)
        {
            TypeID = FlaxEngine.SpriteAtlas.TypeID;
            Extension = FlaxEngine.SpriteAtlas.Extension;
            if (!File.Exists(path))
            {
                Debug.LogException(new Exception("Sprite atlas not found"));
            }

            var lines = File.ReadAllLines(path);
            Dictionary<string, Rectangle> sprites = new Dictionary<string, Rectangle>(lines.Length);

            string ImagePath = lines[0].Split(';')[1];

            for (int i = 1; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Substring(0, 2) != "SP")
                    continue;
                string[] split = line.Split(';');
                sprites.Add(split[1], new Rectangle(int.Parse(split[2]), int.Parse(split[3]), int.Parse(split[4]), int.Parse(split[5])));
            }

            byte[] rawBitmap = null;
            try
            {
                rawBitmap = File.ReadAllBytes(Path.Combine(Path.GetDirectoryName(path), ImagePath));
            }
            catch (Exception ex)
            {
                Debug.LogException(new Exception("Failed to load sprite atlas bitmap", ex));
            }

            using (MemoryStream dataStream = new MemoryStream())
            {
                dataStream.Write(sprites.Count);
                foreach (var kvp in sprites)
                {
                    dataStream.WriteUTF8(kvp.Key, (byte)TypeID); //Offset the string by typeID because it's easiliy readable

                    dataStream.Write(kvp.Value.X);
                    dataStream.Write(kvp.Value.Y);
                    dataStream.Write(kvp.Value.Width);
                    dataStream.Write(kvp.Value.Height);
                }

                using (MemoryStream bitmapStream = new MemoryStream(rawBitmap.Length))
                {
                    using (GZipStream gzipped = new GZipStream(bitmapStream, CompressionMode.Compress))
                    {
                        gzipped.Write(rawBitmap, 0, rawBitmap.Length);
                    }
                    byte[] compressedBitmap = bitmapStream.ToArray();

                    dataStream.Write(compressedBitmap.Length);

                    dataStream.Write('~');
                    dataStream.Write(compressedBitmap, 0, compressedBitmap.Length);
                    dataStream.Write('~');
                }

                return dataStream.ToArray();
            }
        }
    }
}
