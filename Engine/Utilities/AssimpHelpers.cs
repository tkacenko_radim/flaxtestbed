﻿using Assimp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    internal static class AssimpHelpers
    {
        internal unsafe static Vector2 GetVector2(this Vector2D vector)
        {
            return *(Vector2*)(&vector);
        }

        internal unsafe static Vector3 GetVector3(this Vector3D vector)
        {
            return *(Vector3*)(&vector);
        }

        internal static Vector4 GetVector4(this Color4D color)
        {
            return new Vector4(color.R, color.G, color.B, color.A);
        }

        internal unsafe static Matrix GetMatrix(this Matrix4x4 matrix)
        {
            return *(Matrix*)(&matrix);
        }

        internal static Half2 GetUVs(this Vector3D vector)
        {
            float x = vector.X;
            float y = vector.Y;
            if (x < -2f || x > 2f)
            {
                x %= 2f;
            }
            if (y < -2f || y > 2f)
            {
                y %= 2f;
            }
            return new Half2(x, y);
        }
    }
}