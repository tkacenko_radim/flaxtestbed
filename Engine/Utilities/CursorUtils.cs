﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlaxEngine
{
    public static class CursorUtils
    {
        private static bool hidden = false;
        public static Cursor ToWinForms(this CursorType type)
        {
            if (hidden && type != CursorType.Hidden)
            {
                Cursor.Show();
                hidden = false;
            }
            switch (type)
            {
                case CursorType.Default:
                    return Cursors.Default;
                case CursorType.Cross:
                    return Cursors.Cross;
                case CursorType.Hand:
                    return Cursors.Hand;
                case CursorType.Help:
                    return Cursors.Help;
                case CursorType.IBeam:
                    return Cursors.IBeam;
                case CursorType.No:
                    return Cursors.No;
                case CursorType.Wait:
                    return Cursors.WaitCursor;
                case CursorType.SizeAll:
                    return Cursors.SizeAll;
                case CursorType.SizeNESW:
                    return Cursors.SizeNESW;
                case CursorType.SizeNS:
                    return Cursors.SizeNS;
                case CursorType.SizeNWSE:
                    return Cursors.SizeNWSE;
                case CursorType.SizeWE:
                    return Cursors.SizeWE;
                case CursorType.Hidden:
                    Cursor.Hide(); //Hide and return default
                    hidden = true;
                    return Cursors.Default;
            }
            return Cursors.Default;
        }
    }
}
