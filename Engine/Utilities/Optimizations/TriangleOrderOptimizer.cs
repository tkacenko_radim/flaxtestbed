﻿using System;

namespace FlaxEngine.Utilities.Optimizations
{

    public sealed class TriangleOrderOptimizer
    {
        private const int DeadEndStackSize = 128;

        private const int DeadEndStackMask = 127;

        private static int skipDeadEnd(int[] liveTriangleCount, int[] deadEndStack, ref int deadEndStackPos, ref int deadEndStackStart, int vertexCount, ref int i)
        {
            while ((deadEndStackPos & DeadEndStackMask) != deadEndStackStart)
            {
                //Find vertex with live triangles in dead-end stack
                int num = deadEndStackPos - 1;
                deadEndStackPos = num;
                int d = deadEndStack[num & DeadEndStackMask];
                if (liveTriangleCount[d] > 0)
                {
                    return d;
                }
            }

            //If not found, find next arbitrary vertex with live triangles
            while (i + 1 < vertexCount)
            {
                i++;
                if (liveTriangleCount[i] > 0)
                {
                    return i;
                }
            }
            return -1;
        }

        private static int getNextVertex(int nVertices, ref int i, int cacheSize, int[] candidates, int candidatesCount, int[] timestamp, int time, int[] liveTriangles, int[] deadEndStack, ref int deadEndStackPos, ref int deadEndStackStart)
        {
            // Go through candidates in 1-ring around fanning vertex
            int fanningVertex = -1;
            int candidatePriority = -1;
            for (int j = 0; j < candidatesCount; j++)
            {
                int v = candidates[j];
                // Skip if doesn't have any live triangles
                if (liveTriangles[v] > 0)
                {
                    // Get most fresh candidate
                    int priority = 0;
                    if (time - timestamp[v] + 2 * liveTriangles[v] <= cacheSize)
                    {
                        priority = time - timestamp[v];
                    }
                    if (priority > candidatePriority)
                    {
                        fanningVertex = v;
                        candidatePriority = priority;
                    }
                }
            }
            // On dead-end
            if (fanningVertex == -1)
            {
                fanningVertex = skipDeadEnd(liveTriangles, deadEndStack, ref deadEndStackPos, ref deadEndStackStart, nVertices, ref i);
            }
            return fanningVertex;
        }

        public unsafe static int[] Tipsify(int[] indices, int nVertices, int cacheSize)
        {

            int numIndices = indices.Length;

            // How many times is each vertex referenced
            int[] liveTriangleCount = new int[nVertices];

            for (int i = 0; i < numIndices; i++)
            {
                liveTriangleCount[indices[i]]++;
            }

            // Offset array from counts.
            int sum = 0;
            int[] neighborPosition = new int[nVertices + 1];
            int maxLiveTriangleCount = 0;

            for (int i = 0; i < nVertices; i++)
            {
                neighborPosition[i] = sum;
                sum += liveTriangleCount[i];
                if (liveTriangleCount[i] > maxLiveTriangleCount)
                {
                    maxLiveTriangleCount = liveTriangleCount[i];
                }
                liveTriangleCount[i] = 0;
            }

            // Array of neighbors
            neighborPosition[nVertices] = sum;
            int[] neighbors = new int[numIndices];

            for (int i = 0; i < numIndices / 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int current = indices[3 * i + j];
                    neighbors[neighborPosition[current] + liveTriangleCount[current]] = i;
                    liveTriangleCount[current]++;
                }
            }
            // Global time, per-vertex caching timestamps
            int[] timestap = new int[nVertices];
            Array.Clear(timestap, 0, nVertices);
            int time = cacheSize + 1;

            // Dead-end vertex stack, position and start
            int[] deadEndStack = new int[DeadEndStackSize];
            Array.Clear(deadEndStack, 0, DeadEndStackSize);

            int deadEndStackPos = 0;
            int deadEndStackStart = 0;

            // Per triangle emmited flag
            bool[] emmited = new bool[numIndices / 3];
            Array.Clear(emmited, 0, emmited.Length);

            // Output triangles buffer
            int[] outputTriangles = new int[numIndices / 3];
            int triangleIndex = 0;

            // Array with candidates for fanning
            int[] candidates = new int[3 * maxLiveTriangleCount];

            // Starting vertex for fanning, cursor
            int cursor = 0;
            int fanningVertex = 0;

            while (fanningVertex >= 0)
            {
                int candidatesCount = 0;
                // For all neighbors of fanning vertex
                for (int ti = neighborPosition[fanningVertex]; ti < neighborPosition[fanningVertex + 1]; ti++)
                {
                    int t = neighbors[ti];
                    // Skip already emmited
                    if (!emmited[t])
                    {
                        // Write triangle to output buffer
                        outputTriangles[triangleIndex++] = t;


                        for (int vi = 0; vi < 3; vi++)
                        {
                            int index = indices[vi + t * 3];

                            // Add to dead end stack and candidates array
                            deadEndStack[deadEndStackPos++ & DeadEndStackMask] = index;
                            if ((deadEndStackPos & DeadEndStackMask) == (deadEndStackStart & DeadEndStackMask))
                            {
                                deadEndStackStart = (deadEndStackStart + 1 & DeadEndStackMask);
                            }
                            candidates[candidatesCount++] = index;

                            // Decrease live triangle count
                            liveTriangleCount[index]--;

                            // If not in cache, set timestamp
                            if (time - timestap[index] > cacheSize)
                            {
                                timestap[index] = time;
                                time++;
                            }
                        }
                        // Set as emmited
                        emmited[t] = true;
                    }
                }
                // Get next fanning vertex
                fanningVertex = getNextVertex(nVertices, ref cursor, cacheSize, candidates, candidatesCount, timestap, time, liveTriangleCount, deadEndStack, ref deadEndStackPos, ref deadEndStackStart);
            }

            // Create output indices
            int[] outputIndices = new int[numIndices];
            triangleIndex = 0;
            for (int i = 0; i < numIndices / 3; i++)
            {
                for (int n = 0; n < 3; n++)
                {
                    outputIndices[triangleIndex++] = indices[3 * outputTriangles[i] + n];
                }
            }
            return outputIndices;
        }
    }
}