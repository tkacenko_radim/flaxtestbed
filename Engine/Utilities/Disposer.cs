﻿using System;

namespace FlaxEngine
{
    public static class Disposer
    {
        public static void Dispose<T>(ref T obj) where T : class
        {
            if (obj != null)
            {
                if (obj is IDisposable disposable)
                {
                    try
                    {
                        disposable.Dispose();
                    }
                    catch
                    {
                    }
                }
                obj = default(T);
            }
        }
    }
}
