﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FlaxEngine
{
    /// <summary>
    /// Class responsible for working with assets
    /// </summary>
    public partial class ContentPool
    {
        /// <summary>
        /// Interval in which Content pool is updated
        /// </summary>
        private static readonly float ContentUpdateInterval = 1; //1 second

        /// <summary>
        /// Timeout after which asset is unloaded
        /// </summary>
        private static readonly float AssetUnloadTimeout = 5; //5 seconds

        private float _lastUpdateTime = 0;

        public static ContentPool Instance;
        private List<Type> AssetTypes = new List<Type> { };
        private Dictionary<string, Asset> AssetCache = new Dictionary<string, Asset> { };
        private Dictionary<Guid, string> AssetRegistry = new Dictionary<Guid, string> { };
        private Dictionary<Asset, float> UnloadQueue = new Dictionary<Asset, float> { };
        private List<Asset> UnloadCache = new List<Asset> { };

        /// <summary>
        /// Initialize
        /// </summary>
        static ContentPool()
        {
            Instance = new ContentPool();
        }

        public ContentPool()
        {
            //Setup loading
            SetupWorkers();

            //Register assets
            AssetTypes.Add(typeof(SpriteAtlas));
            AssetTypes.Add(typeof(FontAsset));
            AssetTypes.Add(typeof(Model));
            AssetTypes.Add(typeof(Shader));
            //Build asset cache

        }

        /// <summary>
        /// Get asset by it's name
        /// </summary>
        /// <param name="name">Asset name</param>
        /// <returns>Returns asset if avalible or null</returns>
        public Asset GetAsset(string name)
        {
            //Standardize
            name = name.Replace('/', '\\');
            lock (AssetCache)
            {
                //If name is valid and loaded
                if (!string.IsNullOrEmpty(name) && AssetCache.ContainsKey(name))
                {
                    //Return it
                    return AssetCache[name];
                }
            }
            //Return null
            return null;
        }

        /// <summary>
        /// Get asset by it's id
        /// </summary>
        /// <param name="id">Asset id</param>
        /// <returns>Returns asset if avalible or null</returns>
        public Asset GetAsset(Guid id)
        {
            lock (AssetCache)
            {
                //Itterate through
                foreach (KeyValuePair<string, Asset> current in AssetCache)
                {
                    if (current.Value.ID == id)
                    {
                        //Return asset
                        return current.Value;
                    }
                }
            }
            //Return null
            return null;
        }

        public void Update()
        {
            if(Time.RealtimeSinceStartup - _lastUpdateTime >= ContentUpdateInterval)
            {
                lock (AssetCache)
                {
                    foreach (Asset current in AssetCache.Values)
                    {
                        if(current.RefCount == 0 && !UnloadQueue.ContainsKey(current))
                        {
                            UnloadQueue.Add(current, Time.RealtimeSinceStartup);
                        }
                    }

                    UnloadCache.Clear();

                    foreach (var current in UnloadQueue)
                    {
                        if(current.Key.RefCount > 0 || Time.RealtimeSinceStartup - current.Value >= AssetUnloadTimeout)
                        {
                            UnloadCache.Add(current.Key);
                        }
                    }

                    for (int i = 0; i < UnloadCache.Count; i++)
                    {
                        Asset current = UnloadCache[i];

                        if(current.RefCount == 0)
                        {
                            Debug.LogFormat("Asset '{0}':{1}({2}) isn't referenced by any object. Disposing it...", current.Name, current.ID,current.GetType().Name);
                            current.Dispose();
                        }
                        UnloadQueue.Remove(current);
                    }
                    UnloadCache.Clear();
                }
                _lastUpdateTime = Time.RealtimeSinceStartup;
            }
        }

        internal string GetNameFromID(Guid id)
        {
            lock (AssetRegistry)
            {
                //Try get name from registry
                if(AssetRegistry.TryGetValue(id,out string cachedName))
                {
                    //NOTE: Maybe check if file exists
                    return cachedName;
                }

                //If not found rebuild/build the cache
                List<string> files = new List<string> { };
                files.AddRange(Directory.GetFiles(Globals.ContentFolder, "*.meta", SearchOption.AllDirectories));
                files.AddRange(Directory.GetFiles(Globals.SourceFolder, "*.meta", SearchOption.AllDirectories));
                files.AddRange(Directory.GetFiles(Globals.EngineFolder, "*.meta", SearchOption.AllDirectories));
                files.AddRange(Directory.GetFiles(Globals.EditorFolder, "*.meta", SearchOption.AllDirectories));

                Debug.LogFormat("Start searching asset with ID: {0}. {1} potential files to check...", id, files.Count);
                

                for (int i = 0; i < files.Count; i++)
                {
                    string current = files[i];
                    string filename = Path.GetFileNameWithoutExtension(current);

                    //Store without extension cause it can differ
                    if (!AssetRegistry.ContainsValue(filename))
                    {
                        var metadata = MetaFile.LoadMetadata(current);
                        //Save to registry or update
                        if (!AssetRegistry.ContainsKey(metadata.ID))
                        {
                            AssetRegistry.Add(metadata.ID, filename);
                            if(metadata.ID == id)
                            {
                                Debug.LogFormat("Found {0} at '{1}'!", id, filename);
                                return filename;
                            }
                        }
                        else
                        {
                            AssetRegistry[metadata.ID] = filename;
                        }
                    }
                }
            }
            Debug.LogFormat("Cannot find {0}.", id);
            return null;
        }

        internal void assetDisposed(Asset asset)
        {
            UnloadQueue.Remove(asset);

            lock (AssetCache)
            {
                AssetCache.Remove(asset.FullName);
            }
        }


        public MaterialInstance CreateVirtualMaterialInstance()
        {
            //Asset name
            Guid assetGuid = Guid.NewGuid();

            string assetName = Path.Combine(Globals.TemporaryFolder, assetGuid.ToString("N") + MaterialInstance.Extension);

            MaterialInstance instance = new MaterialInstance(this, assetName);
            instance.id = assetGuid;
            instance.RefCount++;

            lock (AssetCache)
            {
                AssetCache.Add(assetName, instance);
            }
            return instance;
        }
    }
}
