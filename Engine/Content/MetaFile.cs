﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public sealed class MetaFile
    {
        private static byte[] FileHeader = new byte[] { (byte)'F', (byte)'M', (byte)'F' }; //Flax meta file

        public class MetadataCollection : Dictionary<string, CommonValue>
        {
            //Set of metadata which is same for every asset entry and type
            public Guid ID => (Guid)this["ID"].Value;
            public int TypeID => (int)this["TypeID"].Value;
            public string Extension => (string)this["Extension"].Value;

            public MetadataCollection()
            {
            }

            public MetadataCollection(Guid id, int typeID, string extension)
            {
                Add("ID", id);
                Add("TypeID", typeID);
                Add("Extension", extension);
            }
        }

        public static MetadataCollection LoadMetadata(string path)
        {
            if (!File.Exists(path))
            {
                throw new Exception("File not found");
            }

            using(FileStream stream = new FileStream(path, FileMode.Open))
            {
                byte[] header = new byte[3];
                stream.Read(header, 0, 3);
                if(header[0] != FileHeader[0] || header[1] != FileHeader[1] || header[2] != FileHeader[2])
                {
                    throw new Exception("Invalid meta file");
                }

                MetadataCollection output = new MetadataCollection();
                int count = stream.ReadInt();

                for (int i = 0; i < count; i++)
                {
                    output.Add(stream.ReadStringUTF8(11), stream.ReadCommonValue());
                }

                if (stream.ReadByte() != 126)
                {
                    throw new Exception("Invalid meta file");
                }
                return output;
            }
        }

        public static void SaveMetadata(string path, MetadataCollection metadata)
        {
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                stream.Write(FileHeader, 0, 3);
                stream.Write(metadata.Count);
                foreach (var item in metadata)
                {
                    stream.WriteUTF8(item.Key, 11); //11 is random number :)
                    stream.Write(item.Value);
                }
                stream.WriteByte(126);
            }
        }
    }
}
