﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public partial class ContentPool
    {
        /// <summary>
        /// Locker for loader threads
        /// </summary>
        private object _loadLocker;
        /// <summary>
        /// Array of loader threads
        /// </summary>
        private Thread[] _loadThreads;
        /// <summary>
        /// Queue containing assets to be loaded
        /// </summary>
        private Queue<Asset> _loadQueue;
        /// <summary>
        /// Exit flag used to tell loader threads that they should close
        /// </summary>
        private volatile bool _loadExitFlag;


        /// <summary>
        /// Method used to setup and start all threads used for asset loading
        /// </summary>
        private void SetupWorkers()
        {
            _loadExitFlag = false;
            _loadLocker = new object();
            _loadQueue = new Queue<Asset>(64); //Prealocate memory for 64 assets (should be enough even for bigger scene)

            //TODO: Probably check for exceptions and return bool if success or not

            //Determine count of workers (1-3 should be enough depending on core count)
            //We allways want atleast 1 worker, but we want one less than is our core count
            int workerCount = Mathf.Clamp(Environment.ProcessorCount - 1, 1, 3);

            _loadThreads = new Thread[workerCount];
            for (int i = 0; i < workerCount; i++)
            {
                Thread thread = new Thread(loadWork)
                {
                    IsBackground = true,
                    Name = "Asset loader " + i, //TODO: probably better fitting name :)
                    Priority = ThreadPriority.Normal
                };
                _loadThreads[i] = thread;
            }

            //Start the threads
            for (int i = 0; i < workerCount; i++)
            {
                _loadThreads[i].Start();
            }
        }

        /// <summary>
        /// Method which is base of loader thread (it's loader's work)
        /// </summary>
        private void loadWork()
        {
            try
            {
                while (true)
                {
                    Asset asset = null;

                    lock (_loadQueue)
                    {
                        if(_loadQueue.Count > 0)
                        {
                            asset = _loadQueue.Dequeue();
                        }
                    }

                    if(asset != null)
                    {
                        try
                        {
                            //Load it using synchronous Load method
                            asset.Load();

                            //Sleep after import so we don't stress core/thread that much so it can process other engine stuff
                            Thread.Sleep(10);
                        }
                        catch (Exception loadException)
                        {
                            Debug.LogException(loadException);
                            //Some sleep so we have time to process exception before processing another asset (10ms should be enough)
                            Thread.Sleep(10);
                        }
                    }
                    else
                    {
                        //No asset to laod so we probably should do some sleep so we don't stress CPU that much, 1ms should be enough (not sure tho)
                        Thread.Sleep(1);
                    }

                    lock (_loadLocker)
                    {
                        Thread.MemoryBarrier();
                        if (_loadExitFlag)
                        {
                            //If exit is requested, than quit the thread loop and cause the thread is as background it terminates itself
                            break;
                        }
                    }
                }
            }
            catch (Exception workerException)
            {
                Debug.LogException(workerException);
            }
        }
    }
}
