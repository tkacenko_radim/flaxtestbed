﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public static class Content
    {
        private delegate byte[] Importer(string path, out int typeID, out string Extension);

        private static ContentPool contentPool => ContentPool.Instance;
        private static Dictionary<string, Importer> Importers = new Dictionary<string, Importer> { };


        static Content()
        {
            //HLSL and SAI used only for development, not in packed game/release
            Importers.Add("hlsl", new Importer(FlaxEngine.Import.Shader));
            Importers.Add("api", new Importer(FlaxEngine.Import.Shader));
            Importers.Add("sai", new Importer(FlaxEngine.Import.SpriteAtlas));
            //TODO: Add any missing Assimp importers i forgot
            Importers.Add("x", new Importer(FlaxEngine.Import.Model));
            Importers.Add("obj", new Importer(FlaxEngine.Import.Model));
            Importers.Add("dae", new Importer(FlaxEngine.Import.Model));
            Importers.Add("fbx", new Importer(FlaxEngine.Import.Model));
            Importers.Add("3ds", new Importer(FlaxEngine.Import.Model));
            Importers.Add("ase", new Importer(FlaxEngine.Import.Model));
            Importers.Add("ifc", new Importer(FlaxEngine.Import.Model));
            Importers.Add("xgl", new Importer(FlaxEngine.Import.Model));
            Importers.Add("zgl", new Importer(FlaxEngine.Import.Model));
            Importers.Add("ply", new Importer(FlaxEngine.Import.Model));
            Importers.Add("dxf", new Importer(FlaxEngine.Import.Model));
            Importers.Add("lws", new Importer(FlaxEngine.Import.Model));
            Importers.Add("lwo", new Importer(FlaxEngine.Import.Model));
            Importers.Add("lxo", new Importer(FlaxEngine.Import.Model));
            Importers.Add("stl", new Importer(FlaxEngine.Import.Model));
            Importers.Add("ac", new Importer(FlaxEngine.Import.Model));
            Importers.Add("ms3d", new Importer(FlaxEngine.Import.Model));
            Importers.Add("cob", new Importer(FlaxEngine.Import.Model));
            Importers.Add("scn", new Importer(FlaxEngine.Import.Model));
            Importers.Add("irrmesh", new Importer(FlaxEngine.Import.Model));
            Importers.Add("irr", new Importer(FlaxEngine.Import.Model));
            Importers.Add("mdl", new Importer(FlaxEngine.Import.Model));
            Importers.Add("md2", new Importer(FlaxEngine.Import.Model));
            Importers.Add("md3", new Importer(FlaxEngine.Import.Model));
            Importers.Add("pk3", new Importer(FlaxEngine.Import.Model));
            Importers.Add("mdc", new Importer(FlaxEngine.Import.Model));
            Importers.Add("smd", new Importer(FlaxEngine.Import.Model));
            Importers.Add("vta", new Importer(FlaxEngine.Import.Model));
            Importers.Add("m3", new Importer(FlaxEngine.Import.Model));
            Importers.Add("3d", new Importer(FlaxEngine.Import.Model));
            Importers.Add("q3d", new Importer(FlaxEngine.Import.Model));
            Importers.Add("q3s", new Importer(FlaxEngine.Import.Model));
            Importers.Add("b3d", new Importer(FlaxEngine.Import.Model));
            Importers.Add("ndo", new Importer(FlaxEngine.Import.Model));
        }

        /// <summary>
        /// Loads asset to the Content Pool and holds it until it won't be referenced by any object. Returns null if asset was not loaded.
        /// </summary>
        /// <param name="id">Asset unique ID.</param>
        /// <returns>Asset instance if loaded, null otherwise</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Asset LoadAsync(Guid id)
        {
            return LoadAsync<Asset>(id);
        }

        /// <summary>
        /// Loads asset to the Content Pool and holds it until it won't be referenced by any object. Returns null if asset was not created (see log for error info).
        /// </summary>
        /// <param name="id">Asset unique ID.</param>
        /// <typeparam name="T">Type of the asset to load. Includes any asset types derived from the type.</typeparam>
        /// <returns>Asset instance if loaded, null otherwise.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T LoadAsync<T>(Guid id) where T : Asset
        {
            return LoadAsyncInternal<T>(contentPool.GetNameFromID(id));
        }

        /// <summary>
        /// Loads asset to the Content Pool and holds it until it won't be referenced by any object. Returns null if asset was not created (see log for error info).
        /// </summary>
        /// <param name="path">Path to the asset.</param>
        /// <typeparam name="T">Type of the asset to load. Includes any asset types derived from the type.</typeparam>
        /// <returns>Asset instance if loaded, null otherwise.</returns>
        public static T LoadAsync<T>(string path) where T : Asset
        {
            return contentPool.LoadAsync<T>(path);
        }

        /// <summary>
        /// Loads asset to the Content Pool and holds it until it won't be referenced by any object. Returns null if asset was not created (see log for error info).
        /// </summary>
        /// <param name="internalPath">Intenral path to the asset. Relative to the Engine startup folder and without an asset file extension.</param>
        /// <typeparam name="T">Type of the asset to load. Includes any asset types derived from the type.</typeparam>
        /// <returns>Asset instance if loaded, null otherwise.</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T LoadAsyncInternal<T>(string internalPath) where T : Asset
        {
            return LoadAsync<T>(Path.Combine(Globals.StartupPath, internalPath));
        }


        public static T LoadInternal<T>(string filename) where T : Asset
        {
            T asset = LoadAsyncInternal<T>(filename);
            asset.WaitForLoaded();
            return asset;
        }


        /// <summary>
        /// Find asset info by id.
        /// </summary>
        /// <param name="id">The asset path (full path).</param>
        /// <param name="typeId">If method returns true, this contains found asset type id.</param>
        /// <param name="path">If method returns true, this contains found asset path.</param>
        /// <returns>True if found any asset, otherwise false.</returns>
        public static bool GetAssetInfo(Guid id, out int typeId, out string path)
        {
            typeId = 0;
            path = null;
            return false;
        }

        /// <summary>
        /// Find asset info by path.
        /// </summary>
        /// <param name="path">The asset id.</param>
        /// <param name="typeId">If method returns true, this contains found asset type id.</param>
        /// <param name="id">If method returns true, this contains found asset id.</param>
        /// <returns>True if found any asset, otherwise false.</returns>
        public static bool GetAssetInfo(string path, out int typeId, out Guid id)
        {
            //TODO: Cache this
            string metaPath = path.Replace(Path.GetExtension(path), ".meta");
            try
            {
                var collection = MetaFile.LoadMetadata(metaPath);
                typeId = collection.TypeID;
                id = collection.ID;
                return true;
            }
            catch
            {
                typeId = 0;
                id = Guid.Empty;
                return false;
            }
        }


        /// <summary>
        /// Creates temporary and virtual asset of the given type. Virtual assets have limited usage but allow to use custom assets data at runtime.
        /// </summary>
        /// <typeparam name="T">Type of the asset to create. Includes any asset types derived from the type.</typeparam>
        /// <returns>Asset instance if created, null otherwise. See log for error message if need to.</returns>
        public static T CreateVirtualAsset<T>() where T : Asset
        {
            int typeId;
            if (typeof(T) == typeof(MaterialInstance))
                typeId = MaterialInstance.TypeID;
            else
                throw new InvalidOperationException("Asset type " + typeof(T).FullName + " does not support virtual assets.");

            return (T)(Asset)contentPool.CreateVirtualMaterialInstance();
        }

        public static void Import(string what, string where)
        {
            string extension = Path.GetExtension(what).Remove(0, 1).ToLower();

            if (!Importers.TryGetValue(extension, out Importer importer))
            {
                throw new Exception("Unknown file type: " + extension);
            }

            byte[] asset = importer(what, out int typeID, out string Extension);

            //TODO: Check if file already exists

            MetaFile.MetadataCollection mc = new MetaFile.MetadataCollection(Guid.NewGuid(),typeID, Extension);
            mc.Add("ImportPath", what);
            MetaFile.SaveMetadata(Path.Combine(where, Path.GetFileNameWithoutExtension(what) + ".meta"), mc);
            File.WriteAllBytes(Path.Combine(where, Path.GetFileNameWithoutExtension(what) + Extension), asset);
        }
    }
}
