﻿using System;
using System.IO;
using System.Text;

namespace FlaxEngine
{
    /// <summary>
    /// Class used to work with custom binary format
    /// </summary>
    public static class BinaryFile
    {
        /// <summary>
        /// Structure containing metadata for <see cref="FlaxEngine.BinaryFile"/> instance
        /// </summary>
        public struct Header
        {
            /// <summary>
            /// Unique ID of asset
            /// </summary>
            public Guid ID;

            /// <summary>
            /// ID used to identify asset type
            /// </summary>
            public int TypeID;

            /// <summary>
            /// Version of Binary format
            /// </summary>
            public int FileVersion;

            /// <summary>
            /// Path to file used to import this binary file, can be null if no path is availible
            /// </summary>
            public string ImportPath;

            public Header(int typeID)
            {
                ID = Guid.NewGuid();
                TypeID = typeID;
                FileVersion = Version;
                ImportPath = null;
            }

            public Header(Guid id, int typeID, string importPath)
            {
                if (id == Guid.Empty)
                {
                    ID = Guid.NewGuid();
                }
                else
                {
                    ID = id;
                }
                TypeID = typeID;
                FileVersion = Version;
                ImportPath = importPath;
            }
        }

        /// <summary>
        /// Reference type used as file identifier
        /// </summary>
        private static byte[] FlaxHeader = new byte[] { (byte)'F', (byte)'F', 0 }; //Flax File

        /// <summary>
        /// Current version of file format
        /// </summary>
        private static int Version = 1;

        /// <summary>
        /// Loads header from path
        /// </summary>
        /// <param name="path">Path to file to load header from</param>
        /// <param name="password">File password</param>
        /// <returns>Header</returns>
        public static Header LoadHeader(string path, string password = "")
        {
            Header header = default(Header);

            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                byte[] identifier = new byte[3];
                stream.Read(identifier, 0, 3);
                if (identifier[0] != FlaxHeader[0] && identifier[1] != FlaxHeader[1] && identifier[2] != FlaxHeader[2])
                {
                    throw new Exception("Invalid file format.");
                }

                header.FileVersion = stream.ReadInt();
                switch (header.FileVersion)
                {
                    case 1:
                        {
                            int hashLength = stream.ReadInt();
                            if (hashLength > 0)
                            {
                                byte[] hashArray = new byte[hashLength];
                                stream.Read(hashArray, 0, hashLength);

                                string hash = Encoding.UTF8.GetString(hashArray);

                                if (!Password.ValidatePassword(password, hash))
                                {
                                    throw new InvalidPasswordException(path);
                                }
                            }
                            else if (!string.IsNullOrEmpty(password))
                            {
                                throw new InvalidPasswordException(path);
                            }

                            header.ID = stream.ReadGuid();
                            header.TypeID = stream.ReadInt();
                            header.ImportPath = stream.ReadStringUTF8((byte)header.TypeID);
                            if (header.ImportPath.Length == 0) header.ImportPath = null; //Required cause we return string.Empty
                        }
                        break;
                    default:
                        throw new Exception("Unknown version: " + header.FileVersion);
                }
            }

            //If file is older version, then update it
            if (header.FileVersion < Version)
            {
                byte[] assetData = Load(path, password, out header);
                Save(path, password, assetData, header);
            }
            return header;
        }

        /// <summary>
        /// Load asset data from file content in form of byte array
        /// </summary>
        /// <param name="bytes">byte array to load data from</param>
        /// <param name="password">Password</param>
        /// <param name="header">Header</param>
        /// <returns>Asset data</returns>
        public static byte[] Load(byte[] bytes, string password, out Header header)
        {
            using (MemoryStream memoryStream = new MemoryStream(bytes))
            {
                return Load(memoryStream, password, out header, null);
            }
        }

        /// <summary>
        /// Load asset data from file path
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <param name="password">Password</param>
        /// <param name="header">Header</param>
        /// <returns>Asset data</returns>
        public static byte[] Load(string path, string password, out Header header)
        {
            byte[] data;
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                data = Load(stream, password, out header);
            }

            if (header.FileVersion < Version)
            {
                Save(path, password, data, header);
            }
            return data;
        }

        /// <summary>
        /// Load asset data from stream
        /// </summary>
        /// <param name="stream">File data stream</param>
        /// <param name="password">Password</param>
        /// <param name="header">Header</param>
        /// <param name="target">Path to file from which data stream comes</param>
        /// <returns>Asset data</returns>
        public static byte[] Load(Stream stream, string password, out Header header, string target = null)
        {
            header = default(Header);
            byte[] identifier = new byte[3];
            stream.Read(identifier, 0, 3);
            if (identifier[0] != FlaxHeader[0] && identifier[1] != FlaxHeader[1] && identifier[2] != FlaxHeader[2])
            {
                throw new Exception("Invalid file format.");
            }

            header.FileVersion = stream.ReadInt();
            switch (header.FileVersion)
            {
                case 1:
                    {
                        int hashLength = stream.ReadInt();
                        if (hashLength > 0)
                        {
                            byte[] hashArray = new byte[hashLength];
                            stream.Read(hashArray, 0, hashLength);

                            string hash = Encoding.UTF8.GetString(hashArray);

                            if (!Password.ValidatePassword(password, hash))
                            {
                                throw new InvalidPasswordException(target);
                            }
                        }
                        else if (target != null && !string.IsNullOrEmpty(password))
                        {
                            throw new InvalidPasswordException(target);
                        }

                        header.ID = stream.ReadGuid();
                        header.TypeID = stream.ReadInt();
                        header.ImportPath = stream.ReadStringUTF8((byte)header.TypeID);
                        if (header.ImportPath.Length == 0) header.ImportPath = null; //Required cause we return string.Empty

                        int dataLength = stream.ReadInt();
                        byte[] data;

                        if (stream.ReadByte() != 126) // char '~' check for data begin
                        {
                            throw new Exception("Corrupted file.");
                        }

                        if (dataLength > 0)
                        {
                            data = new byte[dataLength];
                            stream.Read(data, 0, dataLength);
                        }
                        else
                        {
                            throw new Exception("Corrupted file.");
                        }

                        if (stream.ReadByte() != 126) // char '~' check for data end
                        {
                            throw new Exception("Corrupted file.");
                        }
                        return data;
                    }
                default:
                    throw new Exception("Unknown version: " + header.FileVersion);
            }
        }

        /// <summary>
        /// Change header for file path
        /// </summary>
        /// <param name="path">File path</param>
        /// <param name="password">Password</param>
        /// <param name="header">New header to replace old with</param>
        public static void ChangeHeader(string path, string password, Header header)
        {
            byte[] asset = Load(path, password, out Header currentHeader);
            if (currentHeader.TypeID != header.TypeID)
            {
                throw new Exception("Header is not mathcing to the asset. Invalid TypeName.");
            }
            if (header.ID == Guid.Empty)
            {
                throw new Exception("Invalid header ID.");
            }
            Save(path, password, asset, header);
        }

        /// <summary>
        /// Change asset data for file path
        /// </summary>
        /// <param name="path">File path</param>
        /// <param name="password">Password</param>
        /// <param name="asset">New asset data to replace old with</param>
        public static void ChangeAsset(string path, string password, byte[] asset)
        {
            Load(path, password, out Header header);
            Save(path, password, asset, header);
        }

        /// <summary>
        /// Save asset data to byte array
        /// </summary>
        /// <param name="password">Password</param>
        /// <param name="asset">Asset data</param>
        /// <param name="header">Header</param>
        /// <returns>Asset file in form of byte array</returns>
        public static byte[] Save(string password, byte[] asset, Header header)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Save(stream, password, asset, header);
                return stream.ToArray();
            }
        }

        /// <summary>
        /// Save asset data to file
        /// </summary>
        /// <param name="path">File path</param>
        /// <param name="password">Password</param>
        /// <param name="asset">Asset data</param>
        /// <param name="header">Header</param>
        public static void Save(string path, string password, byte[] asset, Header header)
        {
            using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                Save(stream, password, asset, header);
            }
        }

        /// <summary>
        /// Save asset data to stream
        /// </summary>
        /// <param name="stream">Stream to write data to</param>
        /// <param name="password">Password</param>
        /// <param name="asset">Asset data</param>
        /// <param name="header">Header</param>
        public static void Save(Stream stream, string password, byte[] asset, Header header)
        {
            if (asset == null || asset.Length == 0)
            {
                throw new ArgumentException("No asset.");
            }
            if (header.TypeID <= 0)
            {
                throw new ArgumentException("No type name specified.");
            }

            stream.Write(FlaxHeader, 0, 3);
            stream.Write(Version);

            if (string.IsNullOrEmpty(password))
            {
                stream.Write(0);
            }
            else
            {
                string hash = Password.CreateHash(password);
                byte[] hashArray = Encoding.UTF8.GetBytes(hash);
                stream.Write(hashArray.Length);
                stream.Write(hashArray, 0, hashArray.Length);
            }

            stream.Write(header.ID);
            stream.Write(header.TypeID);
            stream.WriteUTF8(header.ImportPath, (byte)header.TypeID);

            stream.Write(asset.Length);

            stream.Write('~');
            stream.Write(asset, 0, asset.Length);
            stream.Write('~');
        }
    }
}
