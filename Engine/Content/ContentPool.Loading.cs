﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    public partial class ContentPool
    {
        public T LoadAsync<T>(string path) where T : Asset
        {
            if (!string.IsNullOrEmpty(path)) //TODO: Add some path validation
            {
                path = path.Replace('/', '\\'); //Replace slashes

                lock (AssetCache)
                {
                    if (AssetCache.TryGetValue(path, out Asset asset))
                    {
                        asset.RefCount++;
                        return (T)asset;
                    }
                    else
                    {
                        if (AssetTypes.Contains(typeof(T)))
                        {
                            Type assetType = typeof(T);

                            T Asset = (T)assetType.InvokeMember(assetType.FullName, System.Reflection.BindingFlags.CreateInstance, null, null, new object[] { this, path }, null); //Pass reference to ContentPool and file path to asset
                            //Probably do some initialization
                            Asset.RefCount++;
                            AssetCache.Add(path, Asset);

                            //Enqueue for load
                            lock (_loadQueue)
                            {
                                _loadQueue.Enqueue(Asset);
                            }

                            return Asset;
                        }


                    }
                }
                Debug.LogErrorFormat("Coudn't find requested type of asset in AssetTypes ({0})",typeof(T).Name);
                return null;
            }
            else
            {
                Debug.LogErrorFormat("Couldn't load asset with path '{0}'",path);
                return null;
            }
        }
    }
}
