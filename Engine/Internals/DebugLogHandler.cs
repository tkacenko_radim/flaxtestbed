﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaxEngine
{
    internal sealed class DebugLogHandler : ILogHandler
    {
        public void LogException(Exception exception, Object context)
        {
            Internal_LogException(exception, context);
        }



        public void Log(LogType logType, Object context, string message)
        {
#if DEBUG
            string stackTrace = string.Empty;// Environment.StackTrace;
#else
            string stackTrace = string.Empty;
#endif
            Internal_Log(logType, message, context, stackTrace);
        }

        private void Internal_LogException(Exception exception, Object obj)
        {
            Console.WriteLine($"Exception: {obj?.ToString()}\n{exception.Message}");
#if THROW_EXCEPTIONS
            throw exception;
#endif
        }

        private void Internal_Log(LogType level, string msg, Object obj, string stackTrace)
        {
            Console.WriteLine($"{level}: {msg}");
            if (obj)
            {
                Console.WriteLine(obj.ToString());
            }
            if (!string.IsNullOrEmpty(stackTrace))
            {
                Console.WriteLine(stackTrace);
            }
        }
    }
}
